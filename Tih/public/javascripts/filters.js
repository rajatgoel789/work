'use strict';

/* Filters */

angular.module('myApp.filters', []).
  filter('interpolate', ['version', function(version) {
    return function(text) {
      return String(text).replace(/\%VERSION\%/mg, version);
    }
  }])
  .filter('newlines', function(){
  	  return function(text) {
  	  	var str = encodeURI(text)
  	  	var str1 = str.replace('%5Cn', '<br>')
    	return decodeURI(str1)
 	 }
  });
