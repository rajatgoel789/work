 $(function(){
    var iosocket = io.connect();
    iosocket.on('connect', function () {
        // alert('Connected!!!')
        var tim = [];
        iosocket.on('message', function(message) {
            console.log('message-called',message)
            if(message.message == 'background-process'){
                var time = 90000;
                var stat = message.status;
                var con = stat.split("-")[0];
                var len = stat.split("-")[1];
                var per = parseInt(con) * 100 / parseInt(len);
                $('#pstat').show();
                $('#hide').show();
                $('#savebtn').hide();
                $('#savebtnstp').show();
                $('#saveup').hide();
                $('#savedupstop').show();

                //$('#pstat span').text('Total='+parseInt(len)+', Done URL='+parseInt(con));
                $('#proc div').css('width', parseFloat(per).toFixed(2)+'%');
                $('#proc div').text(parseFloat(per).toFixed(2)+'%');

                //console.log('Arry',tim)
                if(tim.length > 0) {
                    for (var i = 0; i < tim.length; i++) {
                        clearInterval(tim[i])
                        if(i == tim.length - 1){
                            tim = [];
                        }
                    };
                }
                var timer = setTimeout(function(){
                    $('#pstat').hide();
                    $('#savebtn').show();
                    $('#savebtnstp').hide();
                    $('#saveup').show();
                    $('#savedupstop').hide();
                }, time)
                tim.push(timer)
                // console.log('Timer----',timer)
                // console.log('Arry----1',tim,message)
            }
        });

    });
});
