'use strict';

/* Edit Events Controller */
function EditEventCtrl($scope, $http, $location, $routeParams, limitToFilter) {
  $scope.Category = [{ Name: 'Event', id: 36 },{ Name: 'Birth', id: 37 },{ Name: 'Death', id: 38 },{ Name: 'Holiday', id: 39 }];

  $scope.Months = [{ Name: 'January', id: 1 },{ Name: 'February', id: 2 },{ Name: 'March', id: 3 },{ Name: 'April', id: 4 },
    { Name: 'May', id: 5 },{ Name: 'June', id: 6 },{ Name: 'July', id: 7 },{ Name: 'August', id: 8 },{ Name: 'September', id: 9 },
    { Name: 'October', id: 10 },{ Name: 'November', id: 11 },{ Name: 'December', id: 12 }];
  var ddData = [
    {
        text: "Event",
        value: 36,
        selected: true,
        imageSrc: "images/event_icon_wt.png"
    },
    {
        text: "Birth",
        value: 37,
        selected: false,
        imageSrc: "images/birth_icon_wt.png"
    },
    {
        text: "Death",
        value: 38,
        selected: false,
        imageSrc: "images/death_icon_wt.png"
    },
    {
        text: "Holiday",
        value: 39,
        selected: false,
        imageSrc: "images/holiday_icon_wt.png"
    }
];

  $scope.form = {};
  var values = [];
  $scope.items = [];
  $scope.form = [];
  if($routeParams.id){
    $scope.page = 'Edit Event';
  $http.get('/event/' + $routeParams.id).
    success(function(data) {

      $scope.DLink = data[0].link_id;
      $http.get('/api/eventLinks/' + $routeParams.id).
      success(function(data){
        if($scope.DLink){
          for (var i=0; i<data.length; i++){
            if(data[i].id == $scope.DLink){
              $scope.form.push(data[i]);
              values.push(data[i]);
            }
          }
          for (var i=0; i<data.length; i++){
             if(data[i].id != $scope.DLink){
              $scope.form.push(data[i]);
              values.push(data[i]);
             }
          }
        }
        else{
          values.push(data);
          $scope.form=data;
        }

        $('#summernote').summernote({height: 300});
      });
      for (var i=0; i<ddData.length; i++){
        if(ddData[i].value == data[0].category_id){
          ddData[i].selected = true;
        }
        else{
          ddData[i].selected = false;
        }
      }
    $('#demo-htmlselect').ddslick({
      data:ddData,
      onSelected: function(selectedData){
        var id = $('.dd-selected-value').val();
        $scope.selectedCategory = id;
        if($scope.selectedCategory == 37 || $scope.selectedCategory == 38){
          $('.aName , .yearDrop').show();
        }
        else if($scope.selectedCategory == 39){
          $('.yearDrop , .aName').hide();
        }
        else{
          $('.yearDrop').show();
          $('.aName').hide();

        }
      }
    });
      $scope.selectedCategory = data[0].category_id;
      $scope.title = data[0].title;
      //alert($scope.title);
      if(data[0].category_id == 39){
        $('.yearDrop').hide();
        $scope.eventName = data[0].name;
        $scope.selectedMonth = 0;
      }
      if(data[0].category_id == 37|| data[0].category_id == 38)
      {
        $('.aName').show();
        $scope.authorName = data[0].name;
      }
      $scope.events = data;
      console.log(data , data[0].event)
      $scope.abc = data[0].event;
      $scope.years = [];
      $scope.events[0].event = data[0].event;

      for (var i=data[0].month; i<2014; i++){
        $scope.years.push({'Name': i , 'id': i ,'url': 'images/death_icon_wt.png'});
      }
      $scope.dates = [];
      for (var i=1; i<=31; i++){
        $scope.dates.push({'Name': i , 'id': i});
      }
      $scope.options = [{ Name: 'other', id: 2 }];
      $scope.selectedMonth = data[0].month;
      $scope.selectedDate = data[0].day;
      $scope.selectedYear = data[0].year;
     // console.log('editevent', data[0].event);
    });
  }
  else{
    $scope.page = 'Add New Event';
    $scope.newEvent = 'yes';
    var d = new Date();
    //var sachin1="abx";
    var day = d.getDate();
    $scope.dates = [];
      for (var i=1; i<=31; i++){
        $scope.dates.push({'Name': i , 'id': i});
      }
    var month = d.getMonth();
    var year = d.getFullYear();
    $scope.events =[{'events':''}];
    $scope.selectedMonth = month+1;
    $scope.selectedDate = day;
    $scope.selectedYear = year;
    $('#demo-htmlselect').ddslick({
      data:ddData,
      onSelected: function(selectedData){
        var id = $('.dd-selected-value').val();
        $scope.selectedCategory = id;
        if($scope.selectedCategory == 37 || $scope.selectedCategory == 38){
          $('.aName , .yearDrop').show();
        }
        else if($scope.selectedCategory == 39){
          $('.yearDrop , .aName').hide();
        }
        else{
          $('.yearDrop').show();
          $('.aName').hide();

        }
      }
    });
  }
    $scope.items = [];
    $scope.items.push($scope.selectLink);

  //FUNCTION CALL WHEN WE WRITE TO SEARCH LINK  IN AUTOSEARCH BOX
  $scope.getLocation = function(linkName) {
    return $http.get("/search/"+linkName).then(function(res){
      var links = []
      angular.forEach(res.data, function(item){
        links.push(item);
      });
      return links;
    });
  };
  $scope.newLineToBR = function(text) {
    alert(text)
    return text ? text.replace(/\n/g, '<br />') : '';
  };
  $scope.checkEventType = function(id){
    var id = $('.dd-selected-value').val();
    $scope.selectedCategory = id;
    if($scope.selectedCategory == 37 || $scope.selectedCategory == 38){
      $('.aName , .yearDrop').show();
    }
    else if($scope.selectedCategory == 39){
      $('.yearDrop , .aName').hide();
      $scope.selectedYear = 0;
    }
    else{
      $('.yearDrop').show();
      $('.aName').hide();
    }
  };
  //FUNCTION CALL WHEN SELECT LINK FROM AUTOSEARCH
  $scope.onSelect = function ($item) {
    $scope.form.push($item);
 };

//FUNCTION CALL WHEN  DELETE LINK FROM LIST
$scope.deleteLink = function(index){
  $scope.form.splice(index, 1);
};

//FUNCTION CALL WHEN CLCIK ON EVENT UPDATE BUTTON
  $scope.submitForm = function(){
    var title = $scope.title;
    var name = $scope.form1.textarea.$modelValue;
    //var info = $scope.event.event;
    if(!name){
      $(".empty-error").show();
      $(".empty-error").delay(3000).fadeOut();
      return false;
    }
    if($scope.form[0])
      var link_id = $scope.form[0].id;
    else
      var link_id = '';
    var aName = $scope.form1.txtbx.$modelValue;
    var name = $scope.form1.textarea.$modelValue;
    if($scope.selectedCategory == 39)
    {
      $scope.selectedYear = 0;
    }
    if($scope.selectedCategory == 36 || $scope.selectedCategory == 39)
    {
      aName = '';
    }
    var formData = {'event': name , 'day': $scope.selectedDate , 'month': $scope.selectedMonth ,'year': $scope.selectedYear, 'category_id': $scope.selectedCategory , 'link_id': link_id ,'name':aName , 'title':$scope.title};
     if($scope.newEvent == 'yes'){
      $http.post('/add/newEvent', formData).
      success(function(data){
        $scope.insertId = data.insertId;
        //console.log(values[0].length);
        if($scope.form.length > 0) {
      for(var i = 0; i < $scope.form.length; i++) {
        var value = {'link_id': $scope.form[i].id};
        $http.post('/event/link/'+$scope.insertId, value).
          success(function(data){
            $("#eveForm").trigger("reset");
            $("#eveForm textarea").text(' ');
            $(".link_list").hide();
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $('.error').show().text('You have updated Event successfully').delay(2500).hide(0);
          });
        }
      }
      });
     }
     else{
      $http.put('/editEvent/'+ $routeParams.id, formData).
      success(function(data){
        // console.log(values[0].length);
        $("html, body").animate({ scrollTop: 0 }, "slow");
        $('.error').show().text('You have updated Event successfully').delay(2500).hide(0);
      });
    $http.get('/linkDelete/'+ $routeParams.id).
      success(function(data){
       if($scope.form.length > 0) {
      for(var i = 0; i < $scope.form.length; i++) {
        var value = {'link_id': $scope.form[i].id};
        $http.post('/event/link/'+$routeParams.id, value).
          success(function(data){
            $("#eveForm").trigger("reset");
            $("#eveForm textarea").text(' ');
            $(".link_list").hide();
            console.log("Event Updated Successfully");
          });
        }
      }
      });
     }
   };
}
