	// create the module and name it app
	var app = angular.module('facts', ['ngRoute']);

	// configure our routes
	app.config(function($routeProvider) {
		$routeProvider

			// route for the home page
			.when('/', {
				templateUrl : 'pages/home.jade',
				controller  : 'mainController'
			});
	});

	// create the controller and inject Angular's $scope
	app.controller('mainController', function($scope, $http) {
		// create a message to display in our view
		alert('here');
      $http.get('/events')
        .success(function(data) {
    	  alert('success');
	    })
	    .error(function(data) {
	      alert('eror');
	      console.log('Error: ' + data);
	    });
		$scope.message = 'Everyone come and see how good I look!';
	});

	app.controller('aboutController', function($scope) {
		alert('app');
		$scope.message = 'Look! I am an about page.';
	});

	app.controller('contactController', function($scope) {
		$scope.message = 'Contact us! JK. This is just a demo.';
	});