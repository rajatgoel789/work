'use strict';
/* Event Controller */
var monthNames = [ "" , "January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December" ];
var save_event_type = 'single';

function parserCtrl($scope, $http , $location , $routeParams) {
  $('.exit-event').hide();
  var d = new Date();
    $scope.events=[];
    $scope.births=[];
    $scope.deaths=[];
    $scope.holiday=[];
    $scope.set_category  = 36;
  if($routeParams.month && $routeParams.day){
    var day = parseInt($routeParams.day);
    var month = parseInt($routeParams.month)-1;
  }
  else{
    var day = d.getDate();
    var month = d.getMonth();
  }
  var year = d.getFullYear();
  $scope.monthNum = month;
  if($scope.monthNum == 2){
    var totaldays = 29;
  }
  else{
    var totaldays = new Date(year, $scope.monthNum, 0).getDate();
  }
  $scope.selectedMonth = month;
  $scope.selectedDate = day;
  $scope.eventType = 36;
  $scope.dates = [];
  for(var i=1; i<=totaldays; i++){
    $scope.dates.push({'Name': i , 'id': i});
  }

  $scope.Months = [{ Name: 'January', id: 0 },{ Name: 'February', id: 1 },{ Name: 'March', id: 2 },{ Name: 'April', id: 3 },
    { Name: 'May', id: 4 },{ Name: 'June', id: 5 },{ Name: 'July', id: 6 },{ Name: 'August', id: 7 },{ Name: 'September', id: 8 },
    { Name: 'October', id: 9 },{ Name: 'November', id: 10 },{ Name: 'December', id: 11 }];

  $scope.getEvents = function(){

    $('.event-list').hide();
    $('.loader , .new-events .active').show();
    var sltmnt = $scope.selectedMonth+1;
    $scope.month = monthNames[sltmnt];
    var path = 'http://en.wikipedia.org/wiki/'+$scope.month+'_'+$scope.selectedDate;
    var data = {'path':path ,'day':$scope.selectedDate , 'month':$scope.selectedMonth+1};
    var data1 = {'path':path ,'day':$scope.selectedDate , 'month':$scope.selectedMonth+1};

    $http.post('/wikiData', data).
    success(function(data,status,header,config){
      if(data.status == 'inprocess'){
          console.log('Processing')
          $('.event-list').hide();
          $('.loader').show();
          var intVal  = setInterval(function(){
              $http.post('/localWikiData', data1).
                  success(function(data2,status,header,config){
                      if(data2.status == 'inprocess'){
                          console.log('DATA In Process');
                      }else{
                          console.log('DATA DONE');
                          clearInterval(intVal);
                          wikiDataPro(data2);
                      }
                  }).
                  error(function(err,status,header,config){
                      alert('Please wait and try again');
                  });
          }, 5000);

      }else{
          wikiDataPro(data);
      }
    }).
    error(function(err,status,header,config){
      console.log('error', status);
      alert('Plear Try Again');
    });
  };


function wikiDataPro(data){

     var data1 = {'day':$scope.selectedDate , 'month':$scope.selectedMonth+1 , 'wikiEvents':data.wikiEvents};
      $http.post('/eventItem', data1).
        success(function(data,status,header,config){
         console.log("DATA EVENTS",data);
         $scope.events = data.newevents;
         $scope.eventsLen = data.newevents.length;
         $scope.exitEvents = data.exitEvents;
         $scope.eventsE = data.internal;
        });
      var data2 = {'day':$scope.selectedDate , 'month':$scope.selectedMonth+1 , 'wikiBirths':data.wikiBirths};
      $http.post('/birthItem', data2).
        success(function(data,status,header,config){
        console.log("DATA Birth",data);
        $scope.births = data.internalBirths;
        $scope.birthsLen = data.internalBirths.length;
        $scope.newbirths = data.NewBirths;
        $scope.exitBirthEvents = data.exitBirthEvents;
      });
      var data3 = {'day':$scope.selectedDate , 'month':$scope.selectedMonth+1 , 'wikiDeaths':data.wikiDeaths};
      $http.post('/deathItem', data3).
        success(function(data,status,header,config){
        console.log("DATA death",data);
        $scope.deaths = data.internalDeaths;
        $scope.deathsLen = data.internalDeaths.length;
        $scope.newdeaths = data.NewDeaths;
        $scope.exitDeathEvent = data.exitDeathEvent;
      });
      var data4 = {'day':$scope.selectedDate , 'month':$scope.selectedMonth+1 , 'wikiHolidays':data.wikiHolidays};
      $http.post('/holidayItem', data4).
        success(function(data,status,header,config){
          console.log("DATA holiday",data);
          $('.event-list').show();
          $('.loader').hide();
          $scope.holidays = [];
          $scope.newholidays = [];
          $scope.exitHolidayEvents = data.exitHolidayEvents;
          $scope.exitHolidayEvents = [];
          var idarray = [];
          //$('#savebtn').show();
          for(var i=0; i<=data.internalHolidays.length-1; i++){
            $scope.holidays.push({'event':data.internalHolidays[i], 'day':$scope.selectedDate});
          }
          for(var i=0; i<=data.exitHolidayEvents.length-1; i++){
            $scope.exitHolidayEvents.push({'event':data.exitHolidayEvents[i].event, 'day':$scope.selectedDate});
          }
          console.log('inter', $scope.exitHolidayEvents);
          for(var i=0; i<=data.NewHolidays.length-1; i++){

            $scope.newholidays.push({'event':data.NewHolidays[i].event, 'finalUrls':data.NewHolidays[i].finalUrls , 'day':$scope.selectedDate});
          }
        });
        $(".add_event_bx").show();
        $("#savebtnstp , #savedupstop").hide();
}

  $scope.saveNewEventsStop = function(){
     alert("Please wait background process is running.")
  };
  $scope.savedupEventsStop = function(){
     alert("Please wait background process is running.")
  };

  // Harinder - All events
  $scope.saveNewEvents = function(){
        var timer = 0;
        var tUrls = 0;
        var flag  = 0;
        $scope.category = $scope.set_category;
        save_event_type = 'multiple';
        var category = $scope.category;
        if(category == 36){
            $scope.newEvents = $scope.events;
        }else if(category == 37){
            $scope.newEvents = $scope.newbirths;
        }else if(category == 38){
            $scope.newEvents = $scope.newdeaths;
        }else {
            $scope.newEvents = $scope.newholidays;
        }

        for(var i =0; i<$scope.newEvents.length; i++){
            tUrls = tUrls + $scope.newEvents[i].finalUrls.length;
            var index = i;
            if(category == 36){
                $('.'+index+'button').hide();
                $('.'+index+'loader').show();
                $scope.newEvents[index].name = '';
            }else if(category == 37){
                var str = $scope.newEvents[index].event;
                // console.log($scope.newEvents[index].event)
                var res = str.replace($scope.newEvents[index].name+', ', "");
                $scope.newEvents[index].event = res;
                //console.log($scope.newEvents[index].name,'----',res)
                $('.'+index+'button-birth').hide();
                $('.'+index+'loader-birth').show();
            }else if(category == 38){
                var str = $scope.newEvents[index].event;
              //  console.log($scope.newEvents[index].event)
                var res = str.replace($scope.newEvents[index].name+', ', "");
                $scope.newEvents[index].event = res;
              //  console.log($scope.newEvents[index].name,'----',res)
                $('.'+index+'button-death').hide();
                $('.'+index+'loader-death').show();
            }else {
                $('.'+index+'button-holiday').hide();
                $('.'+index+'loader-holiday').show();
                $scope.newEvents[index].name = '';
                $scope.newEvents[index].year = 0;
            }
            var month = $scope.selectedDate;
            var title = '';
            var data = {'name':$scope.newEvents[index].name,'event':$scope.newEvents[index].event,'month':$scope.selectedMonth+1,'day':$scope.newEvents[index].day,'year':$scope.newEvents[index].year,'title':title,'category_id':category,'link_id': '','urls':$scope.newEvents[index].finalUrls,'status':1 , 'index':index};

            $http.post('/add/newEvent', data).
                success(function(data){
                    $scope.insertId = data.insertId;
                    var inxedval = data.indexR;
                    if($scope.category == 36){
                        $scope.exitEvents.push({'eventInternal':$scope.events[inxedval].event,'year':$scope.events[inxedval].year,'title':$scope.events[inxedval].title,'name':$scope.events[inxedval].name,'category_id':$scope.events[inxedval].category_id,'day':$scope.events[inxedval].day,'month':$scope.events[inxedval].month,'status':$scope.events[inxedval].status});
                    }else if($scope.category == 37){
                        $scope.exitBirthEvents.push({'event':$scope.newbirths[inxedval].event,'year':$scope.newbirths[inxedval].year,'title':$scope.newbirths[inxedval].title,'name':$scope.newbirths[inxedval].name,'category_id':$scope.newbirths[inxedval].category_id,'day':$scope.newbirths[inxedval].day,'month':$scope.newbirths[inxedval].month,'status':$scope.newbirths[inxedval].status});
                    }else if($scope.category == 38){
                        $scope.exitDeathEvent.push({'event':$scope.newdeaths[inxedval].event,'year':$scope.newdeaths[inxedval].year,'title':$scope.newdeaths[inxedval].title,'name':$scope.newdeaths[inxedval].name,'category_id':$scope.newdeaths[inxedval].category_id,'day':$scope.newdeaths[inxedval].day,'month':$scope.newdeaths[inxedval].month,'status':$scope.newdeaths[inxedval].status});
                    }else{
                       console.log('SAVE Year--',$scope.newholidays[inxedval].year); $scope.exitHolidayEvents.push({'event':$scope.newholidays[inxedval].event,'year':$scope.newholidays[inxedval].year,'title':$scope.newholidays[inxedval].title,'name':$scope.newholidays[inxedval].name,'category_id':$scope.newholidays[inxedval].category_id,'day':$scope.newholidays[inxedval].day,'month':$scope.newholidays[inxedval].month,'status':$scope.newholidays[inxedval].status});
                    }
                    var eId = data.result.insertId;
                    var link_id= data.result.insertId;
                    var totalUrls = data.urlsName;
                    var index_val = data.indexR;
                    for(var c=0; c<totalUrls.length; c++){
                        var url = totalUrls[c].url;
                        var urldata = {'url':url,'count':c , 'link_id':link_id , 'index':index_val , 'image_index':c};

                        $http.post('/linkImage', urldata).
                            success(function(dataR, status, headers, config) {
                                var image_index_val = dataR.image_index;
                                var index_val = data.indexR;
                                if(typeof dataR.sUrl == 'undefined'){
                                    console.log('undefiend');
                                    if($scope.category == 36){
                                        if(image_index_val == 0){
                                            $('.'+index_val+'loader').hide();
                                            $scope.events.splice(index_val,1);
                                        }
                                    }else if($scope.category == 37){
                                        if(image_index_val == 0){
                                            $('.'+index_val+'loader-birth').hide();
                                            $scope.newbirths.splice(index_val,1);
                                        }
                                    }else if($scope.category == 38){
                                        if(image_index_val == 0){
                                            $('.'+index_val+'loader-death').hide();
                                            $scope.newdeaths.splice(index_val,1);
                                        }
                                    }else{
                                        if(image_index_val == 0){
                                            $('.'+index_val+'loader-holiday').hide();
                                            $scope.newholidays.splice(index_val,1);
                                        }
                                    }
                                    var event = totalUrls[0].name;
                                    var url = totalUrls[0].url;
                                    var event_id = data.event_id;
                                    var dataa = {"url": dataR.curl }
                                    $http.post('/noImageLink',dataa).
                                        success(function(result1){
                                            if (typeof result1.result != 'undefined'){
                                                var linkid = result1.result.insertId;
                                            }else{
                                                var linkid = result1.link_id;
                                            }
                                            var newdata= {'event_id' : eId, 'linkId' : linkid};
                                            $http.post('/connectLinkEvent',newdata).
                                                success(function(result2){
                                                    console.log('Connected->',result2);
                                                });
                                        });

                                }else{
                                    var index_val = data.indexR;
                                    var dataVal = {'url':dataR.curl,'image': dataR.sUrl,'name':data.urlsName[dataR.count].name, 'counter':dataR.count, 'eventId': eId };

                                    $http.post('/addLink1', dataVal).
                                        success(function(result){
                                            if(typeof result.res == 'undefined') {
                                                var dataa= {'event_id' : result.event_id,"linkId":result.link_id}
                                            }else{
                                                var dataa = {'linkId':result.res.insertId,'event_id':result.event_id};
                                            }
                                            var imageval = 0;
                                            console.log('index_val',index_val)
                                            if($scope.category == 36){
                                                if(save_event_type == 'multiple'){
                                                    $scope.events = [];
                                                }
                                                if(imageval == 0){
                                                    $('.'+index_val+'loader').hide();
                                                    $scope.events.splice(index_val,1);
                                                }
                                            }else if($scope.category == 37){
                                                if(imageval == 0){
                                                    $('.'+index_val+'loader-birth').hide();
                                                    $scope.newbirths.splice(index_val,1);
                                                }
                                                if(save_event_type == 'multiple'){
                                                    $scope.newbirths = [];
                                                }
                                            }else if($scope.category == 38){
                                                if(imageval == 0){
                                                    $('.'+index_val+'loader-death').hide();
                                                    $scope.newdeaths.splice(index_val,1);
                                                }
                                                if(save_event_type == 'multiple'){
                                                    $scope.newdeaths = [];
                                                }
                                            }else {
                                                if(imageval == 0){
                                                    $('.'+index_val+'loader-holiday').hide();
                                                    $scope.newholidays.splice(index_val, 1);
                                                }
                                                if(save_event_type == 'multiple'){
                                                    $scope.newholidays = [];
                                                }
                                            }
                                            $http.post('/eventLink', dataa).
                                                success(function(res){
                                                    console.log('Done')
                                                }).
                                                error(function(err){
                                                    console.log('Getting error');
                                                });
                                            // Call API in the End of all events added
                                            console.log('Timer --> ', timer, tUrls)
                                            if(timer == tUrls){
                                                console.log('Timer111')
                                                if(flag != 1 ) {
                                                    $http.post('/addImagesUrls', dataVal).
                                                        success(function(result){
                                                            console.log(result)
                                                        });
                                                        // $('#savebtn').hide();
                                                        // $('#savebtnstp').show();
                                                        // $('#saveup').hide();
                                                        // $('#savedupstop').show();

                                                    console.log('Final Timer called --> ', timer, tUrls)
                                                    flag = 1;
                                                }
                                            }
                                        }); //

                                } // Not undefined

                                timer++;
                            }).error(function(err){
                                console.log(err)
                            });
                    }// totalUrl

                })// newEvent

        }// main loop

  }


  // Harinder - single event..
  $scope.saveNewEvent = function(even, index, category){
      $scope.category = category;
      if(category == 36){
          $('.'+index+'button').hide();
          $('.'+index+'loader').show();
          $scope.newEvents = $scope.events;
          $scope.newEvents[index].name = '';
      }else if(category == 37){
          $('.'+index+'button-birth').hide();
          $('.'+index+'loader-birth').show();
          $scope.newEvents = $scope.newbirths;
          var str = $scope.newEvents[index].event;
          var res = str.replace($scope.newEvents[index].name+', ', "");
          $scope.newEvents[index].event = res;
      }else if(category == 38){
          $('.'+index+'button-death').hide();
          $('.'+index+'loader-death').show();
          $scope.newEvents = $scope.newdeaths;
          var str = $scope.newEvents[index].event;
          var res = str.replace($scope.newEvents[index].name+', ', "");
          $scope.newEvents[index].event = res;
      }else {
          $('.'+index+'button-holiday').hide();
          $('.'+index+'loader-holiday').show();
          $scope.newEvents = $scope.newholidays;
          $scope.newEvents[index].name = '';
          $scope.newEvents[index].year = 0;

      }
      var month = $scope.selectedDate;
      var title = '';
      var data = {'name':$scope.newEvents[index].name,'event':$scope.newEvents[index].event,'month':$scope.selectedMonth+1,'day':$scope.newEvents[index].day,'year':$scope.newEvents[index].year,'title':title,'category_id':category,'link_id': '','urls':$scope.newEvents[index].finalUrls,'status':1 , 'index':index};
      $http.post('/add/newEvent', data).
          success(function(data) {
              $scope.insertId = data.insertId;
              var inxedval = data.indexR;
              if ($scope.category == 36) {
                  $scope.exitEvents.push({'eventInternal': $scope.events[inxedval].event, 'year': $scope.events[inxedval].year, 'title': $scope.events[inxedval].title, 'name': $scope.events[inxedval].name, 'category_id': $scope.events[inxedval].category_id, 'day': $scope.events[inxedval].day, 'month': $scope.events[inxedval].month, 'status': $scope.events[inxedval].status});
              } else if ($scope.category == 37) {
                  $scope.exitBirthEvents.push({'event': $scope.newbirths[inxedval].event, 'year': $scope.newbirths[inxedval].year, 'title': $scope.newbirths[inxedval].title, 'name': $scope.newbirths[inxedval].name, 'category_id': $scope.newbirths[inxedval].category_id, 'day': $scope.newbirths[inxedval].day, 'month': $scope.newbirths[inxedval].month, 'status': $scope.newbirths[inxedval].status});
              } else if ($scope.category == 38) {
                  $scope.exitDeathEvent.push({'event': $scope.newdeaths[inxedval].event, 'year': $scope.newdeaths[inxedval].year, 'title': $scope.newdeaths[inxedval].title, 'name': $scope.newdeaths[inxedval].name, 'category_id': $scope.newdeaths[inxedval].category_id, 'day': $scope.newdeaths[inxedval].day, 'month': $scope.newdeaths[inxedval].month, 'status': $scope.newdeaths[inxedval].status});
              } else {

                  $scope.exitHolidayEvents.push({'event': $scope.newholidays[inxedval].event, 'year': $scope.newholidays[inxedval].year, 'title': $scope.newholidays[inxedval].title, 'name': $scope.newholidays[inxedval].name, 'category_id': $scope.newholidays[inxedval].category_id, 'day': $scope.newholidays[inxedval].day, 'month': $scope.newholidays[inxedval].month, 'status': $scope.newholidays[inxedval].status});
              }
              var eId = data.result.insertId;
              var link_id = data.result.insertId;
              var totalUrls = data.urlsName;
              var index_val = data.indexR;
              var url = [];
              var totalUrlsLen = totalUrls.length;
              console.log("TOTAL URLS ", totalUrlsLen);
              if (totalUrlsLen == 0) {
                  $('.' + index_val + 'loader').hide();
                  $scope.events.splice(index_val, 1);
              }
              for (var c = 0; c < totalUrls.length; c++) {
                  var url = totalUrls[c].url;
                  var url1 = url;
                  var urldata = {'url': url, 'count': c, 'link_id': link_id, 'index': index_val, 'image_index': c};
                  var t = 0;
                  $http.post('/linkImage', urldata).
                      success(function(dataR, status, headers, config) {
                          console.log("data from link image===>",dataR);
                          var image_index_val = dataR.image_index;
                          var index_val = data.indexR;
                          if(typeof dataR.sUrl == 'undefined') {
                              if ($scope.category == 36) {
                                  if (image_index_val == 0) {
                                      $('.' + index_val + 'loader').hide();
                                      $scope.events.splice(index_val, 1);
                                  }
                              } else if ($scope.category == 37) {
                                  if (image_index_val == 0) {
                                      $('.' + index_val + 'loader-birth').hide();
                                      $scope.newbirths.splice(index_val, 1);
                                  }
                              } else if ($scope.category == 38) {
                                  if (image_index_val == 0) {
                                      $('.' + index_val + 'loader-death').hide();
                                      $scope.newdeaths.splice(index_val, 1);
                                  }
                              } else {
                                  if (image_index_val == 0) {
                                      $('.' + index_val + 'loader-holiday').hide();
                                      $scope.newholidays.splice(index_val, 1);
                                  }
                              }

                              var event = totalUrls[0].name;
                              var url = totalUrls[0].url;
                              var event_id = data.event_id;

                              var dataa = {"url": dataR.curl }
                              console.log("data urls--->", dataa);
                              $http.post('/noImageLink', dataa).
                                  success(function (result1) {
                                      if (typeof result1.result != 'undefined') {
                                          var linkid = result1.result.insertId;
                                      } else {
                                          var linkid = result1.link_id;
                                      }
                                      var newdata= {'event_id' : eId,"linkId":linkid}
                                      $http.post('/connectLinkEvent', newdata).
                                          success(function (result2) {
                                              console.log(result2);

                                          });
                                  });
                          }else{
                              console.log('Link available');
                              var imageUrl = {'url':'http:'+dataR.sUrl,'counter':dataR.count,'link_id':dataR.link_id,'index':index_val,"image_index":image_index_val};
                              console.log("here","imageurls",imageUrl);
                              $http.post('/uploadParserImage',imageUrl).
                                  success(function(imageData , status , headers , config){
                                      var index_val = data.indexR;
                                      var dataVal  = {'image_index':imageData.image_index,'index':index_val,'url':data.urlsName[imageData.indx].url,'name':data.urlsName[imageData.indx].name,'id':imageData.link_id,'medium_image':imageData.medium_image , 'small_image':imageData.small_image ,'image':imageData.image ,'link_id':imageData.link_id};
                                      console.log("here2",data.urlsName[imageData.indx].url,"len",totalUrls.length);

                                      $http.post('/addLink', dataVal).
                                          success(function(result){
                                              if(result.res == undefined){
                                                  var dataa= {'event_id' : result.event_id,"linkId":result.link_id}
                                              }else{
                                                  var image_indx = result.image_index;
                                                  var index_val = data.indexR;
                                                  var dataa = {'eventId':eId,'linkId':result.res.insertId,'event_id':result.event_id,'index':index_val,'image_index':image_indx};
                                              }
                                              var index_val = data.indexR;
                                              $http.post('/eventLink', dataa).
                                                  success(function(res){
                                                      //if
                                                      var inxedval = res.indexR;
                                                      var imageval = res.image_index;
                                                      console.log("resrsesrrfsf--->",res);
                                                      if($scope.category == 36){
                                                          $(even.currentTarget).parent().prev().prev().parent().remove();
                                                          if(save_event_type == 'multiple'){
                                                              $scope.events = [];
                                                          }
                                                          if(imageval == 0){
                                                              $('.'+inxedval+'loader').hide();
                                                              $scope.events.splice(inxedval,1);
                                                          }
                                                      }else if($scope.category == 37){
                                                          //console.log("sachin");
                                                          $(even.currentTarget).parent().prev().prev().parent().remove();
                                                          if(imageval == 0){
                                                              $('.'+inxedval+'loader-birth').hide();
                                                              $scope.newbirths.splice(inxedval,1);
                                                          }
                                                          if(save_event_type == 'multiple'){
                                                              $scope.newbirths = [];
                                                          }
                                                      }else if($scope.category == 38){
                                                          $(even.currentTarget).parent().prev().prev().parent().remove();
                                                          if(imageval == 0){
                                                              $('.'+inxedval+'loader-death').hide();
                                                              $scope.newdeaths.splice(inxedval,1);
                                                          }
                                                          if(save_event_type == 'multiple'){
                                                              $scope.newdeaths = [];
                                                          }
                                                      }else {
                                                          $(even.currentTarget).parent().prev().prev().parent().remove();
                                                          if(imageval == 0){
                                                              $('.'+inxedval+'loader-holiday').hide();
                                                              $scope.newholidays.splice(inxedval, 1);
                                                          }
                                                          if(save_event_type == 'multiple'){
                                                              $scope.newholidays = [];
                                                          }
                                                      }
                                                  }).
                                                  error(function(err){
                                                      console.log('Getting error');
                                                  });
                                          }).
                                          error(function(err){
                                              console.log('Problem in adding links');
                                          });
                                  }).
                                  error(function(err){
                                      console.log('Problem In getting Data');
                                  });

                          } // not undefined
                      });

              } // - totalUrls
          }); // - /newEvent

  };


//save individual events from new items
  $scope.saveNewEvent_stop = function(even, index, category){

    var data = {'name':$scope.newEvents[index].name,'event':$scope.newEvents[index].event,'month':$scope.selectedMonth+1,'day':$scope.newEvents[index].day,'year':$scope.newEvents[index].year,'title':title,'category_id':category,'link_id': '','urls':$scope.newEvents[index].finalUrls,'status':1 , 'index':index};
     $http.post('/add/newEvent', data).
      success(function(data){
        $scope.insertId = data.insertId;
        var inxedval = data.indexR;
        if($scope.category == 36){
          $scope.exitEvents.push({'eventInternal':$scope.events[inxedval].event,'year':$scope.events[inxedval].year,'title':$scope.events[inxedval].title,'name':$scope.events[inxedval].name,'category_id':$scope.events[inxedval].category_id,'day':$scope.events[inxedval].day,'month':$scope.events[inxedval].month,'status':$scope.events[inxedval].status});
        }else if($scope.category == 37){
          $scope.exitBirthEvents.push({'event':$scope.newbirths[inxedval].event,'year':$scope.newbirths[inxedval].year,'title':$scope.newbirths[inxedval].title,'name':$scope.newbirths[inxedval].name,'category_id':$scope.newbirths[inxedval].category_id,'day':$scope.newbirths[inxedval].day,'month':$scope.newbirths[inxedval].month,'status':$scope.newbirths[inxedval].status});
        }else if($scope.category == 38){
          $scope.exitDeathEvent.push({'event':$scope.newdeaths[inxedval].event,'year':$scope.newdeaths[inxedval].year,'title':$scope.newdeaths[inxedval].title,'name':$scope.newdeaths[inxedval].name,'category_id':$scope.newdeaths[inxedval].category_id,'day':$scope.newdeaths[inxedval].day,'month':$scope.newdeaths[inxedval].month,'status':$scope.newdeaths[inxedval].status});
        }else{
          $scope.exitHolidayEvents.push({'event':$scope.newholidays[inxedval].event,'year':$scope.newholidays[inxedval].year,'title':$scope.newholidays[inxedval].title,'name':$scope.newholidays[inxedval].name,'category_id':$scope.newholidays[inxedval].category_id,'day':$scope.newholidays[inxedval].day,'month':$scope.newholidays[inxedval].month,'status':$scope.newholidays[inxedval].status});
        }
        var eId = data.result.insertId;
        var link_id= data.result.insertId;
        var totalUrls = data.urlsName;
        var index_val = data.indexR;
        var url = [];
        var totalUrlsLen= totalUrls.length;
        console.log("TOTAL URLS ",totalUrlsLen);
        if(totalUrlsLen ==0) {
          $('.'+index_val+'loader').hide();
          $scope.events.splice(index_val,1);
        }

        for(var c=0; c<totalUrls.length; c++) {
          var url = totalUrls[c].url;
          var url1=url;
          var urldata = {'url':url,'count':c , 'link_id':link_id , 'index':index_val , 'image_index':c};
          console.log("urldata==>",urldata);
          var t = 0;
          $http.post('/linkImage', urldata).
            success(function(dataR, status, headers, config) {
              console.log("data from link image===>",dataR);
              var image_index_val = dataR.image_index;
              var index_val = data.indexR;

              if(typeof dataR.sUrl == 'undefined'){
              console.log("call from here  &&&& " );

                if($scope.category == 36){
                  if(image_index_val == 0){
                    $('.'+index_val+'loader').hide();
                    $scope.events.splice(index_val,1);
                  }
                }else if($scope.category == 37){
                  if(image_index_val == 0){
                    $('.'+index_val+'loader-birth').hide();
                    $scope.newbirths.splice(index_val,1);
                  }
                }else if($scope.category == 38){
                  if(image_index_val == 0){
                    $('.'+index_val+'loader-death').hide();
                    $scope.newdeaths.splice(index_val,1);
                  }
                }else{
                  if(image_index_val == 0){
                    $('.'+index_val+'loader-holiday').hide();
                    $scope.newholidays.splice(index_val,1);
                  }
                }

              /**added by rajat 16/7 */
              console.log("===================",url , "URL!",url1,"URLDATA",urldata.url);
              console.log("data here rajat--->",data.result.insertId);
              console.log("imageUrl" , data.sUrl );
              var event = totalUrls[0].name;
              //var link_id = dataR.link_id;
              var url = dataR.src;
              console.log('CALedf' , url ,dataR.src)
              var event_id = data.result.insertId;
              var dataa = {"url":url1 }

              t++;
              console.log("data urls--->",dataa);
              //var dataa= {'event_id' : event_id,"linkId":link_id,"url":url }
              $http.post('/noImageLink',dataa).
                    success(function(result1){
                     console.log("result of noImageLink--->",result1.result);
                     if (result1.result != undefined){
                      var linkid = result1.result.insertId;
                    }else{
                      var linkid = result1.link_id;
                    }
                      ///var linkid = result1.result.insertId;
                      console.log('??????',linkid, event_id ,eId)
                      var newdata= {'event_id' : eId,"linkId":linkid}
                        $http.post('/connectLinkEvent',newdata).
                          success(function(result2){
                            console.log(result2);

                          });
                    });



              }
              else if(!dataR.sUrl){
                console.log('no url');
                if($scope.category == 36){
                  if(image_index_val == 0){
                    $('.'+index_val+'loader').hide();
                    $scope.events.splice(index_val,1);
                  }
                }else if($scope.category == 37){
                  if(image_index_val == 0){
                    $('.'+index_val+'loader-birth').hide();
                    $scope.newbirths.splice(index_val,1);
                  }
                }else if($scope.category == 38){
                  if(image_index_val == 0){
                    $('.'+index_val+'loader-death').hide();
                    $scope.newdeaths.splice(index_val,1);
                  }
                }else{
                  if(image_index_val == 0){
                    $('.'+index_val+'loader-holiday').hide();
                    $scope.newholidays.splice(index_val,1);
                  }
                }
              console.log(totalUrls);
              console.log("data here sachin--->",data.result.insertId);
              console.log("imageUrl" , data.sUrl );
              var event = totalUrls[0].name;
              //var link_id = dataR.link_id;
              var url = totalUrls[0].url;
              var event_id = data.result.insertId;
              var dataa = {"url":url }
              console.log("data urls--->",dataa);
              //var dataa= {'event_id' : event_id,"linkId":link_id,"url":url }
              $http.post('/noImageLink',dataa).
                    success(function(result1){
                     console.log("result of noImageLink--->",result1.result);
                     if (result1.result != undefined){
                      var linkid = result1.result.insertId;
                    }else{
                      var linkid = result1.link_id;
                    }
                      ///var linkid = result1.result.insertId;
                      var newdata= {'event_id' : event_id,"linkId":linkid}
                        $http.post('/connectLinkEvent',newdata).
                          success(function(result2){
                            console.log(result2);

                          });
                    });

              }
              else{
              var imageUrl = {'url':'http:'+dataR.sUrl,'counter':dataR.count,'link_id':dataR.link_id,'index':index_val,"image_index":image_index_val};
              console.log("here","imageurls",imageUrl);
              $http.post('/uploadParserImage',imageUrl).
                success(function(imageData , status , headers , config){
                   var index_val = data.indexR;
                  var dataVal  = {'image_index':imageData.image_index,'index':index_val,'url':data.urlsName[imageData.indx].url,'name':data.urlsName[imageData.indx].name,'id':imageData.link_id,'medium_image':imageData.medium_image , 'small_image':imageData.small_image ,'image':imageData.image ,'link_id':imageData.link_id};
                  console.log("here2",data.urlsName[imageData.indx].url,"len",totalUrls.length);
                  $http.post('/addLink', dataVal).
                    success(function(result){
                      console.log("res Sachin===>",result.res);
                      if(result.res == undefined){
                        var dataa= {'event_id' : result.event_id,"linkId":result.link_id}
                      }else{
                      //console.log("resu==>",result);
                      var image_indx = result.image_index;
                      var index_val = data.indexR;
                      var dataa = {'eventId':eId,'linkId':result.res.insertId,'event_id':result.event_id,'index':index_val,'image_index':image_indx};
                    }
                    var index_val = data.indexR;
                    $http.post('/eventLink', dataa).
                      success(function(res){
                        //if
                        var inxedval = res.indexR;
                        var imageval = res.image_index;
                        console.log("resrsesrrfsf--->",res);
                        if($scope.category == 36){
                          $(even.currentTarget).parent().prev().prev().parent().remove();
                          if(save_event_type == 'multiple'){
                            $scope.events = [];
                          }
                          if(imageval == 0){
                            $('.'+inxedval+'loader').hide();
                            $scope.events.splice(inxedval,1);
                          }
                        }else if($scope.category == 37){
                          //console.log("sachin");
                          $(even.currentTarget).parent().prev().prev().parent().remove();
                          if(imageval == 0){
                            $('.'+inxedval+'loader-birth').hide();
                            $scope.newbirths.splice(inxedval,1);
                          }
                          if(save_event_type == 'multiple'){
                            $scope.newbirths = [];
                          }
                        }else if($scope.category == 38){
                          $(even.currentTarget).parent().prev().prev().parent().remove();
                          if(imageval == 0){
                            $('.'+inxedval+'loader-death').hide();
                            $scope.newdeaths.splice(inxedval,1);
                          }
                          if(save_event_type == 'multiple'){
                            $scope.newdeaths = [];
                          }
                        }else {
                          $(even.currentTarget).parent().prev().prev().parent().remove();
                          if(imageval == 0){
                            $('.'+inxedval+'loader-holiday').hide();
                            $scope.newholidays.splice(inxedval, 1);
                          }
                          if(save_event_type == 'multiple'){
                            $scope.newholidays = [];
                          }
                        }
                    }).
                    error(function(err){
                        console.log('Getting error');
                    });
                  }).
                    error(function(err){
                      console.log('Problem in adding links');
                  });
                }).
                error(function(err){
                  console.log('Problem In getting Data');
                });
              }



              //console.log("here-->",event,url,link_id);
          }).
            error(function(err){
              console.log("error in getting data from wiki")
            });
          var dataVal  = {'url':data.urlsName[c].url,'name':data.urlsName[c].name,'id':data.result.insertId}
          /*$http.post('/addLink', dataVal).
            success(function(result){
            console.log('LInkId',result.insertId);
            console.log('eventId',eId);
            var dataa = {'eventId':eId,'linkId':result.insertId}
            $http.post('/eventLink', dataa).
              success(function(res){
              console.log('counter',res.result);
            });
          });*/
        }
      });
  };

  $scope.saveNewEventss = function(){
    save_event_type = 'multiple';
    var fff = $('#myTab .active p').attr('class');
    var d = fff.split(' ');
    var dd = d[0].split('-');
    if(dd[1] == 36){
      $('.new-button').click();
      $('.single-event').trigger('click');
    }else if(dd[1] == 37){
      console.log('bbb');
      $('.new-button-birth').click();
      $('.single-event-birth').trigger('click');
    }else if(dd[1] == 38){
      console.log('ddddddd');
      $('.new-button-death').click();
      $('.single-event-death').trigger('click');
    }else{
      console.log('esleee');
      $('.new-button-holi').click();
      $('.single-event-holiday').trigger('click');
    }
  }
// Harinder - save duplicate
$scope.savedupEvents = function(event){
    var timer = 0;
    var tUrls = 0;
    var flag  = 0;
    $scope.category = $scope.set_category;
    save_event_type = 'multiple';
    var category = $scope.category;
    if(category == 36){
        $scope.newEvents = $scope.eventsE;
    }else if(category == 37){
        $scope.newEvents = $scope.births;
    }else if(category == 38){
        $scope.newEvents = $scope.deaths;
    }else {
        $scope.holEvents = $scope.holidays;
        var newEvents = [];
        for (var a=0;a<$scope.holEvents.length;a++){
            var eventWiki = $scope.holEvents[a].event.event;
            var id = $scope.holEvents[a].event.id;
            var finalUrls = $scope.holEvents[a].event.finalUrls;
            newEvents.push({'eventWiki':eventWiki,'id':id,'finalUrls':finalUrls})
        }
        $scope.newEvents = newEvents;
    }
    for(var i =0; i<$scope.newEvents.length; i++) {
        tUrls = tUrls + $scope.newEvents[i].finalUrls.length;
        var index = i;
        if (category == 36) {
            $('.' + index + 'dEedit').hide();
            $('.' + index + 'dEloader').show();
            $scope.newEvents[index].name = '';
        } else if (category == 37) {
            console.log('cate - - - 37',$scope.newEvents[index] , $scope.newEvents[index].eventWiki ,$scope.newEvents[index].name)
            var str = $scope.newEvents[index].eventWiki;
            if(typeof str != 'undefined') {
                var res = str.replace($scope.newEvents[index].name+', ', "");
                $scope.newEvents[index].eventWiki = res;
            }
            $('.' + index + 'dBedit').hide();
            $('.' + index + 'dBloader').show();
        } else if (category == 38) {
            console.log('cate - - - 38',$scope.newEvents[index], $scope.newEvents[index].eventWiki , $scope.newEvents[index].name)
            var str = $scope.newEvents[index].eventWiki;
            if(typeof str != 'undefined') {
                var res = str.replace($scope.newEvents[index].name+', ', "");
                $scope.newEvents[index].eventWiki = res;
            }
            $('.' + index + 'dDedit').hide();
            $('.' + index + 'dDloader').show();
        } else if (category = 39) {
            $('.' + index + 'dHedit').hide();
            $('.' + index + 'dHloader').show();
            $scope.newEvents[index].name = '';
            $scope.newEvents[index].year = 0;
        }
        var month = $scope.selectedDate;
        var title = '';
        var data = {"id" : $scope.newEvents[index].id ,'name':$scope.newEvents[index].name,'event':$scope.newEvents[index].eventWiki,'month':$scope.selectedMonth+1,'day':$scope.newEvents[index].day,'year':$scope.newEvents[index].year,'title':title,'category_id':category,'link_id': '','urls':$scope.newEvents[index].finalUrls,'status':1 , 'index':index};
        $http.post('/updateEvent', data).
            success(function(data) {
                $scope.insertId = data.insertId;
                var inxedval = data.indexR;
                if ($scope.category == 36) {

                    $scope.exitEvents.push({'eventInternal': $scope.eventsE[inxedval].eventWiki, 'year': $scope.eventsE[inxedval].year, 'title': $scope.eventsE[inxedval].title, 'name': $scope.eventsE[inxedval].name, 'category_id': $scope.eventsE[inxedval].category_id, 'day': $scope.eventsE[inxedval].day, 'month': $scope.eventsE[inxedval].month, 'status': $scope.eventsE[inxedval].status});

                } else if ($scope.category == 37) {
                    $scope.exitBirthEvents.push({'event': $scope.births[inxedval].eventWiki, 'year': $scope.births[inxedval].year, 'title': $scope.births[inxedval].title, 'name': $scope.births[inxedval].name, 'category_id': $scope.births[inxedval].category_id, 'day': $scope.births[inxedval].day, 'month': $scope.births[inxedval].month, 'status': $scope.births[inxedval].status});
                } else if ($scope.category == 38) {
                    $scope.exitDeathEvent.push({'event': $scope.deaths[inxedval].eventWiki, 'year': $scope.deaths[inxedval].year, 'title': $scope.deaths[inxedval].title, 'name': $scope.deaths[inxedval].name, 'category_id': $scope.deaths[inxedval].category_id, 'day': $scope.deaths[inxedval].day, 'month': $scope.deaths[inxedval].month, 'status': $scope.deaths[inxedval].status});
                } else {
                    $scope.exitHolidayEvents.push({'event': $scope.holidays[inxedval].eventWiki, 'year': $scope.holidays[inxedval].year, 'title': $scope.holidays[inxedval].title, 'name': $scope.holidays[inxedval].name, 'category_id': $scope.holidays[inxedval].category_id, 'day': $scope.holidays[inxedval].day, 'month': $scope.holidays[inxedval].month, 'status': $scope.holidays[inxedval].status});

                }
                var eId = data.event_id;
                var link_id = data.result.insertId;
                var totalUrls = data.urlsName;
                var urlsName = data.urlsName;
                var index_val = data.indexR;
                console.log('totalUrls', totalUrls.length, eId, index_val, link_id)
                for(var c=0; c<totalUrls.length; c++) {
                    var url = totalUrls[c].url;
                    var noImgUrl = totalUrls[c].url;
                    var urldata = {'url': decodeURIComponent(url), 'count': c, 'link_id': link_id, 'index': index_val, 'image_index': c};
                    console.log('urldata', urldata)
                    $http.post('/linkImage', urldata).
                        success(function (dataR, status, headers, config) {
                            var image_index_val = dataR.image_index;
                            var index_val = data.indexR;
                            console.log('>>>>>>>',index_val,image_index_val)
                            if (typeof dataR.sUrl == 'undefined') {
                                var event = totalUrls[0].name;
                                var url = totalUrls[0].url;
                                var event_id = data.event_id;
                                var dataa = {"url": dataR.curl }
                                console.log('dasdas',dataa , event_id)
                                $http.post('/noImageLink', dataa).
                                    success(function (result1) {
                                        if (typeof result1.result != 'undefined') {
                                            var linkid = result1.result.insertId;
                                        } else {
                                            var linkid = result1.link_id;
                                        }
                                        var newdata = {'event_id': event_id, "linkId": linkid}
                                        $http.post('/connectLinkEvent', newdata).
                                            success(function (result2) {
                                                console.log('image_index_val', image_index_val)
                                                if ($scope.category == 36) {
                                                    $('.' + index_val + 'dEloader').hide();
                                                    $scope.eventsE.splice(index_val, 1);
                                                } else if ($scope.category == 37) {
                                                    if (image_index_val == 0) {
                                                        $('.' + index_val + 'dBloader').hide();
                                                        $scope.births.splice(index_val, 1);
                                                    }
                                                } else if ($scope.category == 38) {
                                                    if (image_index_val == 0) {
                                                        $('.' + index_val + 'dDloader').hide();
                                                        $scope.deaths.splice(index_val, 1);
                                                    }
                                                } else {
                                                    if (image_index_val == 0) {
                                                        $('.' + index_val + 'dHloader').hide();
                                                        $scope.holidays.splice(index_val, 1);
                                                    }
                                                }

                                            });
                                    });

                            } else {
                                console.log('Url available')
                                var dataVal = {'url':decodeURIComponent(dataR.curl),'image': dataR.sUrl,'name':data.urlsName[dataR.count].name, 'counter':dataR.count, 'eventId': eId };
                                console.log('dataVal', dataVal)
                                     $http.post('/addLink1', dataVal).
                                        success(function(result) {
                                            if (typeof result.res == 'undefined') {
                                                var dataa = {'event_id': result.event_id, "linkId": result.link_id}
                                            } else {
                                                var dataa = {'linkId': result.res.insertId, 'event_id': result.event_id};
                                            }
                                            var imageval = 0;
                                            console.log('index_val cate', index_val , image_index_val , $scope.category)
                                             if ($scope.category == 36) {
                                                 //if(image_index_val == 0){
                                                   //  $('.' + index_val + 'dEloader').hide();
                                                    $scope.eventsE.splice(index_val, 1);
                                                    $scope.eventsE = [];
                                                // }
                                             } else if ($scope.category == 37) {
                                                // if (image_index_val == 0) {
                                                   //  $('.'+index_val+'loader-birth').hide();
                                                     $scope.births.splice(index_val, 1);
                                                     $scope.births =[];
                                               //  }
                                             } else if ($scope.category == 38) {
                                               //  if (image_index_val == 0) {
                                                   //  $('.'+index_val+'loader-death').hide();
                                                     $scope.deaths.splice(index_val, 1);
                                                     $scope.deaths = [];
                                                // }
                                             } else {
                                               //  if (image_index_val == 0) {
                                                     //$('.'+index_val+'loader-holiday').hide();
                                                     $scope.holidays.splice(index_val, 1);
                                                     $scope.holidays = [];
                                                // }
                                             }
                                             $http.post('/eventLink', dataa).
                                                 success(function(res){
                                                     console.log('Done')
                                                 }).
                                                 error(function(err){
                                                     console.log('Getting error');
                                                 });
                                             console.log('Timer --> ', timer, tUrls)
                                             if(timer == tUrls) {
                                                 if(flag != 1 ) {
                                                     $http.post('/addImagesUrls', dataVal).
                                                         success(function(result){
                                                             console.log(result)
                                                         });
                                                       // $('#savebtn').hide();
                                                       // $('#savebtnstp').show();
                                                       // $('#saveup').hide();
                                                       // $('#savedupstop').show();
                                                     console.log('Final Timer called --> ', timer, tUrls)
                                                     flag = 1;
                                                 }
                                             }


                                        }).error(function(err){
                                            console.log(err)
                                        });

                            }// not undefined
                            timer++;
                        });
                }
            }); // updateEvent
    } // main for
}

//Delete any existing event
$scope.delExistEvent = function(even,$index,category,event,eventid){
  //console.log($(even.currentTarget).parent().prev().parent());
  var category = category;
  var Existevent  = event;
  var eventid = eventid;
  var eventInfo={
            eventid: eventid,
            category:category,
            event:Existevent,
        };

  $http({method: 'POST', url: '/deleteExistEvents',data:eventInfo}).
        success(function(data, status, headers, config) {
          console.log("data",data);
          if(data.message == "DONE"){
            //alert("successfully Deleted");
            $(even.currentTarget).parent().prev().parent().hide();
          }
        }).
        error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });


  };

  $scope.daySelect = function(){
    $scope.monthNum = $scope.selectedMonth+1;
    if($scope.monthNum == 2){
      var totaldays = 29;
    }
    else{
      var totaldays = new Date(year, $scope.monthNum, 0).getDate();
    }
    $scope.dates = [];
    for (var i=1; i<=totaldays; i++){
      $scope.dates.push({'Name': i , 'id': i});
    }
  };

  $scope.seelctedTab = function(category){
    save_event_type = 'single';
    $scope.set_category = category;
  };
  $scope.showExitEvents = function(){
    $('.exit-event').show();
  };


}




