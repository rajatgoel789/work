'use strict';

/* Links Controller */

function linksCtrl($scope, $http, $location, $routeParams, limitToFilter) {
    var jcrop_api,
        boundx,
        boundy,

    $preview = $('#preview-pane'),
    $pcnt = $('#preview-pane .img_box'),
    $pimg = $('#preview-pane .img_box img'),
    xsize = $pcnt.width(),
    ysize = $pcnt.height();
    var temp = [];
   $scope.selected = function(x) {
    // console.log("selected", x);
    $scope.$apply(function(){
      $scope.x1 = x.x;
      $scope.y1 = x.y;
      var selHeight = x.h;
      var selWidth = x.w;
      $scope.w = parseInt(selWidth.toFixed(0));
      $scope.h = parseInt(selHeight.toFixed(0));
      if($scope.w > 2040) {
        var nh = $scope.calculateAspectRatioFit(2040);
        $scope.nW = parseInt(2040);
        $scope.nH = parseInt(2040);
        if(typeof nh.nheight != 'undefined') {
          $scope.nW = parseInt(2040);
          $scope.nH = parseInt(nh.nheight);
        }
        else {
          $scope.nW = parseInt(nh.width);
          $scope.nH = parseInt(nh.height);
        }

      }else{
        $scope.nW = parseInt(selWidth.toFixed(0));
        $scope.nH = parseInt(selHeight.toFixed(0));        
      }

      if(x.w < 641) {
          $scope.mw = parseInt($scope.w);
          $scope.mw1 = parseInt($scope.w);
          $scope.mh1 = parseInt($scope.h);
      }else {
        var mh = $scope.calculateAspectRatioFit(640);
        $scope.mw = parseInt(640);
        if(typeof mh.nheight != 'undefined') {
          $scope.mw1 = parseInt(640);
          $scope.mh1 = parseInt(mh.nheight);
        }
        else {
          $scope.mw1 = parseInt(mh.width);
          $scope.mh1 = parseInt(mh.height);
        }
      }
      if(x.w < 241) {
        $scope.sw = parseInt(x.w);
        $scope.sw1 = parseInt(x.w);
        $scope.sh1 = parseInt(x.h);
      }
      else {
        var sh = $scope.calculateAspectRatioFit(240);
        $scope.sw = parseInt(240);
        if(typeof sh.nheight != 'undefined') {
          $scope.sw1 = parseInt(240);
          $scope.sh1 = parseInt(sh.nheight);
        }else{
           $scope.sw1 = parseInt(sh.width);
           $scope.sh1 = parseInt(sh.height);
        }
      }
    });
    if (parseInt(x.w) > 0) {
        var rx = xsize / x.w;
        var ry = ysize / x.h;
        boundx = $('#wid').val();
        boundy = $('#hei').val();
        $pimg.css({
          width: Math.round(rx * boundx) + 'px',
          height: Math.round(ry * boundy) + 'px',
          marginLeft: '-' + Math.round(rx * x.x) + 'px',
          marginTop: '-' + Math.round(ry * x.y) + 'px'
        });
      }
  };

  $scope.calculateAspectRatioFit = function(max_size){
      var arr = [];
      if ($scope.h > $scope.w) {
          var h = max_size;
          var w = Math.ceil($scope.w / $scope.h * max_size);
          var nh = Math.ceil($scope.h / $scope.w * max_size);
          arr['nheight'] = nh;
          arr['width'] = w;
      }
      else{
          var w = max_size;
          var h = Math.ceil($scope.h / $scope.w * max_size);
          arr['height'] = h;
          arr['width'] = w;
      }
      // console.log(arr);
      return arr;
  };

  $('.upload1 , .upload2').hide();
  if($routeParams.id){
    var dataId = {'id':$routeParams.id};
    console.log(dataId);
      $http.post('/linkDataa', dataId).
      success(function(data, status, headers, config) {
        console.log(data[0].name);
        $scope.linkID = data[0].id;
        $scope.title = data[0].name;
        $scope.images = data[0].image_url;
        $('.link_bx').show();
        if($scope.images){
          $('.inp_rgt , .systemFile').hide();
        }else{
          $('.inp_rgt , .systemFile').show()
        }
        $scope.asyncSelected = data[0].url;
      });
    }

  $scope.onFileSelect = function($file){
    $('.urlimage , .direct-url-image').hide();
    $('.base64-image').show();
    var reader = new FileReader();
    reader.onload = function (e) {
      var image = new Image();
      var nWidth;
      var jcrop_api;
      var nHeight;
      image.src = e.target.result;
      image.onload = function() {
        $('.imageMap, .img img ,img-cropped').attr('src', this.src);
        $('.jcrop-holder div div img').attr('src', this.src);
        if(this.width > 800){
          nWidth = 800;
          nHeight = 600;
        }
        else {
          nWidth = this.width;
          nHeight = this.height;
        }
        $("#fwidth").text(this.width);
        $("#fheight").text(this.height);
        $scope.images =  this.src;
        $('.img img').Jcrop({
          onSelect:   $scope.selected
        });
        $('.jcrop-holder img, .jcrop-holder, .jcrop-tracker').css({height: nHeight+'px', width: nWidth+'px'});
        $(".imageMap").css({width:'172px', height:'172px', margin:'0px'});
      };
    }
    // image/gif
    if($file[0].type == "image/gif") {
      alert(".gif image not Support!")
    }
    else {
      $scope.images =  $file[0];
      reader.readAsDataURL($file[0]);
    }
  };


  $scope.directUrlImage = function(){
    $('.base64-image').hide();
    $('.urlimage , .direct-url-image').show();
  };
  $scope.directUrlImg = function(){
    $('.imageMap').attr('src', $scope.dImageUrl);
    $scope.images = $scope.dImageUrl;
    $('.imageMap').attr('src', $scope.images);
    $('.jcrop-holder div div img').attr('src', $scope.images);
    $(".imageMap").css({width:'172px', height:'172px', margin:'0px'});
    var image = new Image();
    image.src = $scope.dImageUrl;
    image.onload = function() {
       $("#fwidth").text(this.width);
       $("#fheight").text(this.height);
    }
  };
  $scope.getUrlData = function(){
    var url = $scope.asyncSelected;
    console.log(url);
    if (typeof url == 'undefined'){
    $(".url-error").show();
    $('.url-error').delay(3000).fadeOut();
     return false;
    }
    $('.base64-image , .direct-url-image, .wait-wiki-main').hide();
    $('.urlimage, .loadImg, .wait-wiki').show();
    $scope.images = "";
    $(".imageMap").css({height:'172px',width:'172px','margin':'0px'})
    var data = {'url':url};
    $http.post('/linkImage', data).
    success(function(data, status, headers, config) {
      console.log("imageUrl" , data.sUrl );
      if( typeof data.sUrl != 'undefined'){
          var imgU = data.sUrl;
          var fileExt = imgU.split('.').pop();
          if(fileExt=='svg')
            $scope.images = 'http:'+data.mnUrl;
          else
            $scope.images = 'http:'+data.sUrl;
      }
      else {
        alert("Image Not Found");
      }
      $('.imageMap').attr('src', $scope.images);
      $('.jcrop-holder div div img').attr('src', $scope.images);
      $('.link_bx, .wait-wiki-main').show();
      $('.success-image, .loadImg, .wait-wiki').hide();
      var image = new Image();
      image.src = $scope.images;
      image.onload = function() {
         $("#fwidth").text(this.width);
         $("#fheight").text(this.height);
      }
    }).error(function (data, status, headers, config) {
        alert("Image Not Found");
        $('.loadImg').hide();
    });
  };

  $scope.getLocation = function(linkName) {
    return $http.get("/search/"+linkName).then(function(res){
      var links = []
      angular.forEach(res.data, function(item){
        links.push(item);
      });
      return links;
    });
  };

  $scope.onSelect = function ($item) {
    $(".imageMap").css({width:'172px', height:'172px', margin:'0px'});
    $scope.linkID = $item.id;
    $scope.title = $item.name;
    $scope.images = $item.image_url;
    $scope.full =   decodeURIComponent($item.image_url);
    var image = new Image();
    image.src = $scope.images;
    image.onload = function() {
      $("#fwidth").text(this.width);
      $("#fheight").text(this.height);
    }
    $scope.medium = $item.medium_image_url;
    $scope.small = $item.small_image_url;
    $('.imageMap').attr('src', $scope.images);
    $('.jcrop-holder div div img').attr('src', $scope.images);
    $('.link_bx').show();
    if($scope.images){
      $('.inp_rgt , .systemFile , .success-image' ).hide();
    }else{
      $('.inp_rgt , .systemFile').show()
    }
 };
  $scope.imageUpload = function(imageUrl){
    if( typeof imageUrl == "undefined" || imageUrl == null) {
      $(".url-error").show();
      $('.url-error').delay(3000).fadeOut();
      return false;
    }
    $(".urlimage").hide();
    $(".wait-process").show();
    var svgw = $("#wid").val();
    var data = {'url':imageUrl, 'x1':$scope.x1,'y1':$scope.y1,'w':$scope.w, 'h':$scope.h , 'mSize':$scope.mw,'sSize':$scope.sw ,'svgW': svgw };
    console.log("Data:" , data);
    $http.post('/uploadImage', data).
      success(function(data, status, headers, config) {
        $scope.uploadedImage = data.image;
        var path = data.image;
        var imageName = path.match(/.*\/([^/]+)\.([^?]+)/i)[1];
        var mediumImg = "https://s3.amazonaws.com/today_in_history/"+imageName+"_medium.png";
        var smallImg  = "https://s3.amazonaws.com/today_in_history/"+imageName+"_small.png";
        var data = {'imageUrl':$scope.uploadedImage,'sImg':smallImg, 'mImg':mediumImg ,'id':$scope.linkID};
        $http.post('/saveLink', data).
          success(function(data, status, headers, config) {
            $scope.uploadedImage = data;
            $scope.full = path;
            $scope.medium = mediumImg;
            $scope.small = smallImg;
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $('.error').show().text('You have updated Link successfully').delay(2500).hide(0);
            $('.upldbtn, .urlimage').show();
            $('.success-image, .wait-process').hide();
            $scope.w = undefined;
            $scope.h = undefined;
          });
      });
  };

  $scope.imageUploadd = function(imageUrl){
    var url = $scope.asyncSelected;
    //alert(url);
    if( typeof url == "undefined") {
     $(".url-error").show();
     $('.url-error').delay(3000).fadeOut();
      return false;
    }
    var image = $('.imageMap').attr('src');
    // var data = {'image':image};
    var data = {'image':image, 'x1':$scope.x1,'y1':$scope.y1, 'w':$scope.w, 'h':$scope.h ,'mSize':$scope.mw,'sSize':$scope.sw  };
    console.log("dasdsdasdasd" ,data);
    $http.post('/base64/image', data).
      success(function(data, status, headers, config) {
        $scope.uploadedImage = data.image;
        $('.upldbtn').hide();
        $('.success-image').show();
        var path = data.image;
        var imageName = path.match(/.*\/([^/]+)\.([^?]+)/i)[1];
        var mediumImg = "https://s3.amazonaws.com/today_in_history/"+imageName+"_medium.png";
        var smallImg  = "https://s3.amazonaws.com/today_in_history/"+imageName+"_small.png";
        var data = {'imageUrl':$scope.uploadedImage,'sImg':smallImg, 'mImg':mediumImg ,'id':$scope.linkID};
        $http.post('/saveLink', data).
          success(function(data, status, headers, config) {
            $scope.uploadedImage = data;
            $scope.full = path;
            $scope.medium = mediumImg;
            $scope.small = smallImg;
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $('.error').show().text('You have updated Link successfully').delay(2500).hide(0);
            $('.upldbtn').show();
            $('.success-image').hide();
            console.log(data);
            $scope.w = undefined;
            $scope.h = undefined;
          });
      });
  };

  $scope.addLinkData = function(){
    console.log('title',$scope.title);
    console.log('images',$scope.images);
    console.log('url',$scope.uploadedImage);
    console.log('id',$scope.linkID);
    var data = {'imageUrl':$scope.uploadedImage,'id':$scope.linkID};
    $http.post('/saveLink', data).
      success(function(data, status, headers, config) {
        $scope.uploadedImage = data;
        $("html, body").animate({ scrollTop: 0 }, "slow");
        $('.error').show().text('You have updated Link successfully').delay(2500).hide(0);
        $('.upldbtn').show();
        $('.success-image').hide();
        console.log(data);
      });
  };
}
