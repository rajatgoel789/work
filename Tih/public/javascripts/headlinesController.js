'use strict';

/* HeadLine Controller */
function headlinesCtrl($scope, $http , $location , $routeParams){
  var d = new Date();
  if($routeParams.month && $routeParams.day){
    var day = parseInt($routeParams.day);
    var month = parseInt($routeParams.month)-1;
  }
  else{
    var day = d.getDate();
    var month = d.getMonth();
  }
  var year = d.getFullYear();
  $scope.monthNum = month+1;
  var totaldays = new Date(year, $scope.monthNum, 0).getDate();
  $scope.selectedHead = [];
  $scope.selectedMonth = month;
  $scope.selectedDate = day;
  $scope.allEvent = [];
  $scope.headlineEvent = {};
  $scope.link_id = {};
  $scope.orderBy = 'DESC';
  $scope.dailyQuote ;
  $scope.events = [];
  $scope.eventType = 36;

  //Array Of All Months
  $scope.Months = [{ Name: 'January', id: 0 },{ Name: 'February', id: 1 },{ Name: 'March', id: 2 },{ Name: 'April', id: 3 },
      { Name: 'May', id: 4 },{ Name: 'June', id: 5 },{ Name: 'July', id: 6 },{ Name: 'August', id: 7 },{ Name: 'September', id: 8 },
      { Name: 'October', id: 9 },{ Name: 'November', id: 10 },{ Name: 'December', id: 11 }];

  //Days Array From 1 to 31
  $scope.dates = [];
  for (var i=1; i<=totaldays; i++){
    $scope.dates.push({'Name': i , 'id': i});
  }
var data1 = {'day':$scope.selectedDate,'month':$scope.selectedMonth}
$http.post('/quotes' , data1).
    success(function(data, status, headers, config) {
      if(data.length>0){
        $scope.quote = data[0].quote;
        $scope.dailyQuote = data[0].id;
        if(data[0].link_id)
        {
          $http.get('/linkData'+ data[0].link_id).
          success(function(data, status, headers, config) {
            if(data.length>0){
              $('.frm_edit').show();
              $scope.asyncSelected = data[0].name+' , '+data[0].url;
              $scope.link_id = data[0].id;
            }
          });
        }
        else{
          $scope.link_id = '';
        }
      }
      else{
        $scope.link_id = '';
        $scope.quote = '';
        $scope.dailyQuote = '';
      }
    });


  var data = {'day':$scope.selectedDate , 'month':$scope.selectedMonth+1 ,  'order':$scope.orderBy};
  $http.post('/events', data).
    success(function(data, status, headers, config) {
      $scope.allEvent = data;
      for (var i=0; i<data.length; i++){
        if(data[i].is_headline == 1 ){
          var monthName = monthNames[data[i].month];
          if(data[i].category_id == 37 || data[i].category_id == 38 || data[i].category_id == 39){
            var nameData = data[i].name+'-'+data[i].event;
            $scope.selectedHead.push({'event':nameData,'date':data[i].day+' '+monthName +' '+data[i].year , 'id':data[i].id , 'order':data[i].order , 'category_id':data[i].category_id});
          }
          else{
            $scope.selectedHead.push({'event':data[i].event,'date':data[i].day+' '+monthName +' '+data[i].year , 'id':data[i].id , 'order':data[i].order , 'category_id':data[i].category_id});
          }
        }
        else if(data[i].is_headline == 0 && data[i].category_id == 36){
           $scope.events.push(data[i]);
        }
      }
      $scope.selectedHead.sort(sortEvents);
      function sortEvents(val1, val2) {
        var date1, date2;
        date1 = new Date(val1.order);
        date2 = new Date(val2.order);
        return date1 - date2;
      }
      var mnth = data[0].month;
      $scope.month = monthNames[mnth];
    });

  $scope.selectedTab=function(eventType){
    $('.tab-content').hide();
    $('.loader2').show();
    $scope.eventType = eventType;
    $('li').removeClass('active');
    $(".two"+eventType).addClass('active');
    var data = {'day':$scope.selectedDate , 'month':$scope.selectedMonth+1 , 'category' : $scope.eventType , 'order':$scope.orderBy};
    $http.post('/event' , data).
      success(function(data, status, headers, config) {
        $scope.events = [];
        $('.tab-content').show();
        $('.loader2').hide();
        $scope.allEvent = data;
        for (var i=0; i<data.length; i++){
        if(data[i].is_headline == 1){
          console.log('exit');
        }
        else{
          $scope.events.push(data[i]);
        }
      }

      for(var i=0; i<$scope.selectedHead.length; i++){
          for(var k=0; k<$scope.events.length; k++){
            if($scope.events[k].id == $scope.selectedHead[i].id){
              $scope.events.splice(k, 1);
            }
            else{
              console.log('not exit');
            }
          }
        }
        $scope.month = monthNames[data[0].month];
      });
  };

  //ON DROP EVENT FROM EVENT LIST TO HEADLINE BOX
  $scope.$on('my-sorted',function(ev,val){
    $scope.selectedHead.splice(val.to, 0, $scope.selectedHead.splice(val.from, 1)[0]);
  })

  $scope.$on('my-created',function(ev,val){
    var check = 'false';
    var eventText = val.name[0].children[0].childNodes[0].childNodes[0].data;
    var eventDate = val.name[0].children[0].childNodes[1].children[0].childNodes[0].data;
    var str = val.name[0].attributes['eventid'].nodeValue;
    console.log('id', str);
    console.log('text', val);
    console.log('date', eventDate);
    for (var i = 0; i < $scope.selectedHead.length; i++){
      if ($scope.selectedHead[i].id === str) {
        check = 'true';
      }
    }
    if(check == 'false'){
      $scope.selectedHead.push({'event':eventText,'date':eventDate , 'id':str , 'category_id':$scope.eventType});
    }
    $scope.headlineEvent = str;
    for(var i=0; i < $scope.events.length; i++){
      if($scope.events[i].id == $scope.headlineEvent){
        $scope.events.splice(i, 1);
      }
    }
    for(var i = 0; i < $scope.allEvent.length; i++) {
      if($scope.allEvent[i].id == str) {
        $scope.selectedHeadline = $scope.allEvent[i].event;
        $('.headline').click();
      }
    }
  })

  $scope.onSelect = function ($item) {
    $('.frm_edit').show();
    $scope.link_id = $item.id;
    $scope.asyncSelected = $item.name+' , '+$item.url;
  };

  $scope.daySelect = function(){
    $scope.selectedDate = 1;
  };

  $scope.deleteLinkk = function(index){
    for(var i=0; i<$scope.allEvent.length; i++){
      if($scope.allEvent[i].id == $scope.selectedHead[index].id && $scope.allEvent[i].category_id == $scope.eventType){
        $scope.events.push({'name':$scope.allEvent[i].name, 'event':$scope.allEvent[i].event, 'day':$scope.allEvent[i].day,'month':$scope.allEvent[i].month,'year':$scope.allEvent[i].year,'id':$scope.allEvent[i].id});
      }
    }
    $scope.selectedHead.splice(index, 1);
    $scope.events.sort(sortEvents);
    function sortEvents(val1, val2){
      var date1, date2;
      date1 = new Date(val1.year);
      date2 = new Date(val2.year);
      if($scope.orderBy == 'DESC')
        return date2 - date1;
      else
        return date1-date2;
    }
  };

  $scope.getLocation = function(linkName) {
    return $http.get("/search/"+linkName).then(function(res){
      var links = []
      angular.forEach(res.data, function(item){
        links.push(item);
      });
      return links;
    });
  };

  $scope.selectHeadline = function(id) {
    $scope.headlineEvent = $scope.events[id].id;
    $scope.selectedHeadline = $scope.events[id].id
    var day = $scope.events[id].day;
    var mnth = monthNames[$scope.events[id].month];
    var year = $scope.events[id].year;
    $scope.duplicasyCheck = 'false';
    if($scope.selectedHead.length > 0){
      for (var i = 0; i < $scope.selectedHead.length; i++){
        if ($scope.selectedHead[i].id === $scope.selectedHeadline) {
          $scope.duplicasyCheck = 'true';
        }
      }
    }
    if($scope.duplicasyCheck == 'false'){
      if($scope.events[id].category_id == 39 ){
        var date = day+' '+mnth;
      }else{
        var date = day+' '+mnth +' '+year;

      }
      if($scope.events[id].category_id == 37 || $scope.events[id].category_id == 38){
        var eventDa = $scope.events[id].name+' - '+$scope.events[id].event;
      }else{
        var eventDa = $scope.events[id].event;
      }
      $scope.selectedHead.push({'event':eventDa,'date':date , 'id':$scope.selectedHeadline , 'categiry_id':$scope.events[id].category_id});
    }
    for(var i=0; i<$scope.events.length; i++){
      if($scope.events[i].id == $scope.selectedHeadline){
        $scope.events.splice(i, 1);
      }
    }
  };

  $scope.getEventBySelectedDate = function(){
    $('.tab-content').hide();
    $('.loader2').show();
    $scope.monthNum = $scope.selectedMonth+1;
    var totaldays = new Date(year, $scope.monthNum, 0).getDate();
    $scope.dates = [];
    $scope.events = [];
    for (var i=1; i<=totaldays; i++){
        $scope.dates.push({'Name': i , 'id': i});
    }
    $('li').removeClass('active');
    $(".two"+$scope.eventType).click();
    $scope.selectedHead = [];
    $('.frm_edit').hide();
    var data = {'day':$scope.selectedDate , 'month':$scope.selectedMonth+1 , 'order':$scope.orderBy};
    var data1 = {'day':$scope.selectedDate,'month':$scope.selectedMonth}
    $http.post('/quotes' , data1).
    success(function(data, status, headers, config) {
      if(data.length>0){
        $scope.quote = data[0].quote;
        $scope.dailyQuote = data[0].id;
        if(data[0].link_id)
        {
          $http.get('/linkData'+ data[0].link_id).
          success(function(data, status, headers, config) {
           if(data.length>0){
            $('.frm_edit').show();
            $scope.asyncSelected = data[0].name+' , '+data[0].url;
            $scope.link_id = data[0].id;
          }
        });
      }
      else{
        $scope.link_id = '';
      }}
      else{
        $scope.quote = '';
        $scope.asyncSelected = '';
        $scope.dailyQuote = '';
      }
    });
    $http.post('/events' , data).
    success(function(data, status, headers, config) {
      $('.tab-content').show();
      $('.loader2').hide();
      $scope.selectedHeadline = '';
      $scope.selectedHead = [];
      $scope.events = [];
      //$scope.asyncSelected = '';
      for (var i=0; i<data.length; i++){
        if(data[i].is_headline == 1){
          var monthName = monthNames[data[i].month];
          $scope.selectedHead.push({'event':data[i].event,'date':data[i].day+' '+monthName +' '+data[i].year , 'id':data[i].id , 'order':data[i].order , 'category_id':data[i].category_id});
        }
        else if(data[i].is_headline == 0 && data[i].category_id == $scope.eventType){
          $scope.events.push(data[i]);
        }
      }
      $scope.allEvent = data;
      $scope.month = monthNames[data[0].month];
      $scope.selectedHead.sort(sortEvents);
      function sortEvents(val1, val2) {
        var date1, date2;
        date1 = new Date(val1.order);
        date2 = new Date(val2.order);
        return date1 - date2;
      }
    });
  };

  $scope.submitForm=function(){
    console.log('selectedHead',$scope.selectedHead);
    var data = {'day':$scope.selectedDate,'month':$scope.selectedMonth,'eventId':$scope.selectedHead};

    $http.post('/headline/event', data).
    success(function(data, status, headers, config){
      for(var i = 0; i<$scope.selectedHead.length; i++){
        var id = $scope.selectedHead[i].id;
      var dataVal = {'day':$scope.selectedDate,'month':$scope.selectedMonth+1,'eventId':id , 'order':i+1};
      $http.post('/headline/eventTrue', dataVal).
    success(function(data, status, headers, config){
      console.log('successs');
    });
  }
    });
    if($scope.dailyQuote){
      var id = $scope.dailyQuote;
    }
    else
    {
      var id = '';
    }
    var quoteData = {'id':id,'day':$scope.selectedDate,'month':$scope.selectedMonth,'quote':$scope.quote,'link_id':$scope.link_id};
    $http.post('/headline/quote', quoteData).
    success(function(data,status,headers,config){
      if(data.insertId){
        $scope.dailyQuote = data.insertId
      }
      console.log('linksdata',data);
      $("html, body").animate({ scrollTop: 0 }, "slow");
      $('.error').show().text('You have updated Headline successfully').delay(2500).hide(0);
    });
  };
  $scope.getEventByOrder=function(){
    $('.tab-content').hide();
    $('.loader2').show();
    $('li').removeClass('active');
    $(".two"+$scope.eventType).click();
    $scope.allEvent = [];
    var data = {'day':$scope.selectedDate , 'month':$scope.selectedMonth+1 , 'category' : $scope.eventType ,'order':$scope.orderBy};
    $http.post('/event' , data).
    success(function(data, status, headers, config) {
      $scope.events = [];
      $('.tab-content').show();
      $('.loader2').hide();
      $scope.allEvent = data;
      for (var i=0; i<data.length; i++){
        if(data[i].is_headline == 1){
          var monthName = monthNames[data[i].month];
        }
        else{
           $scope.events.push(data[i]);
        }
      }

      for(var i=0; i<$scope.selectedHead.length; i++){
          for(var k=0; k<$scope.events.length; k++){
            if($scope.events[k].id == $scope.selectedHead[i].id){
              $scope.events.splice(k, 1);
            }
            else{
              console.log('not exit');
            }
          }
        }
      $scope.month = monthNames[data[0].month];
    });
  };
}