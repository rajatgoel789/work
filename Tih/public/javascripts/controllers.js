'use strict';

/* Event Controller */
var monthNames = [ "" , "January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December" ];

function IndexCtrl($scope, $http , $location , $routeParams) {
  var d = new Date();
if($routeParams.month && $routeParams.day){
    var day = parseInt($routeParams.day);
    var month = parseInt($routeParams.month)-1;
  }
  else{
      var day = d.getDate();
      var month = d.getMonth();
  }
  var year = d.getFullYear();
  $scope.monthNum = month+1;
  var totaldays = new Date(year, $scope.monthNum, 0).getDate();
  $scope.orderBy = 'DESC';
  $scope.selectedMonth = month;
  $scope.selectedDate = day;
  $scope.eventType = 36;
  $scope.dates = [];

  for (var i=1; i<=totaldays; i++){
        $scope.dates.push({'Name': i , 'id': i});
    }

  $scope.Months = [{ Name: 'January', id: 0 },{ Name: 'February', id: 1 },{ Name: 'March', id: 2 },{ Name: 'April', id: 3 },
      { Name: 'May', id: 4 },{ Name: 'June', id: 5 },{ Name: 'July', id: 6 },{ Name: 'August', id: 7 },{ Name: 'September', id: 8 },
      { Name: 'October', id: 9 },{ Name: 'November', id: 10 },{ Name: 'December', id: 11 }];

  // TO GET EVENTS LIST OF CURRENT DATE
  $('.tab-content').hide();
  $('.loader').show();
  var data = {'day':$scope.selectedDate , 'month':$scope.selectedMonth+1 , 'category' : $scope.eventType ,'order':$scope.orderBy};
  $http.post('/event' , data).
    success(function(data, status, headers, config) {
      if(data.length > 0){
        if(data.length == 0){
          $('.tab-content').html('No Data Exit');
        }
        $scope.events = data;
        $scope.month = monthNames[data[0].month];
      }
      $('.tab-content').show();
      $('.loader').hide();
    });

  // ON SELECT EVENT TYPE MAKE HTTP REQUEST TO GET DATA
  $scope.selectedTab=function(eventType){
    $scope.eventType = eventType;
    $('li').removeClass('active');
    $(".two"+eventType).addClass('active');
    $('.tab-content').hide();
    $('.loader').show();
   var data = {'category' : eventType,'day':$scope.selectedDate,'month':$scope.selectedMonth+1,'order':$scope.orderBy};
   $http.post('/event' , data).
    success(function(data, status, headers, config) {
      $('.tab-content').show();
      $('.loader').hide();
      $scope.events = data;
      $scope.month = monthNames[data[0].month];
    });
  };

  $scope.editEvent=function(id){
    $location.path('/event/'+id);
  };

  $scope.getEventByOrder=function(){
    $('li').removeClass('active');
    $(".two"+$scope.eventType).click();
    $scope.allEvent = [];
    var data = {'day':$scope.selectedDate , 'month':$scope.selectedMonth+1 , 'category' : $scope.eventType ,'order':$scope.orderBy};
    $('.tab-content').hide();
    $('.loader').show();
    $http.post('/event' , data).
    success(function(data, status, headers, config) {
      $('.tab-content').show();
      $('.loader').hide();
      $scope.allEvent = data;
      $scope.events = data;
      $scope.month = monthNames[data[0].month];
    });
  };
  $scope.daySelect = function(){
    $scope.selectedDate = 1;
  };

  $scope.getEventBySelectedDate= function(){
    $('.tab-content').hide();
    $('.loader').show();
    $scope.monthNum = $scope.selectedMonth+1;
    var totaldays = new Date(year, $scope.monthNum, 0).getDate();
    $scope.dates = [];
    for (var i=1; i<=totaldays; i++){
        $scope.dates.push({'Name': i , 'id': i});
    }
    $('li').removeClass('active');
    $(".two"+$scope.eventType).click();
    $scope.allEvent = [];
    var data = {'day':$scope.selectedDate , 'month':$scope.selectedMonth+1 , 'category' : $scope.eventType ,'order':$scope.orderBy};
    $http.post('/event' , data).
    success(function(data, status, headers, config) {
      if(data.length > 0) {
          $scope.allEvent = data;
          $scope.events = data;
          $scope.month = monthNames[data[0].month];
      }
      $('.loader').hide();
      $('.tab-content').show();
    });
  };
}
