'use strict';

// Declare app level module which depends on filters, and services
var app = angular.module('myApp', ['ngDragDrop','angularFileUpload','myApp.filters', 'myApp.services', 'myApp.directives' , 'ui.bootstrap' , 'ui']).
  config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider.
      when('/', {
        controller: IndexCtrl,
        templateUrl: 'partials/index'
      }).
      when('/event/:id', {
        templateUrl: 'partials/editEvent',
        controller: EditEventCtrl
      }).
      when('/headlines', {
        templateUrl: 'partials/headline',
        controller: headlinesCtrl
      }).
      when('/headlines/:month/:day', {
        templateUrl: 'partials/headline',
        controller: headlinesCtrl
      }).
      when('/links', {
        controller: linksCtrl,
        templateUrl: 'partials/links'
      }).
      when('/links/:id', {
        controller: linksCtrl,
        templateUrl: 'partials/links'
      }).
      when('/:month/:day', {
        controller: IndexCtrl,
        templateUrl: 'partials/index'
      }).
      when('/new-event', {
        templateUrl: 'partials/editEvent',
        controller: EditEventCtrl
      });

    //$locationProvider.html5Mode(true);
  }]);

/*app.directive('uiDraggable', function () {
            return {
                restrict:'A',
                link:function (scope, element, attrs) {
                    element.draggable({
                        revert:true
                    });
                }
            };
        });

        app.directive('uiDropListener', function () {
            return {
                restrict:'A',
                link:function (scope, eDroppable, attrs) {
                    eDroppable.droppable({
                        drop:function (event, ui) {
                            var fnDropListener = scope.$eval(attrs.uiDropListener);
                            if (fnDropListener && angular.isFunction(fnDropListener)) {
                                var eDraggable = angular.element(ui.draggable);
                                fnDropListener(eDraggable, eDroppable, event, ui);
                            }
                        }
                    });
                }
            };
        });
*/
  app.directive('a', function() {
    return {
        restrict: 'E',
        link: function(scope, elem, attrs) {
            if(attrs.ngClick || attrs.href === '' || attrs.href === '#'){
                elem.on('click', function(e){
                    e.preventDefault();
                    if(attrs.ngClick){
                        scope.$eval(attrs.ngClick);
                    }
                });
            }
        }
   };
});

app.directive('mySortable',function(){
  return {
    link:function(scope,el,attrs){
      el.sortable({
        revert: true
      });
      el.disableSelection();

      el.on( "sortdeactivate", function( event, ui ) {
        var from = angular.element(ui.item).scope().$index;
        var to = el.children().index(ui.item);
        if(to>=0){
          scope.$apply(function(){
            if(from>=0){
              scope.$emit('my-sorted', {from:from,to:to});
            }else{
              scope.$emit('my-created', {to:to, name:ui.item});
              ui.item.remove();
            }
          })
        }
      } );
    }
  }
});

app.directive('myDraggable',function(){
  return {
    link:function(scope,el,attrs){
      el.draggable({
        connectToSortable: attrs.myDraggable,
        helper: "clone",
        revert: "invalid"
    });
    el.disableSelection();
    }
  }
});


app.directive('imgCropped', function() {
  return {
    restrict: 'E',
    replace: true,
    scope: { src:'@', selected:'&' },
    link: function(scope,element, attr) {
      var myImg;
      var clear = function() {
        if (myImg) {
          myImg.next().remove();
          myImg.remove();
          myImg = undefined;
        }
      };
      scope.$watch('src', function(nv) {
        clear();
        if (nv) {
          element.after('<img />');
          myImg = element.next();
          myImg.attr('src',nv);
          $(myImg).Jcrop({
            trackDocument: true,
            boxWidth: 800,
            boxHeight: 600,
            onSelect: function(x) {
              scope.selected({cords: x});
            }
          },function(){
              var boundx,
                  boundy;
              var bounds = this.getBounds();
              $('#wid').val(bounds[0]);
              $('#hei').val(bounds[1]);
           });
        }
      });
      scope.$on('$destroy', clear);
    }
  };
});