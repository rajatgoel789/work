'use strict';

/* Links Controller */

function linksCtrl($scope, $http, $location, $routeParams, limitToFilter) {
    var jcrop_api,
        boundx,
        boundy,

    $preview = $('#preview-pane'),
    $pcnt = $('#preview-pane .img_box'),
    $pimg = $('#preview-pane .img_box img'),
    xsize = $pcnt.width(),
    ysize = $pcnt.height();
   $scope.selected = function(x) {
    console.log("selected", x);
    $scope.$apply(function(){
      $scope.x1 = x.x;
      $scope.y1 = x.y;
      var selHeight = x.h;
      var selWidth = x.w;
      $scope.w = parseInt(selWidth.toFixed(0));
      $scope.h = parseInt(selHeight.toFixed(0));
      if(x.w < 641) {
          $scope.mw = parseInt(x.w);
          $scope.mh = parseInt(x.h);
      }else {
        var mw = x.w / 1.5,
            mh = x.h / 1.5;
        $scope.mw = parseInt(640);
        $scope.mh = "Auto"; // parseInt(mh.toFixed(0));
      }
      if(x.w < 241) {
        $scope.sw = parseInt(x.w);
        $scope.sh = parseInt(x.h);
      }
      else {
        $scope.sw = parseInt(240);
        var sh    = x.h / 2.1;
        $scope.sh = "Auto"; // parseInt(sh);
      }
    });
    if (parseInt(x.w) > 0) {
        var rx = xsize / x.w;
        var ry = ysize / x.h;
        boundx = $('#wid').val();
        boundy = $('#hei').val();
        $pimg.css({
          width: Math.round(rx * boundx) + 'px',
          height: Math.round(ry * boundy) + 'px',
          marginLeft: '-' + Math.round(rx * x.x) + 'px',
          marginTop: '-' + Math.round(ry * x.y) + 'px'
        });
      }
  };


  $('.upload1 , .upload2').hide();
  if($routeParams.id){
    var dataId = {'id':$routeParams.id};
    console.log(dataId);
      $http.post('/linkDataa', dataId).
      success(function(data, status, headers, config) {
        console.log(data[0].name);
        $scope.linkID = data[0].id;
        $scope.title = data[0].name;
        $scope.images = data[0].image_url;
        $('.link_bx').show();
        if($scope.images){
          $('.inp_rgt , .systemFile').hide();
        }else{
          $('.inp_rgt , .systemFile').show()
        }
        $scope.asyncSelected = data[0].url;
      });
    }

  $scope.onFileSelect = function($file){
    $('.urlimage , .direct-url-image').hide();
    $('.base64-image').show();
    var reader = new FileReader();
    reader.onload = function (e) {
      var image = new Image();
      var nWidth;
      var jcrop_api;
      var nHeight;
      image.src = e.target.result;
      image.onload = function() {
        $('.imageMap, .img img ,img-cropped').attr('src', this.src);
        $('.jcrop-holder div div img').attr('src', this.src);
        if(this.width > 800){
          nWidth = 800;
          nHeight = 600;
        }
        else {
          nWidth = this.width;
          nHeight = this.height;
        }
        $("#fwidth").text(this.width);
        $("#fheight").text(this.height);
        $(".jcrop-holder div").remove();
        $('.img img').Jcrop({
          onSelect:   $scope.selected
        });
        $scope.images =  this.src;
        $('.jcrop-holder img, .jcrop-holder, .jcrop-tracker').css({height: nHeight+'px', width: nWidth+'px'});
        $(".imageMap").css({width:'172px', height:'172px', margin:'0px'});
      };
    }
    reader.readAsDataURL($file[0]);
  };


  $scope.directUrlImage = function(){
    $('.base64-image').hide();
    $('.urlimage , .direct-url-image').show();
  };
  $scope.directUrlImg = function(){
    $('.imageMap').attr('src', $scope.dImageUrl);
    $scope.images = $scope.dImageUrl;
    $('.imageMap').attr('src', $scope.images);
    $('.jcrop-holder div div img').attr('src', $scope.images);
    $(".imageMap").css({width:'172px', height:'172px', margin:'0px'});
    var image = new Image();
    image.src = $scope.dImageUrl;
    image.onload = function() {
       $("#fwidth").text(this.width);
       $("#fheight").text(this.height);
    }
  };
  $scope.getUrlData = function(){
  	$('.base64-image , .direct-url-image').hide();
    $('.urlimage, .loadImg').show();
  	var url = $scope.asyncSelected;
    var data = {'url':url};
    $http.post('/linkImage', data).
    success(function(data, status, headers, config) {
      console.log(data.sUrl);
      if(data.sUrl)
        $scope.images = 'http:'+data.sUrl;
      else
        alert("Image Not Found");

      $('.imageMap').attr('src', $scope.images);
      $('.jcrop-holder div div img').attr('src', $scope.images);
      $('.link_bx').show();
      $('.success-image, .loadImg').hide();
      var image = new Image();
      image.src = $scope.images;
      image.onload = function() {
         $("#fwidth").text(this.width);
         $("#fheight").text(this.height);
        // $scope.images = $scope.images;
      }
      console.log('images',$scope.images);
    });
  };

  $scope.getLocation = function(linkName) {
    return $http.get("/search/"+linkName).then(function(res){
      var links = []
      angular.forEach(res.data, function(item){
        links.push(item);
      });
      return links;
    });
  };

  $scope.onSelect = function ($item) {
    console.log('link', $item);
    $(".imageMap").css({width:'172px', height:'172px', margin:'0px'});
    $scope.linkID = $item.id;
    $scope.title = $item.name;
    $scope.images = $item.image_url;
    $scope.full =   decodeURIComponent($item.image_url);
    var image = new Image();
    image.src = $scope.images;
    image.onload = function() {
      $("#fwidth").text(this.width);
      $("#fheight").text(this.height);
    }
    $scope.medium = $item.medium_image_url;
    $scope.small = $item.small_image_url;
    $('.imageMap').attr('src', $scope.images);
    $('.jcrop-holder div div img').attr('src', $scope.images);
    $('.link_bx').show();
    if($scope.images){
      $('.inp_rgt , .systemFile , .success-image' ).hide();
    }else{
      $('.inp_rgt , .systemFile').show()
    }
 };
  $scope.imageUpload = function(imageUrl){
    var data = {'url':imageUrl, 'x1':$scope.x1,'y1':$scope.y1,'w':$scope.w, 'h':$scope.h , 'mSize':$scope.mw,'sSize':$scope.sw };
    console.log("message" , data);
    $http.post('/uploadImage', data).
      success(function(data, status, headers, config) {
        $scope.uploadedImage = data.image;
        var path = data.image;
        var imageName = path.match(/.*\/([^/]+)\.([^?]+)/i)[1];
        var mediumImg = "https://s3.amazonaws.com/today_in_history/"+imageName+"_medium.jpg";
        var smallImg  = "https://s3.amazonaws.com/today_in_history/"+imageName+"_small.jpg";
        var data = {'imageUrl':$scope.uploadedImage,'sImg':smallImg, 'mImg':mediumImg ,'id':$scope.linkID};
        $http.post('/saveLink', data).
          success(function(data, status, headers, config) {
            $scope.uploadedImage = data;
            $scope.full = path;
            $scope.medium = mediumImg;
            $scope.small = smallImg;
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $('.error').show().text('You have updated Link successfully').delay(2500).hide(0);
            $('.upldbtn').show();
            $('.success-image').hide();
            console.log(data);
          });
      });
  };

  $scope.imageUploadd = function(imageUrl){
    var image = $('.imageMap').attr('src');
    // var data = {'image':image};
    var data = {'image':image, 'x1':$scope.x1,'y1':$scope.y1, 'w':$scope.w, 'h':$scope.h ,'mSize':$scope.mw,'sSize':$scope.sw  };
    console.log("dasdsdasdasd" ,data);
    $http.post('/base64/image', data).
      success(function(data, status, headers, config) {
        $scope.uploadedImage = data.image;
        $('.upldbtn').hide();
        $('.success-image').show();
        var path = data.image;
        var imageName = path.match(/.*\/([^/]+)\.([^?]+)/i)[1];
        var mediumImg = "https://s3.amazonaws.com/today_in_history/"+imageName+"_medium.jpg";
        var smallImg  = "https://s3.amazonaws.com/today_in_history/"+imageName+"_small.jpg";
        var data = {'imageUrl':$scope.uploadedImage,'sImg':smallImg, 'mImg':mediumImg ,'id':$scope.linkID};
        $http.post('/saveLink', data).
          success(function(data, status, headers, config) {
            $scope.uploadedImage = data;
            $scope.full = path;
            $scope.medium = mediumImg;
            $scope.small = smallImg;
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $('.error').show().text('You have updated Link successfully').delay(2500).hide(0);
            $('.upldbtn').show();
            $('.success-image').hide();
            console.log(data);
          });
        console.log(data);
      });
  };

  $scope.addLinkData = function(){
    console.log('title',$scope.title);
    console.log('images',$scope.images);
    console.log('url',$scope.uploadedImage);
    console.log('id',$scope.linkID);
    var data = {'imageUrl':$scope.uploadedImage,'id':$scope.linkID};
    $http.post('/saveLink', data).
      success(function(data, status, headers, config) {
        $scope.uploadedImage = data;
        $("html, body").animate({ scrollTop: 0 }, "slow");
        $('.error').show().text('You have updated Link successfully').delay(2500).hide(0);
        $('.upldbtn').show();
        $('.success-image').hide();
        console.log(data);
      });
  };
}
