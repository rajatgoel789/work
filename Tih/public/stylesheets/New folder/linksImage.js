var fs = require('fs');
var AWS = require('aws-sdk');
var request= require('request');
var easyimg = require('easyimage');
var sizeOf = require('image-size');

var accessKeyId =  process.env.AWS_ACCESS_KEY || "xxxxxx";
var secretAccessKey = process.env.AWS_SECRET_KEY || "+xxxxxx+B+xxxxxxx";
AWS.config.update({
  accessKeyId: 'AKIAJUC5LOYP6CUM5FGQ',
  secretAccessKey: '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'
});

var s3 = new AWS.S3();

var connection;
// callback
 var myCallback = function(connection) {
   connection.connect(function(err) {
    if(err) {
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000);
    }
  });
   connection.on('error', function(err) {
    if(err.code === 'PROTOCOL_CONNECTION_LOST') {
      console.log('links enter');
      usingItNow(myCallback);
    } else {
      throw err;
    }
  });
 };
 var usingItNow = function(callback) {
  connection = require('../db.js').handleDisconnect();
   callback(connection);
 };
 usingItNow(myCallback);

//TO GET THE LIST OF ALL EVENTS LIST
exports.saveLinkData = function(req, res){
  var id = req.body.id;
  var image_url = req.body.imageUrl;
  var mImg = req.body.mImg;
  var sImg = req.body.sImg;
  var sql = 'UPDATE links SET image_url = ?, medium_image_url = ?, small_image_url= ? WHERE id ='+id;
  connection.query(sql, [image_url, mImg, sImg], function(err, events) {
    if (err) throw err;
    else{
      res.json(events);
    }
  });
};

exports.getLinkData = function(req, res){
    //console.log('r', req.body.name);
  var id = req.body.id;
  var sql = 'SELECT * FROM links WHERE id = '+id;

  connection.query(sql,  function(err, events) {
    if (err) throw err;
    else{
      res.json(events);
    }
  });
};


exports.uploadImage = function(req, res) {
  var imageUrl = req.body.url;
  var r_width;
  var download = function(uri, callback){
    request.head(uri, function(err, res, body){
      console.log('content-type:', res.headers['content-type']);
      console.log('content-length:', res.headers['content-length']);
      request(uri).pipe(fs.createWriteStream('./public/uploads/img.jpg')).on('close', callback);
    });
  };

  var cropImg = function(callback) {
    console.log('w:',req.body.w,", h:",req.body.h,",x:",req.body.x1, ',y:',req.body.y1);
      easyimg.crop({
            src:'./public/uploads/img.jpg', dst:'./public/uploads/img.jpg',
            cropwidth:req.body.w, cropheight:req.body.h,
            gravity:'NorthWest',
            x:req.body.x1, y:req.body.y1
        },
        function(err, stdout, stderr) {
            if (err) throw err;
            console.log('Cropped');
            callback();
        }
      );
  };
  var checkImgDimensions = function(callback){
    sizeOf('./public/uploads/img.jpg', function (err, dimensions) {
      if(typeof dimensions != 'undefined')
        r_width = dimensions.width;
      else
        r_width = 800;
      callback();
    });
  };

  download(imageUrl, function(){
    if(typeof req.body.w != 'undefined') {
        cropImg(function(){
           mainFunction(req.body.mSize, req.body.sSize);
        });
    }
    else {
      checkImgDimensions(function(){
        if(r_width < 640) {
          medium = r_width;
          if(medium > 240)
            small = 240;
          else
            small = r_width/2;
        }
        else {
            medium = 640;
            small = 240;
        }
        console.log("dimensions", medium , small);
        mainFunction(medium, small);
      });
    }
  });

  function mainFunction(medium, small){
    var dateObject = new Date();
    var uniqueId =
      dateObject.getFullYear() + '' +
      dateObject.getMonth() + '' +
      dateObject.getDate() + '' +
      dateObject.getTime();
    var path      = './public/uploads/img.jpg';
    var imgName   = uniqueId+'.png';
    var mediumImg = uniqueId+'_medium'+'.jpg';
    var smallImg  = uniqueId+'_small'+'.jpg';

    // Create Full Size Image and Upload on S3
    fs.readFile(path, function(err, file_buffer){
      var params = {
        Bucket: 'today_in_history',
        Key: imgName,
        Body: file_buffer,
        ContentType:'image/jpeg',
        ACL: 'public-read',
      };
      s3.putObject(params, function (perr, pres) {
        if (perr) {
          console.log("Error uploading data: ", perr);
        } else {
          var result = {'image':'https://s3.amazonaws.com/today_in_history/'+imgName}
          res.json(result);
          console.log('https://s3.amazonaws.com/today_in_history/'+imgName);
        }
      });
      s3.client.getObject(params, function(err, data) {
        if (err) {
          console.log("Error uploading data: ", err);
        } else {
          console.log(data);
        }
      });
    });

    // Create Small Size Image and Upload on S3
    easyimg.resize({
      src: "./public/uploads/img.jpg",
      dst: "./public/uploads/img_small.jpg",
      width: small
    }, function(err, stdout, stderr){
      if (err) throw err;
      var smallPath = "./public/uploads/img_small.jpg";
      fs.readFile(smallPath, function(err, file_buffer){
        var params = {
          Bucket: 'today_in_history',
          Key: smallImg,
          Body: file_buffer,
          ContentType:'image/jpeg',
          ACL: 'public-read',
        };
        s3.putObject(params, function (perr, pres) {
          if (perr) {
            console.log("Error uploading data: ", perr);
          } else {
            var result = {'small_image':'https://s3.amazonaws.com/today_in_history/'+smallImg}
            // res.json(result);
          }
        });
        s3.client.getObject(params, function(err, data) {
          if (err) {
            console.log("Error uploading data: ", err);
          } else {
            console.log(data);
          }
        });
      });
    });

    // Create Medium Size Image and Upload on S3
    easyimg.resize({
      src: "./public/uploads/img.jpg",
      dst: "./public/uploads/img_medium.jpg",
      width: medium
    }, function(err, stdout, stderr){
      if (err) throw err;
      console.log('resized-640px');
        var mediumPath = "./public/uploads/img_medium.jpg";
        fs.readFile(mediumPath, function(err, file_buffer){
          var params = {
            Bucket: 'today_in_history',
            Key: mediumImg,
            Body: file_buffer,
            ContentType:'image/jpeg',
            ACL: 'public-read',
          };
          s3.putObject(params, function (perr, pres) {
            if (perr) {
              console.log("Error uploading data: ", perr);
            } else {
              var result = {'medium_image':'https://s3.amazonaws.com/today_in_history/'+mediumImg}
              // res.json(result);
            }
          });
          s3.client.getObject(params, function(err, data) {
            if (err) {
              console.log("Error uploading data: ", err);
            } else {
              console.log(data);
            }
          });
        });
      });
   }
};

/// Base
exports.localUpload = function(req, res) {
  var image = req.body.image;
  var r_width;
  var data = image.replace(/^data:image\/\w+;base64,/, "");
  var buf = new Buffer(data, 'base64');
  var date = new Date();
  var dateObject = new Date();
  var uniqueId =
    dateObject.getFullYear() + '' +
    dateObject.getMonth() + '' +
    dateObject.getDate() + '' +
    dateObject.getTime();

  var cropImg = function(callback) {
    easyimg.crop({
            src:'./public/uploads/img.jpg', dst:'./public/uploads/img.jpg',
            cropwidth:req.body.w, cropheight:req.body.h,
            gravity:'NorthWest',
            x:req.body.x1, y:req.body.y1
        },
        function(err, stdout, stderr) {
            if (err) throw err;
            console.log('Cropped');
            callback();
        }
      );
  };
  var checkImgDimensions = function(callback){
    sizeOf('./public/uploads/img.jpg', function (err, dimensions) {
      if(typeof dimensions != 'undefined')
        r_width = dimensions.height;
      else
        r_width = 800;
      callback();
    });
  };
  fs.writeFile("./public/uploads/img.jpg", buf , function(err){
    console.log('w:',req.body.w,", h:",req.body.h,",x:",req.body.x1, ',y:',req.body.y1);
    if(typeof req.body.w != 'undefined') {
      cropImg(function(){
         mainFunction(req.body.mSize, req.body.sSize);

      });
    }
    else {
      checkImgDimensions(function(){
        if(r_width < 640) {
          medium = r_width;
          if(medium > 240)
            small = 240;
          else
            small = r_width/2;
        }
        else {
            medium = 640;
            small = 240;
        }
        console.log("dimensions", medium , small);
        mainFunction(medium, small);
      });
    }
  });
  function mainFunction(medium, small){
    var dateObject = new Date();
    var uniqueId =
      dateObject.getFullYear() + '' +
      dateObject.getMonth() + '' +
      dateObject.getDate() + '' +
      dateObject.getTime();
      var path = './public/uploads/img.jpg';
      var imgName   = uniqueId+'.png';
      var mediumImg = uniqueId+'_medium'+'.jpg';
      var smallImg  = uniqueId+'_small'+'.jpg';

    fs.readFile(path, function(err, file_buffer){
      var params = {
        Bucket: 'today_in_history',
        Key: imgName,
        Body: file_buffer,
        ContentType:'image/jpeg',
        ACL: 'public-read',
      };
      s3.putObject(params, function (perr, pres) {
        if (perr) {
          console.log("Error uploading data: ", perr);
        } else {
          console.log(pres);
          var result = {'image':'https://s3.amazonaws.com/today_in_history/'+imgName}
          res.json(result);
          console.log("Successfully uploaded data to myBucket/myKey");
        }
      });
      s3.client.getObject(params, function(err, data) {
        if (err) {
          console.log("Error uploading data: ", err);
        } else {
          console.log(data);
        }
      });
    });

    // Create Small Size Image and Upload on S3
    easyimg.resize({
      src: "./public/uploads/img.jpg",
      dst: "./public/uploads/img_small.jpg",
      width: small
    }, function(err, stdout, stderr){
      if (err) throw err;
      var smallPath = "./public/uploads/img_small.jpg";
      fs.readFile(smallPath, function(err, file_buffer){
      var params = {
        Bucket: 'today_in_history',
        Key: smallImg,
        Body: file_buffer,
        ContentType:'image/jpeg',
        ACL: 'public-read',
      };
      s3.putObject(params, function (perr, pres) {
        if (perr) {
          console.log("Error uploading data: ", perr);
        } else {
          var result = {'image':'https://s3.amazonaws.com/today_in_history/'+smallImg}
        }
      });
      s3.client.getObject(params, function(err, data) {
        if (err) {
          console.log("Error uploading data: ", err);
        } else {
          console.log(data);
        }
      });
      });
    });

    // Create Medium Size Image and Upload on S3
    easyimg.resize({
      src: "./public/uploads/img.jpg",
      dst: "./public/uploads/img_medium.jpg",
      width: medium
    }, function(err, stdout, stderr){
      if (err) throw err;
        var mediumPath = "./public/uploads/img_medium.jpg";
        fs.readFile(mediumPath, function(err, file_buffer){
          var params = {
            Bucket: 'today_in_history',
            Key: mediumImg,
            Body: file_buffer,
            ContentType:'image/jpeg',
            ACL: 'public-read',
          };
          s3.putObject(params, function (perr, pres) {
            if (perr) {
              console.log("Error uploading data: ", perr);
            } else {
              var result = {'image':'https://s3.amazonaws.com/today_in_history/'+mediumImg}
            }
          });
          s3.client.getObject(params, function(err, data) {
            if (err) {
              console.log("Error uploading data: ", err);
            } else {
              console.log(data);
            }
          });
        });
      });
  }
};
