// Worker
var fs = require('fs');
var AWS = require('aws-sdk');
var request = require('request');
var easyimg = require('easyimage');
var http = require('http');
var im = require('imagemagick');
var config = require("./lib/config");

var accessKeyId =  process.env.AWS_ACCESS_KEY || "xxxxxx";
var secretAccessKey = process.env.AWS_SECRET_KEY || "+xxxxxx+B+xxxxxxx";
AWS.config.update({
  accessKeyId: config.AWS[0].accessKeyId,
  secretAccessKey: config.AWS[0].secretAccessKey
});

var s3 = new AWS.S3();

// DataBase Connection
var connection;
// callback
 var myCallback = function(connection) {
   connection.connect(function(err) {
    if(err) {
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 1000);
    }
  });
   connection.on('error', function(err) {
    if(err.code === 'PROTOCOL_CONNECTION_LOST') {
      console.log('links enter');
      usingItNow(myCallback);
    } else {
      throw err;
    }
  });
 };
 var usingItNow = function(callback) {
   connection = require('./db.js').handleDisconnect();
   callback(connection);
 };
 usingItNow(myCallback);


// Child Process
process.on('message', function(m) {
    var d = new Date();
    var nEve = [];
    var length = 0;
    var ext;
    var small   = 240;
    var medium  = 640;
    var r_width = 0;
    var sql = 'SELECT * FROM update_links WHERE status=0';

    connection.query(sql,  function(err, events) {
        if (err){
           throw err;
        }
        else{
            console.log('events.length',events.length);
            if(events.length > 0 ){
                nEve = events;
                uploader(0)
                length = events.length;
            }else{
                console.log('process.pid',process.pid)
            }
        }
    });
   function uploader(i) {
      console.log('Looppp', i)
      if( i < nEve.length ) {
        var link_id = nEve[i].link_id;
        var Iurl = nEve[i].url;
        console.log('Process on Record - ', i)
        download(nEve[i].img_link , link_id , Iurl , i , function(lid, iurl , count){
          if( !lid ) {
            console.log('error:')
          }
          else {
            mainFunction(lid, iurl ,count, function(link_id, smallImg, mediumImg , count1){
                uploader(i+1);
                setTimeout(function(){
                    cropImages(link_id, smallImg, mediumImg , count1);
                },2000)
            });
            console.log('process.pid ->',process.pid , d ,lid,' -MEMORY- ', process.memoryUsage());
          }
        })
      }
    }
    // Fetch All Images from URL
    var download = function(url, link_id, Iurl, count , callback) {
        var url = 'http:'+url;
        console.log('Looppp-1', count)
        var ext = url.split('.').pop().split(/\#|\?/)[0];
        if(typeof ext == 'undefined')
            ext = 'png';
        if(ext == 'svg')
            ext ='svg';
        else
            ext = 'png';
        request.get({url: url, encoding: 'binary'}, function (err, response, body) {
           fs.writeFile('./public/uploads/'+link_id+'img.'+ext, body, 'binary', function(err) {
               if(err)
                   console.log('Error in downloading- ',err);
               else{
                   if(ext == 'svg'){
                      console.log('SVG EXT');
                      im.convert(['./public/uploads/'+link_id+'img.'+ext, '-resize', '800x600', './public/uploads/'+link_id+'img.png'],
                        function(err, stdout){
                          if (err) {
                              console.log('stdout:', err);
                          }
                          callback(link_id, Iurl, count);
                        });
                   }else{
                       callback(link_id, Iurl, count);
                   }
                   console.log("The file was saved!");
               }
           });
        });
    }
    // Crop and Upload to AWS
    function mainFunction(link_id, iurl, count, callback){
        var d1 = new Date();
        console.log('===>',link_id, count)
        var dateObject = new Date();
        var uniqueId =
            dateObject.getFullYear() + '' +
            dateObject.getMonth() + '' +
            dateObject.getDate() + '' +
            dateObject.getTime();
        var path      = './public/uploads/'+link_id+'img.png';
        console.log('path', path)
        var imgName   = uniqueId+'.png';
        var mediumImg = uniqueId+'_medium'+'.png';
        var smallImg  = uniqueId+'_small'+'.png';

        fs.readFile(path, function(err, file_buffer){
            var params = {
                Bucket: config.AWS[0].aws_Bucket,
                Key: imgName,
                Body: file_buffer,
                ContentType:'image/png',
                ACL: 'public-read',
            };
            s3.putObject(params, function (perr, pres) {
                if (perr) {
                    console.log("Error uploading data1: ", perr);
                } else {
                    console.log('https://s3.amazonaws.com/today_in_history/'+imgName, link_id);
                    console.log('https://s3.amazonaws.com/today_in_history/'+mediumImg, link_id);
                    console.log('https://s3.amazonaws.com/today_in_history/'+smallImg, link_id);
                    var main_image = 'https://s3.amazonaws.com/today_in_history/'+imgName;
                    var sml_image  = 'https://s3.amazonaws.com/today_in_history/'+smallImg;
                    var med_image  = 'https://s3.amazonaws.com/today_in_history/'+mediumImg;
                    connection.query('UPDATE links SET image_url = ?, medium_image_url = ?, small_image_url = ? WHERE url = ?', [main_image, med_image, sml_image , iurl],function(err, results) {
                        if(err)
                            console.log(err);
                        else{
                        //  process.send('Process is running');
                            var status = 1;
                            connection.query('UPDATE update_links SET status = ? WHERE link_id = ?', [ status , link_id],function(err, results1) {
                                if(err)
                                    console.log(err);
                                else
                                    console.log("update_links Updated!!");
                            });
                        }
                    });
                    console.log("Successfully uploaded data to myBucket/myKey");
                    var cl = setTimeout(function(){
                        //cropImages(link_id, smallImg, mediumImg);
                        callback(link_id, smallImg, mediumImg , count);
                        clearTimeout(cl)
                    },1000)
                }
            });
        });
    }
    function cropImages(link_id, smallImg, mediumImg , count1){
        console.log('==================>',smallImg, mediumImg, count1, length)
        easyimg.info('./public/uploads/'+link_id+'img.png', function(err, features){
              if(typeof features.width != 'undefined')
                r_width = features.width;
              else
                r_width = 800;

              if(r_width < 640) {
                medium = r_width;
                if(medium < 240) {
                   small = r_width;
                }
               }
               else {
                   medium = 640;
               }
                im.resize({
                    srcPath: './public/uploads/'+link_id+'img.png',
                    dstPath: './public/uploads/'+link_id+'img_medium.png',
                    format:  'png',
                    width:   medium
                }, function(err, stdout, stderr){
                    if (err) {
                        console.log('error --img_medium',err);
                    }
                    console.log('resized-medium', medium);
                    var mediumPath = './public/uploads/'+link_id+'img_medium.png';
                    fs.readFile(mediumPath, function(err, file_buffer){
                        var params = {
                            Bucket: config.AWS[0].aws_Bucket,
                            Key: mediumImg,
                            Body: file_buffer,
                            ContentType:'image/png',
                            ACL: 'public-read',
                        };
                        s3.putObject(params, function (perr, pres) {
                          if (perr) {
                            console.log('Error', perr)
                          } else {
                             //process.send('Process is running');
                              var ful =setTimeout(function(){
                                  fs.unlink('./public/uploads/'+link_id+'img_medium.png');
                                  clearTimeout(ful)
                              },30000)
                              console.log("Successfully uploaded Medium data to myBucket/myKey");
                          }
                        });
                    });
                });
             });
        /// Small Images
        im.resize({
            srcPath: './public/uploads/'+link_id+'img.png',
            dstPath: './public/uploads/'+link_id+'img_small.png',
            width:   small
        }, function(err, stdout, stderr){
            if (err) {
                console.log('error - Small Images',err);
            }
            console.log('resized-small', small);

            count1 =  count1 + 1;
            var val = count1+'-'+length;
            process.send(val);
            var smallPath = './public/uploads/'+link_id+'img_small.png';
            fs.readFile(smallPath, function(err, file_buffer){
                var params = {
                    Bucket: config.AWS[0].aws_Bucket,
                    Key: smallImg,
                    Body: file_buffer,
                    ContentType:'image/png',
                    ACL: 'public-read',
                };
              s3.putObject(params, function (perr, pres) {
                if (perr) {
                  console.log("Error uploading data:small ");
                } else {
                    var sml = setTimeout(function(){
                        fs.unlink('./public/uploads/'+link_id+'img_small.png');
                        clearTimeout(sml)
                    },15000)
                    var med = setTimeout(function(){
                        fs.unlink('./public/uploads/'+link_id+'img.png');
                        fs.unlink('./public/uploads/'+link_id+'img.svg');
                        clearTimeout(med)
                    },30000)
                  console.log("Successfully uploaded Small data to myBucket/myKey");
                }
              });
            });
        });

    }

});
