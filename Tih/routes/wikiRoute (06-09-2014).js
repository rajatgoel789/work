var request= require('request');
var cheerio = require('cheerio');
var ld = require("levenshtein-damerau");
var ld2 = require('ld');
var natural = require('natural');
var fs = require('fs');
var cp = require('child_process');
var child = cp.fork('./workerWiki');
var util = require('util');


var connection;
// callback
 var myCallback = function(connection) {
   connection.connect(function(err) {
    if(err) {
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000);
    }
  });
   connection.on('error', function(err) {
    if(err.code === 'PROTOCOL_CONNECTION_LOST') {
      console.log('event enter');
      usingItNow(myCallback);
    } else {
      throw err;
    }
  });
 };
 var usingItNow = function(callback) {
  connection = require('../db.js').handleDisconnect();
   callback(connection);
 };
 usingItNow(myCallback);

exports.testItem = function(req , res){
    console.log("TEST CALL SUCCESS ");

    var url ="http://en.wikipedia.org/wiki'''''''''''''''''''''''''''s/%C5%BBejtun";
    var linktest  = {url: url, name: 'Hello MySQL'};
    var query = connection.query('INSERT INTO linkstest SET ?', linktest, function(err, result) {
    });
 res.end("Working");
};


exports.test22 = function(req ,res){

  var obj;
  var file = './public/uploads/my.json';
  fs.readFile(file, 'utf8', function (err, data) {
    if (err) throw err;
    obj = JSON.parse(data);
    res.send(obj)
  });

};


exports.test11 = function(req, res, soc){
   // res.send('Process Started - fetch')
    console.log('This platform is ' + child.connected);
    console.log('Process',process.pid ,' req.app.locals.someVar')
    soc = req.app.locals.someVar;
    soc.emit('message', { message: 'welcome to the chat' });

    res.send('DONE')
   // process.kill(process.pid)
        // child.send({url:'http://en.wikipedia.org/wiki/January_1',name:'1jan'});
};

// Process Response
 child.on('message', function(m ) {
     // Receive results from child process
     console.log('received21: ' , m );

});

process.on('SIGINT',function(){
    child.kill("SIGINT");
});

process.on('exit', function() {
  console.log('process is about to exit, kill ffmpeg');
   child.kill(child.pid);
   process.kill(child.pid);
});

child.on('close', function (code, signal) {
    console.log('Worker-wiki child process exited with code ' , code);
});

// send Local wiki data
exports.localWikiData = function(req, res){
    var url = req.body.path;
    var name = req.body.day+'-'+req.body.month;
    var obj;
    var file = './public/wikidata/'+name+'.json';
    fs.stat(file, function(err, stat) {
        if(err == null) {
            fs.readFile(file, 'utf8', function (err, data) {
                if (err) {
                    console.log('wikiData-error',err);
                }else{
                    obj = JSON.parse(data);
                    res.json(obj)
                    setTimeout(function(){
                        fs.unlink(file);
                    },1800000)
                }
            });
        } else if(err.code == 'ENOENT') {
            res.json({'status':'inprocess'});
        } else {
            console.log('Some other error: ', err.code);
            res.json({'status':'inprocess'});
        }
    });
};

exports.wikiData = function(req , res){
  console.log('wikiData process')
  var url = req.body.path;
  var name = req.body.day+'-'+req.body.month;
  var obj;
  var file = './public/wikidata/'+name+'.json';
  fs.stat(file, function(err, stat) {
      if(err == null) {
        fs.readFile(file, 'utf8', function (err, data) {
          if (err) {
            console.log('wikiData-error',err);
          }else{
            obj = JSON.parse(data);
            res.json(obj)
          }
        });
      } else if(err.code == 'ENOENT') {
          child.send({url:url,name: name});
          res.json({'status':'inprocess'});
      } else {
          console.log('Some other error: ', err.code);
          res.json({'status':'inprocess'});
      }
  });

  function getWikiData(url) {
    console.log('getWikiData')
    request(url, function(err,resp,body) {

      if(!err && resp.statusCode == 200) {
        var $ = cheerio.load(body);
        title = $('title').text();
        var eventsW = [];
        var births = [];
        var deaths = [];
        var holidays = [];
        var holiday = $('#Holidays_and_observances').parent().next().text();
        var eventsHtml = $('#Events').parent().next().find('ul li').length;
        var eventsHtml1 = $('.thumb , .tright').next().find('ul li').length;
        var birthHtml = $('#Births').parent().next().find('ul li').length;
        var deathHtml = $('#Deaths').parent().next().find('ul li').length;
        var holidayHtml = $('#Holidays_and_observances').parent().next().find('ul li').length;

        var holidayData = holiday.split('\n');

        // events wiki item
        for(var i=0; i<eventsHtml; i++){
          var linksName = [];
          var eventsText = $('#Events').parent().next().find("li").eq(i).text();
          var eventData = eventsText.split('–');
          var splitYear = eventData[0].split(' ');
          if(splitYear[1])
            var yearC = '-'+splitYear[0];
          else
            var yearC = splitYear[0];
          var events = $('#Events').parent().next().find("li").eq(i).children('a').length;
          for(var a=1; a<events; a++){
            var urls = $('#Events').parent().next().find("li").eq(i).children('a').eq(a).attr('href');
            urls = 'http://en.wikipedia.org'+urls;
            var name = $('#Events').parent().next().find("li").eq(i).children('a').eq(a).text();
            linksName.push({'url':urls,'name':name});
          }
          //console.log("links--->",linksName);

          if(eventData[2]){
            var concatData = eventData[1]+'-'+eventData[2];
            eventsW.push({'event':concatData, 'year':yearC,'eventUrl':linksName});
          }else{
            eventsW.push({'event':eventData[1], 'year':yearC,'eventUrl':linksName});
          }
        }

        //for div inside wiki Events item
        if(eventsHtml1 > 0){
          console.log('find a div inside events wiki item ');
          for(var i=0; i<eventsHtml1; i++){
            var linksName = [];
            var eventsText = $('.thumb , .tright').next().find("li").eq(i).text();
            if(eventsText.search("–") > -1){
              var eventData = eventsText.split('–');
            }
            var splitYear = eventData[0].split(' ');
            if(splitYear[1])
              var yearC = '-'+splitYear[0];
            else
              var yearC = splitYear[0];
            var events = $('.thumb , .tright').next().find("li").eq(i).children('a').length;
            for(var a=0; a<events; a++){
              var urls = $('.thumb , .tright').next().find("li").eq(i).children('a').eq(a).attr('href');
              urls = 'http://en.wikipedia.org'+urls;
              var name = $('.thumb , .tright').next().find("li").eq(i).children('a').eq(a).text();
              linksName.push({'url':urls,'name':name});
            }
            if(eventData[2]){
              var concatData = eventData[1]+'-'+eventData[2];
                console.log("---concat---------",concatData)
              eventsW.push({'event':concatData, 'year':yearC,'eventUrl':linksName});
            }else{
                console.log("-----eventData-------",eventData[1])
              eventsW.push({'event':eventData[1], 'year':yearC,'eventUrl':linksName});
            }
          }
        }
      // births wiki item
      for(var i=0; i<birthHtml; i++){
        var linksName = [];
        var eventsText = $('#Births').parent().next().find("li").eq(i).text();
        if(eventsText.search("–") > -1){
          var eventData = eventsText.split('–');
        }
        var splitYear = eventData[0].split(' ');
        if(splitYear[1])
          var yearC = '-'+splitYear[0];
        else
          var yearC = splitYear[0];
        var nameVal = eventData[1].split(',');
        var events = $('#Births').parent().next().find("li").eq(i).children('a').length;

        for(var a=0; a<events; a++){
          var urls = $('#Births').parent().next().find("li").eq(i).children('a').eq(a).attr('href');
          urls = 'http://en.wikipedia.org'+urls;

          var name = $('#Births').parent().next().find("li").eq(i).children('a').eq(a).text();
          //console.log("name===",name);
          if( isNaN(name) ){
          linksName.push({'url':urls,'name':name});
        }
        }
         //console.log("links--->",linksName);
        births.push({'name':nameVal[0],'event':eventData[1], 'year':yearC,'birthUrl':linksName});
      }
      // death wiki items
      for(var i=0; i<deathHtml; i++){
        var linksName = [];
        var eventsText = $('#Deaths').parent().next().find("li").eq(i).text();
        if(eventsText.search("–") > -1){
          var eventData = eventsText.split('–');
        }
        var splitYear = eventData[0].split(' ');
        if(splitYear[1])
          var yearC = '-'+splitYear[0];
        else
          var yearC = splitYear[0];
        var nameVal = eventData[1].split(',');
        var events = $('#Deaths').parent().next().find("li").eq(i).children('a').length;
        for(var a=0; a<events; a++){
          var urls = $('#Deaths').parent().next().find("li").eq(i).children('a').eq(a).attr('href');
          urls = 'http://en.wikipedia.org'+urls;
          var name = $('#Deaths').parent().next().find("li").eq(i).children('a').eq(a).text();
          if( isNaN(name) ){
          linksName.push({'url':urls,'name':name});
        }
        }
         //console.log("links--->",linksName);
        deaths.push({'name':nameVal[0],'event':eventData[1], 'year':yearC,'birthUrl':linksName});
      }

      // holidays wiki item
      for(var i=0; i<holidayHtml; i++){
        var linksName = [];
        var eventsText = $('#Holidays_and_observances').parent().next().find("li").eq(i).text();
        var events = $('#Holidays_and_observances').parent().next().find("li").eq(i).children('a').length;
        for(var a=0; a<events; a++){
          var urls = $('#Holidays_and_observances').parent().next().find("li").eq(i).children('a').eq(a).attr('href');
          urls = 'http://en.wikipedia.org'+urls;
          var name = $('#Holidays_and_observances').parent().next().find("li").eq(i).children('a').eq(a).text();
          linksName.push({'url':urls,'name':name});
        }
        //console.log("links--->",linksName);
        holidays.push({'event':eventsText,'birthUrl':linksName});
      }

         res.json({ wikiEvents:eventsW , wikiDeaths: deaths , wikiHolidays:holidays , wikiBirths:births });
      }else{
           res.json({'status':'Error'});
      }
    });
  }
};


function unique(arr) {
        var comparer = function compareObject(a, b) {
            if (a.url == b.url) {
                return 0;
            } else {
                if (a.url < b.url) {
                    return -1;
                } else {
                    return 1;
                }
            }
        }
        arr.sort(comparer);
        var end;
        for (var i = 0; i < arr.length - 1; ++i) {
            if (comparer(arr[i], arr[i+1]) === 0) {
                arr.splice(i, 1);
            }
        }
        return arr;
    }

//get events
exports.eventItem = function(req , res){
  var eventsW = req.body.wikiEvents;
  var internalEvents = [];
  var newArray = [];

  var exitEvents = [];
  var dbArray=[]
  var tempArray=[]
  if(req.body.day && req.body.month){
    var day = req.body.day;
    var month = req.body.month;
  }
  else{
    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth();
  }

  var sql="SELECT e.event,e.year,e.id,el.link_id,l.url  FROM events  e left Join  events_links el  on e.id=el.event_id left join links l on el.link_id=l.id WHERE e.day = '"+ day +"' AND e.month = '"+ month +"' AND e.category_id = '36' ";
   console.log("SQLEVENTQUERY",sql) ;
  connection.query(sql, function(err, data , next) {
    if (err) throw err;
    else{
      var prev = [];
      var x = {};
      var xc = {};
      var i = data.length;
      var dbDataLinks = [];
      var dbEvents = [];
      while(i--) {
        var sHash = data[i].event.toString();
        var cHash = data[i].event.toString();
        if (typeof(x[sHash]) == "undefined") {
          x[sHash] = [];
          xc[cHash] = [];
        }
        if(xc[sHash].indexOf(data[i].url) == - 1 ){
          xc[cHash].push(data[i].url);
          dbEvents.push(data[i].event);
          x[sHash].push({
            'url': data[i].url,
            'id':data[i].id,
            'year':data[i].year
          });
        }
      }
      for(d in x){
        dbDataLinks.push({
          'id':x[d][0].id,
          'event':d,
          'url':x[d],
          'year':x[d][0].year
        })
      }
      var i =0
      for(f in xc){
        dbDataLinks[i].url = xc[f];
        i++;
      }

      for(var k=0; k<eventsW.length; k++){
        var breaktag =0 ;
        var checkingArray=[];
        var evtt = eventsW[k].event;
        // console.log("evtt value",evtt) ;
        // if(typeof evtt !="undefined" && evtt)
        //   evtt=  evtt.replace(/\"/g,'&quot;')
        var finalUrls = eventsW[k].eventUrl;

        var finalUrlsLowerCase=[];
        for(var n =0; n < finalUrls.length;n++){
           var finl  = decodeURI(finalUrls[n].url.toUpperCase());
          // console.log("ROCKING",decodeURI(finl.toLowerCase()));
           finalUrlsLowerCase.push(decodeURI(finl.toLowerCase()));
        }


        if(evtt && dbDataLinks.length>0){
            var i1=-1;
            var matched=0;
          for(var i=0; i<dbDataLinks.length; i++){ //console
            i1=i;
            var evt = dbDataLinks[i1].event;
            if(1){
            if(eventsW[k].year == dbDataLinks[i1].year){
              //console.log("Year compared success");
              evt = evt.replace(/&quot;/g, '"');
              evt =  evt.replace(/&#0*39;/g, "'");
              evtt =  evtt.replace(/&quot;/g, '"');
              evtt =  evtt.replace(/&#0*39;/g, "'");
              var lth = natural.LevenshteinDistance(evtt,evt);

              if(evtt.length>evt.length){

                var lth = ld2.computeDistance(evtt.toString().trim(),evt.toString().trim());
              //  var lth = ld(evtt.trim(),evt.trim());
              }else{
                //var lth = ld(evt.trim(),evtt.trim());
                var lth = ld2.computeDistance(evt.toString().trim(),evtt.toString().trim());
              }
              var dbEventLength  = evt.length;
              var lthPer = lthPercent(lth,dbEventLength);
              // console.log("\n\n",i1,"Rajat db==>",evt,"wikievent==>",evtt,"score=-=>",lth,"len==>",evt.length,"percent==>",lthPer);
              if(lthPer <= 15){
                //console.log("db==>",evt,"wikievent==>",evtt,"score=-=>",lth,"len==>",evt.length,"percent==>",lthPer);
                if (dbDataLinks[i1].event.trim() == eventsW[k].event.trim()){
                  var dbUrlLen = dbDataLinks[i1].url.length;
                  var g = unique(eventsW[k].eventUrl);
                  var wikiUrlLen = eventsW[k].eventUrl.length;
                  //console.log("LTH15call",dbUrlLen , wikiUrlLen , dbDataLinks[i1].url  , "==&*&*&*&*&==",finalUrlsLowerCase);
                  for(var m = 0;m<wikiUrlLen;m++){
                        for (var l = 0;l< dbUrlLen;l++ ){ //console.log("L called ",l);
                          if( typeof  eventsW[k].eventUrl[m].url !='undefined'
                            && typeof dbDataLinks[i1].url[l] !='undefined'
                            && eventsW[k].eventUrl[m].url
                            && dbDataLinks[i1].url[l] ){

                            if (dbUrlLen <= wikiUrlLen ){
                              var dif = Math.abs(wikiUrlLen- dbUrlLen);
                              var finl1  = decodeURI(dbDataLinks[i1].url[l].toLowerCase());
                             if(finalUrlsLowerCase.indexOf(decodeURI(finl1.toLowerCase()) )==-1){
                               if (breaktag !=1 ){

                                console.log("Harinder =",finalUrlsLowerCase, finl1.toLowerCase(), decodeURI(dbDataLinks[i1].url[l].toLowerCase()), dbDataLinks[i1].url[l])
                                   internalEvents.push({'id':dbDataLinks[i1].id,'eventInternal':dbDataLinks[i1].event,'eventWiki':eventsW[k].event,'year':eventsW[k].year,'day':day,'finalUrls':eventsW[k].eventUrl});
                                   breaktag =1 ;
                                }
                            }


                              if (l == dbUrlLen-1 && dif > 0 && breaktag !=1 ){
                                // console.log("Duplicate call",dbUrlLen , "  ",wikiUrlLen );
                                 internalEvents.push({'id':dbDataLinks[i1].id,'eventInternal':dbDataLinks[i1].event,'eventWiki':eventsW[k].event,'year':eventsW[k].year,'day':day,'finalUrls':eventsW[k].eventUrl});
                                 breaktag =1 ;
                              }else if(l == dbUrlLen-1 && dif == 0 && breaktag !=1 ){
                                exitEvents.push({'id':dbDataLinks[i1].id,'eventInternal':eventsW[k].event,'eventWiki':eventsW[k].event,'year':eventsW[k].year,'day':day,'finalUrls':eventsW[k].eventUrl})
                                breaktag =1 ;
                              }



                            }else{// console.log("Duplicate call anotherrrrr ",breaktag);
                                 if(breaktag !=1){ console.log("Duplicate call another ");
                              //chk for links
                                exitEvents.push({'id':dbDataLinks[i1].id,'eventInternal':eventsW[k].event,'eventWiki':eventsW[k].event,'year':eventsW[k].year,'day':day,'finalUrls':eventsW[k].eventUrl})
                                breaktag =1 ;
                                }

                            }

                           }else{
                               if(breaktag !=1){
                              //chk for links
                               exitEvents.push({'id':dbDataLinks[i1].id,'eventInternal':eventsW[k].event,'eventWiki':eventsW[k].event,'year':eventsW[k].year,'day':day,'finalUrls':eventsW[k].eventUrl})
                               breaktag =1 ;
                              }
                            //console.log("here2","wiki==>",eventsW[k].eventUrl[m].url,"db==>",dbDataLinks[i].url[l]);
                          }


                       }//**end of for dburl


                  }// ** end of for wikiurl  loop  **
                  if(wikiUrlLen == 0 )
                  {
                                    if(breaktag !=1){
                                     // console.log("here222",eventsW[k].event);
                                    exitEvents.push({'id':dbDataLinks[i1].id,'eventInternal':eventsW[k].event,'eventWiki':eventsW[k].event,'year':eventsW[k].year,'day':day,'finalUrls':eventsW[k].eventUrl})
                                     breaktag =1 ;
                                   }

                  }


                }else{
                        if(breaktag !=1){
                               // console.log("here333",eventsW[k].event);
                               //chk for links
                              if(tempArray.indexOf(eventsW[k].event)==-1){
                                 exitEvents.push({'id':dbDataLinks[i1].id,'eventInternal':eventsW[k].event,'eventWiki':eventsW[k].event,'year':eventsW[k].year,'day':day,'finalUrls':eventsW[k].eventUrl})
                                 breaktag =1 ;
                                  }
                                   }

              }
              }
              else if(lthPer > 15 ){
               checkingArray.push({'lthPer':lthPer,'id':dbDataLinks[i1].id,'eventInternal':dbDataLinks[i1].event,'eventWiki':eventsW[k].event,'year':eventsW[k].year,'day':day,'finalUrls':eventsW[k].eventUrl});

              }
            }//else closed
          }
       /***********/
          if(i == dbDataLinks.length-1){
                if(breaktag !=1){
           // console.log("***********Check only Here**********",checkingArray, checkingArray.length);
              checkingArray = sortByKey(checkingArray,"lthPer") ;
            //console.log("reached ttt 5");
                if(dbEvents.indexOf(eventsW[k].event.trim()) == -1){
                  if(checkingArray.length ==0)
                  {
                     newArray.push({'event':eventsW[k].event,'year':eventsW[k].year,'day':day,'finalUrls':eventsW[k].eventUrl});
                     breaktag=1;
                  }else{ //chk lth score here
                        if(checkingArray[0].lthPer >15  && checkingArray[0].lthPer <= 35){
                        //duplicate
                         internalEvents.push({'id':checkingArray[0].id,'eventInternal':checkingArray[0].eventInternal,'eventWiki':checkingArray[0].eventWiki,'year':checkingArray[0].year,'day':checkingArray[0].day,'finalUrls':checkingArray[0].finalUrls});
                          breaktag=1;
                        }
                        else if(checkingArray[0].lthPer >35  ){
                          //new here
                          newArray.push({'event':eventsW[k].event,'year':eventsW[k].year,'day':day,'finalUrls':eventsW[k].eventUrl});
                          breaktag=1;
                          }
                  }

                }else{
                  var idd = 0;
                  var dbindex=dbEvents.indexOf(eventsW[k].event.trim()) ;
                  //console.log('harinder',dbindex, eventsW[k].event.trim(),dbDataLinks.length)
                  for(var x = 0; x < dbDataLinks.length; x++){
                      var ev = dbDataLinks[x].event;
                      if(eventsW[k].event.trim() == ev.trim()){
                          //console.log('iddd', idd, ev)
                          idd = dbDataLinks[x].id;
                      }
                  }
                   if(breaktag !=1 && idd != 0){
                      exitEvents.push({'id':idd,'eventInternal':eventsW[k].event,'eventWiki':eventsW[k].event,'year':eventsW[k].year,'day':day,'finalUrls':eventsW[k].eventUrl});
                      breaktag =1 ;
                   }
                }
            }
          }
          /************/
          }/*end of for here */
        }else{
          // console.log("sachin",eventsW[k].event);
              if(tempArray.indexOf(eventsW[k].event)==-1){
              newArray.push({'event':eventsW[k].event,'year':eventsW[k].year,'day':day,'finalUrls':eventsW[k].eventUrl});

             }
        }

      }

    }
  res.json({internal:sortByKey(internalEvents,"year"),newevents:sortByKey(newArray,"year"),exitEvents:sortByKey(exitEvents,"year")});
  });
};


/********************************Get Births*******************************************/
exports.birthItem = function(req , res){

  var births = req.body.wikiBirths;
 // console.log("births data ", births);
  //console.log("wiki====>",eventsW);
  var internalBirthEvents = [];
  var exitBirthEvents = [];
  var newBirthEvents = [];;
  if(req.body.day && req.body.month){
    var day = req.body.day;
    var month = req.body.month;
  }
  else{
    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth();
  }
  //var sql = "SELECT * FROM events  WHERE day = '"+ day +"' AND month = '"+ month +"' AND category_id = '36'" ;
  //var sql = "SELECT e.event,e.year,e.id,el.link_id,l.url FROM events e,links l,events_links el WHERE e.day = '"+ day +"' AND e.month = '"+ month +"' AND e.category_id = '36' AND el.event_id = e.id AND l.id = el.link_id";
  var sql="SELECT e.event,e.name,e.year,e.id,el.link_id,l.url  FROM events  e left Join  events_links el  on e.id=el.event_id left join links l on el.link_id=l.id WHERE e.day = '"+ day +"' AND e.month = '"+ month +"' AND e.category_id = '37' ";
  console.log("SQLQUERY",sql) ;
  connection.query(sql, function(err, data , next) {
    if (err) throw err;
    else{
//  console.log("dBase==KKKKKKKKKKKKKKKKKKKKKKKK==>",data);
      var prev = [];
      var x = {};
      var xc = {};
      var i = data.length;

//      console.log("DataLEngth",data.length);
      var dbDataLinks = [];
      var dbEventsOnly = [];
      while(i--) {
        var sHash = data[i].id
        var cHash = data[i].id;
        var event = data[i].event.toString();
        var eventWithName = data[i].name.toString()+", "+data[i].event.toString();
        var year  = data[i].year ;
        if (typeof(x[sHash]) == "undefined") {
          x[sHash] = [];
          xc[cHash] = [];
        }
        if(xc[sHash].indexOf(data[i].url) == - 1 ){
          xc[cHash].push(data[i].url);
          dbEventsOnly.push(data[i].event.trim());
          x[sHash].push({
            'url': data[i].url,
            'id':data[i].id,
            'year':data[i].year ,
            'event':data[i].event.toString() ,
            'eventWithName':data[i].name.toString()+", "+data[i].event.toString()

          });
        }
      }
//      console.log( "++++++++++++++++++++++++++++++",x) ;
      for(d in x){
        dbDataLinks.push({
          'id':x[d][0].id,
          'event':x[d][0].event,
          'eventWithName':x[d][0].eventWithName ,
          'url':x[d],
          'year':x[d][0].year
        })
      }
      var i =0
      for(f in xc){
        dbDataLinks[i].url = xc[f];
        i++;
      }

      for(var k=0; k<births.length; k++){
        var checkingArray=[];
        var breaktag =0 ;
        var evtt = births[k].event;
        if(typeof evtt !="undefined" && evtt)
         evtt=  evtt.replace(/\"/g,'&quot;')
        var finalUrls = births[k].birthUrl;
        var finalUrlsLowerCase=[];
        var matched= 0;

        for(var n =0; n < finalUrls.length; n++){
          //console.log("ROCKING5454545",finalUrls[n].url);
         finalUrlsLowerCase.push(decodeURI(finalUrls[n].url.toLowerCase()));
        }

        if(evtt && dbDataLinks.length>0){
          for(var i=0; i<dbDataLinks.length; i++){
              var evt = dbDataLinks[i].event;
              var evt1 = dbDataLinks[i].eventWithName;
              evt1 = evt1.replace(/&quot;/g, '"');
              evt1 =  evt1.replace(/&#0*39;/g, "'");
              evtt =  evtt.replace(/&quot;/g, '"');
              evtt =  evtt.replace(/&#0*39;/g, "'");

         if(1){
            if(births[k].year == dbDataLinks[i].year){
              var lth = natural.LevenshteinDistance(evtt,evt1);
              //console.log(lth);

              if(evtt.length>evt1.length){
                  var lth = ld2.computeDistance(evtt.toString().trim(),evt1.toString().trim());
               // var lth = ld(evtt.trim(),evt1.trim());
              }else{
                  var lth = ld2.computeDistance(evt1.toString().trim(),evtt.toString().trim());
                //var lth = ld(evt1.trim(),evtt.trim());
              }
              var dbEventLength  = evt1.length;
              var lthPer = lthPercent(lth,dbEventLength);
             // console.log("birthdbwithname==>",evt1, "Bdb name ",evt , "wikievent==>",evtt,"score=-=>",lth,"len==>",evt1.length,"percent==>",lthPer);
              if(lthPer <= 20){

                if (dbDataLinks[i].event.trim() == births[k].event.trim()){
                  var dbUrlLen = dbDataLinks[i].url.length;
                  var wikiUrlLen = births[k].birthUrl.length;

            // console.log("BIRTHSDBLTH15call",dbUrlLen , wikiUrlLen , dbDataLinks[i].url  ,/* births[k].birthUrl , */ "==&*&*&*&*&==",finalUrlsLowerCase);
                  for(var m = 0;m<wikiUrlLen;m++){
                        for (var l = 0;l< dbUrlLen;l++ ){
                          //console.log("L called ",l);
                          if( typeof  births[k].birthUrl[m].url !='undefined'
                            && typeof dbDataLinks[i].url[l] !='undefined'
                            && births[k].birthUrl[m].url
                            && dbDataLinks[i].url[l] ){
                           // console.log("wiki :)))",eventsW[k].event,eventsW[k].eventUrl[m].url ,"db==>", dbDataLinks[i].event,dbDataLinks[i].url[l] );
                            if (dbUrlLen <= wikiUrlLen ){
                           // console.log("\n LenghtMatching",dbUrlLen , "  ",wikiUrlLen);
                            var dif = Math.abs(wikiUrlLen- dbUrlLen);
                            //console.log("Birth Method call  ",dif , "breaktag= ",breaktag , "lvalue", l , "DB URL ",dbUrlLen-1);
                             if(finalUrlsLowerCase.indexOf(decodeURI(dbDataLinks[i].url[l].toLowerCase())   )==-1){
                               if (breaktag !=1 ){
                            //       console.log("Duplicate call RajatGoel");
                                    internalBirthEvents.push({'name':births[k].name,'id':dbDataLinks[i].id,'eventInternal':evt,'eventWiki':births[k].event,'year':births[k].year,'day':day,'finalUrls':births[k].birthUrl});
                                    breaktag =1 ;
                                }
                             }
                              if (l == dbUrlLen-1 && dif > 0 && breaktag !=1 ){
                                //console.log("here0");
                                internalBirthEvents.push({'name':births[k].name,'id':dbDataLinks[i].id,'eventInternal':evt,'eventWiki':births[k].event,'year':births[k].year,'day':day,'finalUrls':births[k].birthUrl});
                                breaktag =1 ;

                              }else if(l == dbUrlLen-1 && dif == 0 && breaktag !=1 ){
                                        exitBirthEvents.push({'name':births[k].name,'id':dbDataLinks[i].id,'event':births[k].event,'year':births[k].year,'day':day,'finalUrls':births[k].birthUrl});
                                        breaktag =1 ;
                              }

                            }else{ //console.log("Duplicate call anotherrrrr ",breaktag);
                                 if(breaktag !=1){ //console.log("Duplicate call another ");
                                        exitBirthEvents.push({'name':births[k].name,'id':dbDataLinks[i].id,'event':births[k].event,'year':births[k].year,'day':day,'finalUrls':births[k].birthUrl});
                                        breaktag =1 ;
                                   }

                                 }
                         }else{

                                if(breaktag !=1){
                                //chk for links
                                 exitBirthEvents.push({'name':births[k].name,'id':dbDataLinks[i].id,'event':births[k].event,'year':births[k].year,'day':day,'finalUrls':births[k].birthUrl});
                                 breaktag =1 ;
                                     }
                          }
                       }//**end of for dburl
                  }// ** end of for wikiurl  loop  **
                  if(wikiUrlLen == 0 )
                  {
                                    if(breaktag !=1){
                                     exitBirthEvents.push({'name':births[k].name,'id':dbDataLinks[i].id,'event':births[k].event,'year':births[k].year,'day':day,'finalUrls':births[k].birthUrl});
                                     breaktag =1 ;
                                     }
                  }
                }else{

                        if(breaktag !=1){
                                   exitBirthEvents.push({'name':births[k].name,'id':dbDataLinks[i].id,'event':births[k].event,'year':births[k].year,'day':day,'finalUrls':births[k].birthUrl});
                                   breaktag =1 ;
                                   }
                     }


              }
              else{
               checkingArray.push({'lthPer':lthPer,'name':births[k].name,'id':dbDataLinks[i].id,'eventInternal':dbDataLinks[i].event,'eventWiki':births[k].event,'year':births[k].year,'day':day,'finalUrls':births[k].birthUrl});
              }
            }
          }


          /***********/
          if(i == dbDataLinks.length-1){
                if(breaktag !=1){
           // console.log("***********Check only Here**********",checkingArray, checkingArray.length);
            checkingArray=sortByKey(checkingArray,"lthPer") ;
                 if(dbEventsOnly.indexOf(births[k].event.trim()) == -1){
                  //chk lenght
                  if(checkingArray.length ==0)
                  {
                      newBirthEvents.push({'name':births[k].name,'event':births[k].event,'year':births[k].year,'day':day,'finalUrls':births[k].birthUrl});
                       breaktag=1;
                  }else{

                        if(checkingArray[0].lthPer >15  && checkingArray[0].lthPer <= 35 && breaktag != 1){
                        //duplicate
     internalBirthEvents.push({'name':checkingArray[0].name,'id':checkingArray[0].id,'eventInternal':checkingArray[0].eventInternal,'eventWiki':checkingArray[0].eventWiki,'year':checkingArray[0].year,'day':checkingArray[0].day,'finalUrls':checkingArray[0].finalUrls});
                          breaktag=1;
                        }
                        else if(checkingArray[0].lthPer >35 && breaktag != 1  ){
                          //new here
                         newBirthEvents.push({'name':births[k].name,'event':births[k].event,'year':births[k].year,'day':day,'finalUrls':births[k].birthUrl});
                         breaktag=1;
                          }
                  }
               }else{
                  var idd = 0;
                  var dbindex=dbEventsOnly.indexOf(births[k].event.trim()) ;
                  for(var x = 0; x < dbDataLinks.length; x++){
                      var ev = dbDataLinks[x].event;
                      if(births[k].event.trim() == ev.trim()){
                          idd = dbDataLinks[x].id;
                         // console.log('iddd-birth', idd, ev)
                      }

                  }
                   if(breaktag != 1  && idd != 0) {
                     exitBirthEvents.push({'name':births[k].name,'id':idd,'event':births[k].event,'year':births[k].year,'day':day,'finalUrls':births[k].birthUrl});
                     breaktag =1 ;
                    }
                }
            }
          }
      /************/
          }
        }else{
         if(breaktag != 1){
          newBirthEvents.push({'name':births[k].name,'event':births[k].event,'year':births[k].year,'day':day,'finalUrls':births[k].birthUrl});
           breaktag =1 ;
            }
        }
      }
    }

  res.json({ exitBirthEvents:exitBirthEvents, internalBirths:internalBirthEvents , NewBirths:newBirthEvents});
  });
};

/*******************************************GET DEATH EVENTS********************************/
exports.deathItem = function(req , res){

  var deaths = req.body.wikiDeaths;
  //console.log("wikideaths====>",deaths);
  var exitDeathEvents = [];
  var internalDeathEvents = [];
  var newDeathEvents = [];
  if(req.body.day && req.body.month){
    var day = req.body.day;
    var month = req.body.month;
  }
  else{
    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth();
  }

  var sql="SELECT e.event,e.name ,e.year,e.id,el.link_id,l.url  FROM events  e left Join  events_links el  on e.id=el.event_id left join links l on el.link_id=l.id WHERE e.day = '"+ day +"' AND e.month = '"+ month +"' AND e.category_id = '38' ";
  //console.log("SQLQUERY",sql) ;
  connection.query(sql, function(err, data , next) {
    if (err) throw err;
    else{
       var prev = [];
       var x = {};
      var xc = {};
      var i = data.length;
      var dbDataLinks = [];
      var dbEventsOnly = [];
      while(i--) {
        var sHash = data[i].id
        var cHash = data[i].id;
        var event = data[i].event.toString();
        var eventWithName  = data[i].name.toString()+", "+data[i].event.toString()
        var year  = data[i].year ;
        if (typeof(x[sHash]) == "undefined") {
          x[sHash] = [];
          xc[cHash] = [];
        }
        if(xc[sHash].indexOf(data[i].url) == - 1 ){
          xc[cHash].push(data[i].url);
          dbEventsOnly.push(data[i].event.trim());
          x[sHash].push({
            'url': data[i].url,
            'id':data[i].id,
            'year':data[i].year ,
            'event':data[i].event.toString() ,
            'eventWithName':data[i].name.toString()+", "+data[i].event.toString()
          });
        }
      }
      for(d in x){
        dbDataLinks.push({
          'id':x[d][0].id,
          'event':x[d][0].event,
          'eventWithName':x[d][0].eventWithName,
          'url':x[d],
          'year':x[d][0].year
        })
      }
      var i =0
      for(f in xc){
        dbDataLinks[i].url = xc[f];
        i++;
      }

      for(var k=0; k<deaths.length; k++){
        var checkingArray=[];
        var breaktag =0 ;

        var evtt = deaths[k].event;
       // var finalUrls=[];
        var finalUrls = deaths[k].birthUrl;
        var finalUrlsLowerCase=[];
        var matched= 0;
        for(var n =0; n < finalUrls.length; n++){
         finalUrlsLowerCase.push(decodeURI(finalUrls[n].url.toLowerCase()));
        }

        if(evtt && dbDataLinks.length>0){
          for(var i=0; i<dbDataLinks.length; i++){
            var evt = dbDataLinks[i].event;  //
            var evt1 = dbDataLinks[i].eventWithName;

       if(1){
              evt1 = evt1.replace(/&quot;/g, '"');
              evt1 =  evt1.replace(/&#0*39;/g, "'");
              evtt =  evtt.replace(/&quot;/g, '"');
              evtt =  evtt.replace(/&#0*39;/g, "'");
            if(deaths[k].year == dbDataLinks[i].year){
            //console.log("db==>",evt,dbDataLinks[i].year,"wikievent==>",evtt,deaths[k].year);
              var lth = natural.LevenshteinDistance(evtt,evt1);
              if(evtt.length>evt1.length){
                  var lth = ld2.computeDistance(evtt.toString().trim(),evt1.toString().trim());
               // var lth = ld(evtt.trim(),evt1.trim());
              }else{
                  var lth = ld2.computeDistance(evt1.toString().trim(),evtt.toString().trim());
                //var lth = ld(evt1.trim(),evtt.trim());
              }
              var dbEventLength  = evt1.length;
              var lthPer = lthPercent(lth,dbEventLength);
              //console.log("^^^^^^^^^^^^^^^^^^^^^^^^^^^^dbDDDDDD==>",evt1,"wikievent==>",evtt,"score=-=>",lth,"len==>",evt1.length,"percent==>",lthPer);
              if(lthPer <= 20){

              if (dbDataLinks[i].event.trim() == deaths[k].event.trim()){
                                var dbUrlLen = dbDataLinks[i].url.length;
                                var wikiUrlLen = deaths[k].birthUrl.length;

                  for(var m = 0;m<wikiUrlLen;m++){
                        for (var l = 0;l< dbUrlLen;l++ ){
                          if( typeof  deaths[k].birthUrl[m].url !='undefined'
                            && typeof dbDataLinks[i].url[l] !='undefined'
                            && deaths[k].birthUrl[m].url
                            && dbDataLinks[i].url[l] ){
                           // console.log("wiki :)))",eventsW[k].event,eventsW[k].eventUrl[m].url ,"db==>", dbDataLinks[i].event,dbDataLinks[i].url[l] );
                            if (dbUrlLen <= wikiUrlLen ){
                           // console.log("\n DEATHLenghtMatching",dbUrlLen , "  ",wikiUrlLen);
                            var dif = Math.abs(wikiUrlLen- dbUrlLen);
                            //console.log("DEATH Method call  ",dif , "breaktag= ",breaktag , "lvalue", l , "DB URL ",dbUrlLen-1);
                             if(finalUrlsLowerCase.indexOf(decodeURI(dbDataLinks[i].url[l].toLowerCase()) )==-1){
                               if (breaktag !=1 ){
                                   console.log(" DEATH Duplicate call");
                                   internalDeathEvents.push({'name':deaths[k].name,'id':dbDataLinks[i].id,'eventInternal':evt,'eventWiki':deaths[k].event,'year':deaths[k].year,'day':day,'finalUrls':deaths[k].birthUrl});
                                   breaktag =1 ;
                                }
                             }
                              if (l == dbUrlLen-1 && dif > 0 && breaktag !=1 ){
                                internalDeathEvents.push({'name':deaths[k].name,'id':dbDataLinks[i].id,'eventInternal':evt,'eventWiki':deaths[k].event,'year':deaths[k].year,'day':day,'finalUrls':deaths[k].birthUrl});
                                breaktag =1 ;

                              }else if(l == dbUrlLen-1 && dif == 0 && breaktag !=1 ){
                                        exitDeathEvents.push({'name':deaths[k].name,'id':dbDataLinks[i].id,'event':deaths[k].event,'year':deaths[k].year,'day':day,'finalUrls':deaths[k].birthUrl});
                                        breaktag =1 ;
                              }

                            }else{ //console.log("Duplicate call anotherrrrr ",breaktag);
                                 if(breaktag !=1){ //console.log("Duplicate call another ");
                                         exitDeathEvents.push({'name':deaths[k].name,'id':dbDataLinks[i].id,'event':deaths[k].event,'year':deaths[k].year,'day':day,'finalUrls':deaths[k].birthUrl});
                                          breaktag =1 ;
                                   }

                            }
                         }else{

                                if(breaktag !=1){
                                //chk for links
                                //console.log("here111",eventsW[k].event);
                                  exitDeathEvents.push({'name':deaths[k].name,'id':dbDataLinks[i].id,'event':deaths[k].event,'year':deaths[k].year,'day':day,'finalUrls':deaths[k].birthUrl});
                                    breaktag =1 ;
                                     }
                          }
                       }//**end of for dburl
                  }// ** end of for wikiurl  loop  **
                  if(wikiUrlLen == 0 )
                  {
                                    if(breaktag !=1){
                                     exitDeathEvents.push({'name':deaths[k].name,'id':dbDataLinks[i].id,'event':deaths[k].event,'year':deaths[k].year,'day':day,'finalUrls':deaths[k].birthUrl});
                                      breaktag =1 ;
                                     }
                  }
                }else{

                        if(breaktag !=1){
                                    exitDeathEvents.push({'name':deaths[k].name,'id':dbDataLinks[i].id,'event':deaths[k].event,'year':deaths[k].year,'day':day,'finalUrls':deaths[k].birthUrl});
                                    breaktag =1 ;
                                   }
                     }

              }
              else{ checkingArray.push({'lthPer':lthPer,'name':deaths[k].name,'id':dbDataLinks[i].id,'eventInternal':dbDataLinks[i].event,'eventWiki':deaths[k].event,'year':deaths[k].year,'day':day,'finalUrls':deaths[k].birthUrl});



                   }
             }
          }

          /***********/
            if(i == dbDataLinks.length-1){
             if(breaktag !=1){
           // console.log("***********Check only Here**********",checkingArray, checkingArray.length);
            checkingArray=sortByKey(checkingArray,"lthPer") ;
                if(dbEventsOnly.indexOf(deaths[k].event.trim()) == -1){
                  //chk lenght
                        if(checkingArray.length ==0) {
                          newDeathEvents.push({'name':deaths[k].name,'event':deaths[k].event,'year':deaths[k].year,'day':day,'finalUrls':deaths[k].birthUrl});
                          breaktag=1;
                        }else{
                                if(checkingArray[0].lthPer >15  && checkingArray[0].lthPer <= 35 && breaktag != 1){

                                internalDeathEvents.push({'name':checkingArray[0].name,'id':checkingArray[0].id,'eventInternal':checkingArray[0].eventInternal,'eventWiki':checkingArray[0].eventWiki,'year':checkingArray[0].year,'day':checkingArray[0].day,'finalUrls':checkingArray[0].finalUrls});
                                breaktag=1;
                              }
                              else if(checkingArray[0].lthPer >35 && breaktag != 1  ){
                                //new here
                               // console.log("TTTTTTTTTTTTTTTTTTTT",deaths[k].event);
                               newDeathEvents.push({'name':deaths[k].name,'event':deaths[k].event,'year':deaths[k].year,'day':day,'finalUrls':deaths[k].birthUrl});
                               breaktag=1;
                                }
                        }
                }else{
                   var idd = 0;
                   var dbindex = dbEventsOnly.indexOf(deaths[k].event.trim()) ;
                  // console.log('harinder-dd',dbindex, deaths[k].event.trim(),dbDataLinks.length)
                   for(var x = 0; x < dbDataLinks.length; x++){
                      var ev = dbDataLinks[x].event;
                      if(deaths[k].event.trim() == ev.trim()){
                          idd = dbDataLinks[x].id;
                          console.log('iddd-death', idd, ev)
                      }

                   }

                   if(breaktag != 1 && idd != 0){
                     exitDeathEvents.push({'name':deaths[k].name,'id':idd ,'event':deaths[k].event,'year':deaths[k].year,'day':day,'finalUrls':deaths[k].birthUrl});
                     breaktag =1 ;
                    }
                }
            }
          }
          /************/
          }
        }else{
          newDeathEvents.push({'name':deaths[k].name,'event':deaths[k].event,'year':deaths[k].year,'day':day,'finalUrls':deaths[k].birthUrl});
        }

      }

    }
  res.json({ exitDeathEvent: exitDeathEvents,internalDeaths: internalDeathEvents , NewDeaths:newDeathEvents});
  });
};





/*******************************************ENDS HERE***************************************/
//new code to get holiday items

exports.holidayItem = function(req , res){

  var holidays = req.body.wikiHolidays;
  //console.log("holidays===>",holidays);
  var exitHolidayEvents = [];
  var internalHolidayEvents = [];
  var newHolidayEvents = [];
  var internalDeathEvents =[];
  if(req.body.day && req.body.month){
    var day = req.body.day;
    var month = req.body.month;
  }
  else{
    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth();
  }
  var idarray = [];

  var sq="SELECT l.url,el.link_id ,e.event,e.id  FROM events  e left Join  events_links el  on e.id=el.event_id left join links l on el.link_id=l.id WHERE e.day = '"+ day +"' AND e.month = '"+ month +"' AND e.category_id = 39 ";
  connection.query(sq, function(err, data) {
    if (err) throw err;
    else{
      //console.log("data==>",data);
      var prev = [];
      var x = {};
      var xc = {};
      var i = data.length;
      var dbData = [];
      dbevents = [];
      while(i--) {
        var sHash = data[i].event.toString();
        var cHash = data[i].event.toString();
        if (typeof(x[sHash]) == "undefined") {
          x[sHash] = [];
          xc[cHash] = [];
        }
        if(xc[sHash].indexOf(data[i].url) == - 1 ){
          xc[cHash].push(data[i].url);
          dbevents.push(data[i].event);
          x[sHash].push({
            'url': data[i].url,
            'id':data[i].id,
            'year':data[i].year,
            'name': data[i].name
          });
        }
      }
      for(d in x){
        dbData.push({
          'id':x[d][0].id,
          'event':d,
          'url':x[d],
          'year':x[d][0].year,
          'name':x[d][0].name
        })
      }
      var i =0
      for(f in xc){
        dbData[i].url = xc[f];
        i++;
      }
      //console.log("DBHOLIDAYSEVENTS==>",dbevents);

      /******/
      for(var k =1; k < holidays.length; k++ ){
        var breaktag =0 ;
        var matched=0;
        var wikievnt = holidays[k].event;
        var checkingArray=[];
        var finalUrls = holidays[k].birthUrl;
        var finalUrlsLowerCase=[];
       // console.log("WIKIEVENT ",holidays[k]);
       for(var n =0; n < finalUrls.length;n++){
       // console.log("ROCKING",finalUrls[n].url);
         finalUrlsLowerCase.push(  decodeURI(finalUrls[n].url.toLowerCase()) );
        }

        if(wikievnt && dbData.length>0){

          for(var i = 0;i<dbData.length;i++){
            var dbevnt = dbData[i].event;
              wikievnt = wikievnt.replace(/&quot;/g, '"');
              wikievnt =  wikievnt.replace(/&#0*39;/g, "'");
              dbevnt =  dbevnt.replace(/&quot;/g, '"');
              dbevnt =  dbevnt.replace(/&#0*39;/g, "'");
            if(dbevnt.length > wikievnt.length){
             // var lth = ld(dbevnt.trim(),wikievnt.trim());
                var lth = ld2.computeDistance(dbevnt.toString().trim(),wikievnt.toString().trim());
            }else{
             // var lth = ld(wikievnt.trim(),dbevnt.trim());
                var lth = ld2.computeDistance(wikievnt.toString().trim(),dbevnt.toString().trim());
            }
            var dbEventLength  = dbevnt.length;
            var lthPer = lthPercent(lth,dbEventLength);

            if(lthPer<=10){
             if(dbData[i].event.trim() == holidays[k].event.trim()){

                var dbUrlLen = dbData[i].url.length;
                var wikiUrlLen = holidays[k].birthUrl.length;

            //      console.log("DBHOLIDAYLTH15call",dbUrlLen , wikiUrlLen , dbData[i].url  , holidays[k].birthUrl , "==&*&*&*&*&==",finalUrlsLowerCase);
                  for(var m = 0;m<wikiUrlLen;m++){
                        for (var l = 0;l< dbUrlLen;l++ ){

                          if( typeof  holidays[k].birthUrl[m].url !='undefined'
                            && typeof dbData[i].url[l] !='undefined'
                            && holidays[k].birthUrl[m].url
                            && dbData[i].url[l] ){
                           // console.log("wiki :)))",eventsW[k].event,eventsW[k].eventUrl[m].url ,"db==>", dbDataLinks[i].event,dbDataLinks[i].url[l] );

                            if (dbUrlLen <= wikiUrlLen ){
                           // console.log("\n DEATHLenghtMatching",dbUrlLen , "  ",wikiUrlLen);
                            var dif = Math.abs(wikiUrlLen- dbUrlLen);
                           // console.log("DEATH Method call  ",dif , "breaktag= ",breaktag , "lvalue", l , "DB URL ",dbUrlLen-1);
                             if(finalUrlsLowerCase.indexOf(decodeURI(dbData[i].url[l].toLowerCase()) )==-1){
                               if (breaktag !=1 ){
                                   internalHolidayEvents.push({'name':holidays[k].name,'id':dbData[i].id,'eventInternal':dbevnt,'eventWiki':holidays[k].event,'year':holidays[k].year,'day':day,'finalUrls':holidays[k].birthUrl});
                                   breaktag =1 ;
                                }
                             }
                              if (l == dbUrlLen-1 && dif > 0 && breaktag !=1 ){
                          //      internalDeathEvents.push({'name':holidays[k].name,'id':dbDataLinks[i].id,'eventInternal':evt,'eventWiki':holidays[k].event,'year':holidays[k].year,'day':day,'finalUrls':holidays[k].birthUrl});
                                  internalHolidayEvents.push({'event':dbData[i].event,'id':dbData[i].id,'finalUrls':holidays[k].birthUrl});
                                  breaktag =1 ;

                              }else if(l == dbUrlLen-1 && dif == 0 && breaktag !=1 ){
                                   //     exitDeathEvents.push({'name':holidays[k].name,'id':dbDataLinks[i].id,'event':holidays[k].event,'year':holidays[k].year,'day':day,'finalUrls':holidays[k].birthUrl});
                                   exitHolidayEvents.push({'event':holidays[k].event,'finalUrls':holidays[k].birthUrl});
                                   breaktag =1 ;
                              }

                            }else{ //console.log("Duplicate call anotherrrrr ",breaktag);
                                 if(breaktag !=1){ //console.log("Duplicate call another ");
                                      //    exitDeathEvents.push({'name':deaths[k].name,'id':dbDataLinks[i].id,'event':holidays[k].event,'year':holidays[k].year,'day':day,'finalUrls':holidays[k].birthUrl});
                                    exitHolidayEvents.push({'event':holidays[k].event,'finalUrls':holidays[k].birthUrl});
                                    breaktag =1 ;
                                   }

                            }
                         }else{

                                if(breaktag !=1){
                                //chk for links
                                //console.log("here111",eventsW[k].event);
                                   // exitDeathEvents.push({'name':holidays[k].name,'id':dbDataLinks[i].id,'event':holidays[k].event,'year':holidays[k].year,'day':day,'finalUrls':holidays[k].birthUrl});
                                   exitHolidayEvents.push({'event':holidays[k].event,'finalUrls':holidays[k].birthUrl});
                                   breaktag =1 ;
                                     }
                          }
                       }//**end of for dburl
                  }// ** end of for wikiurl  loop  **
                  if(wikiUrlLen == 0 )
                  {
                                    if(breaktag !=1){
                                     exitHolidayEvents.push({'event':holidays[k].event,'finalUrls':holidays[k].birthUrl});
                                     breaktag =1 ;
                                     }
                  }
                }else{

                        if(breaktag !=1){
                                   //exitDeathEvents.push({'name':holidays[k].name,'id':dbDataLinks[i].id,'event':holidays[k].event,'year':holidays[k].year,'day':day,'finalUrls':holidays[k].birthUrl});
                                   exitHolidayEvents.push({'event':holidays[k].event,'finalUrls':holidays[k].birthUrl});
                                   breaktag =1 ;
                                   }
                     }


            }
            else{
              checkingArray.push({'lthPer':lthPer,'id':dbData[i].id,'eventInternal':dbData[i].event,'eventWiki':holidays[k].event,'finalUrls':holidays[k].birthUrl});
               }
/***********************/
 if(i == dbData.length-1){
             if(breaktag !=1){
           // console.log("***********Check only Here**********",checkingArray, checkingArray.length);
            checkingArray=sortByKey(checkingArray,"lthPer") ;
             console.log("reached ttt 5");

                if(dbevents.indexOf(holidays[k].event.trim()) == -1){
                  //chk lenght
                  if(checkingArray.length ==0)
                  {
                    newHolidayEvents.push({'event':holidays[k].event,'finalUrls':holidays[k].birthUrl});
                   // newDeathEvents.push({'name':holidays[k].name,'event':holidays[k].event,'year':holidays[k].year,'day':day,'finalUrls':holidays[k].birthUrl});
                    breaktag=1;
                  }else{
                        //console.log("@@@@@@******@@@@@@",checkingArray[0]);
                        if(checkingArray[0].lthPer >15  && checkingArray[0].lthPer <= 35 && breaktag != 1){
                        //duplicate
                        //  internalEvents.push({'id':checkingArray[0].id,'eventInternal':checkingArray[0].eventInternal,'eventWiki':checkingArray[0].eventWiki,'year':checkingArray[0].year,'day':checkingArray[0].day,'finalUrls':checkingArray[0].finalUrls});
                         // internalDeathEvents.push({'name':checkingArray[0].name,'id':checkingArray[0].id,'eventInternal':checkingArray[0].eventInternal,'eventWiki':checkingArray[0].eventWiki,'year':checkingArray[0].year,'day':checkingArray[0].day,'finalUrls':checkingArray[0].finalUrls});
                          internalHolidayEvents.push({'event':checkingArray[0].eventWiki,'id':checkingArray[0].id,'finalUrls':checkingArray[0].finalUrls });
                          breaktag=1;
                        }
                        else if(checkingArray[0].lthPer >35 && breaktag != 1  ){
                         //newDeathEvents.push({'name':deaths[k].name,'event':deaths[k].event,'year':deaths[k].year,'day':day,'finalUrls':deaths[k].birthUrl});
                          newHolidayEvents.push({'event':holidays[k].event,'finalUrls':holidays[k].birthUrl});
                          breaktag=1;
                          }
                  }


                }else{
                   if(breaktag != 1){
                      //exitDeathEvents.push({'name':deaths[k].name,'id':dbDataLinks[i].id,'event':deaths[k].event,'year':deaths[k].year,'day':day,'finalUrls':deaths[k].birthUrl});
                       exitHolidayEvents.push({'event':holidays[k].event,'finalUrls':holidays[k].birthUrl});
                       breaktag =1 ;
                    }
                }
            }
          }
/*************************/
          }//end of for
        }else{

          if(breaktag !=1){
              newHolidayEvents.push({'event':holidays[k].event,'finalUrls':holidays[k].birthUrl});
              breaktag = 1 ;
          }

        }
      }
    }
    res.json({ exitHolidayEvents:exitHolidayEvents, NewHolidays:newHolidayEvents , internalHolidays:internalHolidayEvents});
  });
};



//Save duplicate events SACHIN

exports.savedupEvents = function(req , res){
  //console.log("data",req.body);
  for (var j = 0; j<req.body.length;j++){
    var evt = req.body[j].event;
    var eventid = req.body[j].eventid;
    var evnt = evt.replace(/'/g, "''");

    var sq = "UPDATE events SET event ='"+evnt+"' WHERE id ='"+eventid+"'";
    abc(sq, function(data){
      console.log("Successfully updated");
    });
    if(req.body.length - 1 == j){
      res.json({"message":"DONE"});
    }

  }
  function abc(qur, callback){
    connection.query(qur, function(err, data) {
      if (err)
        throw err;
      else{
        // console.log("Successfully updated");
      }
      callback('OK');
    });
  }


}

//Delete the existing events indivisually
exports.deleteExistEvents = function(req , res){
  console.log("data===>",req.body);
  var event_id = req.body.eventid;
  var category_id = req.body.category;
  var dbevent = req.body.event;
  if (typeof event_id == 'undefined'){
    var sq = "DELETE FROM events  WHERE event  = '"+ dbevent +"' AND category_id = '"+ category_id +"'" ;
  }else{
    var sq = "DELETE FROM events  WHERE id  = '"+ event_id +"' AND category_id = '"+ category_id +"'" ;
  }
  connection.query(sq, function(err, data) {
    if (err) throw err;
    else{
      res.json({"message":"DONE"});
    }
  });
}

function lthPercent  (score, length) {
  var percent = (score/length)*100;
  return percent;
}

function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = parseInt(a[key]); var y =parseInt(b[key]);
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}
