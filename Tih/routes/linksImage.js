var fs = require('fs');
var AWS = require('aws-sdk');
var request= require('request');
var easyimg = require('easyimage');
var im = require('imagemagick');
var config = require("../lib/config");

var cp = require('child_process');
var child = cp.fork('./worker');

var nodemailer = require("nodemailer");

var accessKeyId =  process.env.AWS_ACCESS_KEY || "xxxxxx";
var secretAccessKey = process.env.AWS_SECRET_KEY || "+xxxxxx+B+xxxxxxx";
AWS.config.update({
  accessKeyId: config.AWS[0].accessKeyId,
  secretAccessKey: config.AWS[0].secretAccessKey
});

var s3 = new AWS.S3();

var connection;
// callback
 var myCallback = function(connection) {
   connection.connect(function(err) {
    if(err) {
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000);
    }
  });
   connection.on('error', function(err) {
    if(err.code === 'PROTOCOL_CONNECTION_LOST') {
      console.log('links enter');
      usingItNow(myCallback);
    } else {
      throw err;
    }
  });
 };
 var usingItNow = function(callback) {
  connection = require('../db.js').handleDisconnect();
   callback(connection);
 };
 usingItNow(myCallback);

//TO GET THE LIST OF ALL EVENTS LIST
exports.saveLinkData = function(req, res){
  var id = req.body.id;
  var image_url = req.body.imageUrl;
  var mImg = req.body.mImg;
  var sImg = req.body.sImg;
  var sql = 'UPDATE links SET image_url = ?, medium_image_url = ?, small_image_url= ? WHERE id ='+id;
  connection.query(sql, [image_url, mImg, sImg], function(err, events) {
    if (err) throw err;
    else{
      res.json(events);
    }
  });
};

exports.getLinkData = function(req, res){
  var id = req.body.id;
  connection.query( 'SELECT * FROM links WHERE id = ?',[id],function(err, events) {
    if (err) throw err;
    else{
      res.json(events);
    }
  });
};

// Upload Image to Server....
exports.uploadImage = function(req, res) {
  var imageUrl = req.body.url;
  var r_width;
  var download = function(uri, callback){
    request.head(uri, function(err, res, body){
      console.log('content-type:', res.headers['content-type']);
      console.log('content-length:', res.headers['content-length']);
      request(uri).pipe(fs.createWriteStream('./public/uploads/img.png')).on('close', callback);
    });
  };

  var cropImg = function(callback) {
    console.log('w:',req.body.w,", h:",req.body.h,",x:",req.body.x1, ',y:',req.body.y1);
      easyimg.crop({
            src:'./public/uploads/img.png', dst:'./public/uploads/img.png',
            cropwidth:req.body.w, cropheight:req.body.h,
            gravity:'NorthWest',
            x:req.body.x1, y:req.body.y1
        },
        function(err, stdout, stderr) {
            if (err) throw err;
            if(req.body.w > 2040) {
              im.resize({
                srcPath: './public/uploads/img.png',
                dstPath: './public/uploads/img.png',
                width:   2040
              }, function(err, stdout, stderr){
                if (err) throw err;
                console.log('Cropped Resized!!!!');
                callback();
              });
            }else{
              callback();
            }
      });
  };
  var checkImgDimensions = function(callback){
    im.identify('./public/uploads/img.png', function(err, features){
      if (err) throw err;
      console.log(features.width);
      if(typeof features.width != 'undefined')
        r_width = features.width;
      else
        r_width = 800;
      callback();
    });
  };

  download(imageUrl, function(){
    if(typeof req.body.w != 'undefined') {
        cropImg(function(){
           mainFunction(req.body.mSize, req.body.sSize);
        });
    }
    else {
      checkImgDimensions(function(){
        if(r_width < 640) {
          medium = r_width;
          if(medium > 240)
            small = 240;
          else
            small = r_width/2;
        }
        else {
            medium = 640;
            small = 240;
        }
        mainFunction(medium, small);
      });
    }
  });

  function mainFunction(medium, small){
    console.log("Widths" , medium, small);
    var dateObject = new Date();
    var uniqueId =
      dateObject.getFullYear() + '' +
      dateObject.getMonth() + '' +
      dateObject.getDate() + '' +
      dateObject.getTime();
    var path      = './public/uploads/img.png';
    var imgName   = uniqueId+'.png';
    var mediumImg = uniqueId+'_medium'+'.png';
    var smallImg  = uniqueId+'_small'+'.png';


    // Create Full Size Image and Upload on S3
    fs.readFile(path, function(err, file_buffer){
      var params = {
        Bucket: config.AWS[0].aws_Bucket,
        Key: imgName,
        Body: file_buffer,
        ContentType:'image/jpeg',
        ACL: 'public-read',
      };
      s3.putObject(params, function (perr, pres) {
        if (perr) {
          console.log("Error uploading data: ", perr);
        } else {
          var result = {'image':'https://s3.amazonaws.com/today_in_history/'+imgName}
          res.json(result);
          console.log('https://s3.amazonaws.com/today_in_history/'+imgName);
        }
      });
      s3.client.getObject(params, function(err, data) {
        if (err) {
          console.log("Error uploading data: ", err);
        } else {
          console.log(data);
        }
      });
     });

    // Create Small Size Image and Upload on S3
    im.resize({
        srcPath: './public/uploads/img.png',
        dstPath: './public/uploads/img_small.png',
        width:   small
    }, function(err, stdout, stderr){
      if (err) throw err;
      var smallPath = "./public/uploads/img_small.png";
      fs.readFile(smallPath, function(err, file_buffer){
        var params = {
          Bucket: config.AWS[0].aws_Bucket,
          Key: smallImg,
          Body: file_buffer,
          ContentType:'image/jpeg',
          ACL: 'public-read',
        };
        s3.putObject(params, function (perr, pres) {
          if (perr) {
            console.log("Error uploading data: ", perr);
          } else {
            var result = {'small_image':'https://s3.amazonaws.com/today_in_history/'+smallImg}
            // res.json(result);
          }
        });
        s3.client.getObject(params, function(err, data) {
          if (err) {
            console.log("Error uploading data: ", err);
          } else {
            console.log(data);
          }
        });
      });
    });

     // Create Medium Size Image and Upload on S3
     im.resize({
        srcPath: './public/uploads/img.png',
        dstPath: './public/uploads/img_medium.png',
        width:   medium
     }, function(err, stdout, stderr){
      if (err) throw err;
      console.log('resized-640px');
        var mediumPath = "./public/uploads/img_medium.png";
        fs.readFile(mediumPath, function(err, file_buffer){
          var params = {
            Bucket: config.AWS[0].aws_Bucket,
            Key: mediumImg,
            Body: file_buffer,
            ContentType:'image/jpeg',
            ACL: 'public-read',
          };
          s3.putObject(params, function (perr, pres) {
            if (perr) {
              console.log("Error uploading data: ", perr);
            } else {
              var result = {'medium_image':'https://s3.amazonaws.com/today_in_history/'+mediumImg}
              // res.json(result);
            }
          });
          s3.client.getObject(params, function(err, data) {
            if (err) {
              console.log("Error uploading data: ", err);
            } else {
              console.log(data);
            }
          });
        });
      });
   }
};

/// Upload From Local ......
exports.localUpload = function(req, res) {
  var image = req.body.image;
  var r_width;
  var data = image.replace(/^data:image\/\w+;base64,/, "");
  var buf = new Buffer(data, 'base64');
  var date = new Date();
  var dateObject = new Date();
  var uniqueId =
    dateObject.getFullYear() + '' +
    dateObject.getMonth() + '' +
    dateObject.getDate() + '' +
    dateObject.getTime();

  var cropImg = function(callback) {
    easyimg.crop({
            src:'./public/uploads/img.png', dst:'./public/uploads/img.png',
            cropwidth:req.body.w, cropheight:req.body.h,
            gravity:'NorthWest',
            x:req.body.x1, y:req.body.y1
        },
        function(err, stdout, stderr) {
            if (err) throw err;
            if(req.body.w > 2040) {
              im.resize({
                srcPath: './public/uploads/img.png',
                dstPath: './public/uploads/img.png',
                width:   2040
              }, function(err, stdout, stderr){
                if (err) throw err;
                console.log('Cropped Resized!!!!');
                callback();
              });
          }else{
            callback();
          }

        }
      );
  };
  var checkImgDimensions = function(callback){
    im.identify('./public/uploads/img.png', function(err, features){
      if (err) throw err;
      console.log(features.width);
      if(typeof features.width != 'undefined')
        r_width = features.width;
      else
        r_width = 800;
      callback();
    });
  };
  fs.writeFile("./public/uploads/img.png", buf , function(err){
    console.log('w:',req.body.w,", h:",req.body.h,",x:",req.body.x1, ',y:',req.body.y1);
    if(typeof req.body.w != 'undefined') {
      cropImg(function(){
         mainFunction(req.body.mSize, req.body.sSize);
      });
    }
    else {
      checkImgDimensions(function(){
        if(r_width < 640) {
          medium = r_width;
          if(medium > 240)
            small = 240;
          else
            small = r_width/2;
        }
        else {
            medium = 640;
            small = 240;
        }
        console.log("dimensions", medium, small);
        mainFunction(medium, small);
      });
    }
  });
  function mainFunction(medium, small){
    var dateObject = new Date();
    var uniqueId =
      dateObject.getFullYear() + '' +
      dateObject.getMonth() + '' +
      dateObject.getDate() + '' +
      dateObject.getTime();
      var path = './public/uploads/img.png';
      var imgName   = uniqueId+'.png';
      var mediumImg = uniqueId+'_medium'+'.png';
      var smallImg  = uniqueId+'_small'+'.png';

    fs.readFile(path, function(err, file_buffer){
      var params = {
        Bucket: config.AWS[0].aws_Bucket,
        Key: imgName,
        Body: file_buffer,
        ContentType:'image/jpeg',
        ACL: 'public-read',
      };
      s3.putObject(params, function (perr, pres) {
        if (perr) {
          console.log("Error uploading data: ", perr);
        } else {
          console.log(pres);
          var result = {'image':'https://s3.amazonaws.com/today_in_history/'+imgName}
          res.json(result);
          console.log("Successfully uploaded data to myBucket/myKey");
        }
      });
      s3.client.getObject(params, function(err, data) {
        if (err) {
          console.log("Error uploading data: ", err);
        } else {
          console.log(data);
        }
      });
    });

    // Create Small Size Image and Upload on S3
     im.resize({
        srcPath: './public/uploads/img.png',
        dstPath: './public/uploads/img_small.png',
        width:   small
     }, function(err, stdout, stderr){
      if (err) throw err;
      var smallPath = "./public/uploads/img_small.png";
      fs.readFile(smallPath, function(err, file_buffer){
      var params = {
        Bucket: config.AWS[0].aws_Bucket,
        Key: smallImg,
        Body: file_buffer,
        ContentType:'image/jpeg',
        ACL: 'public-read',
      };
      s3.putObject(params, function (perr, pres) {
        if (perr) {
          console.log("Error uploading data: ", perr);
        } else {
          var result = {'image':'https://s3.amazonaws.com/today_in_history/'+smallImg}
        }
      });
      s3.client.getObject(params, function(err, data) {
        if (err) {
          console.log("Error uploading data: ", err);
        } else {
          console.log(data);
        }
      });
      });
    });

      // Create Medium Size Image and Upload on S3
      im.resize({
        srcPath: './public/uploads/img.png',
        dstPath: './public/uploads/img_medium.png',
        width:   medium
      }, function(err, stdout, stderr){
      if (err) throw err;
        var mediumPath = "./public/uploads/img_medium.png";
        fs.readFile(mediumPath, function(err, file_buffer){
          var params = {
            Bucket: config.AWS[0].aws_Bucket,
            Key: mediumImg,
            Body: file_buffer,
            ContentType:'image/jpeg',
            ACL: 'public-read',
          };
          s3.putObject(params, function (perr, pres) {
            if (perr) {
              console.log("Error uploading data: ", perr);
            } else {
              var result = {'image':'https://s3.amazonaws.com/today_in_history/'+mediumImg}
            }
          });
          s3.client.getObject(params, function(err, data) {
            if (err) {
              console.log("Error uploading data: ", err);
            } else {
              console.log(data);
            }
          });
        });
      });
  }
};

//Parser Image Upload ......
var linskupload = [];


// Run process in background
exports.addImagesUrls = function(req, res){
    res.send('Process Started')
    soc = req.app.locals.someVar;
 // soc.emit('message', { message: 'background-process' });
    child.send('Process Started');
};
var soc = 0;
exports.test = function(req ,res){
    soc = req.app.locals.someVar;
    child.send('Process Started');
    res.send('Done')
}
child.on('message', function(m) {
  console.log('bakkkkk')
  var stat = m;
  var con = stat.split("-")[0];
  var len = stat.split("-")[1];
  if(con == len) {
    sendMail('100%');
  }
  if(con == Math.round(len / 2)){
    sendMail('50%');
  }
  soc.emit('message', { message: 'background-process',status : m});
  console.log('received: ' , m);
});


function sendMail(len){
      var smtpTransport = nodemailer.createTransport("SMTP",{
          service: "Gmail",
          auth: {
              user: config.MAIL[0].fromEmail,
              pass: config.MAIL[0].password
          }
      });
      var mailOptions = {
          //to: "manuel.zamora.86@gmail.com",
          from: config.MAIL[0].fromEmail,
          to: config.MAIL[0].toEmail,
          subject: "Links Updated - "+len,
          html: len+" = Links are Saved and images uploaded!"
      }
      smtpTransport.sendMail(mailOptions, function(error, response){
          if(error){
              console.log(error);
              console.log('massege','Try again');
          }else{
              console.log('massege', 'Mail sent to your Email');
          }
          smtpTransport.close();
      });
}
process.on('SIGINT',function(){
    child.kill("SIGINT");
});
process.on('stdout',function(d){
    child.kill("stdout",d);
});

process.on('exit', function() {
  console.log('process is about to exit, kill ffmpeg');
  child.kill(child.pid);
  process.kill(child.pid);
});

child.on('close', function (code) {
    console.log('Worker - child process exited with code ' + code);
    process.kill(process.pid);
});


//Parser Image Upload ......
exports.uploadParserImage = function(req, res) {
    console.log('request',req.body.counter);
    console.log(req.body.url);
    var count = req.body.counter;
    var image_index_val = req.body.image_index;
    var imageUrl = req.body.url;
    var link_id = req.body.link_id;
    var index = req.body.index;
    var r_width;
    var ext = imageUrl.split('.').pop().split(/\#|\?/)[0];
    if(typeof ext == 'undefined')
        ext = 'png';
    if(ext == 'svg')
        ext ='svg';
    else
        ext = 'png';

    var download = function(uri, count ,link_id ,image_index_val,ext,callback){
        request.head(uri, function(err, res, body){
            request(uri).pipe(fs.createWriteStream('./public/uploads/'+count+'img'+link_id+'.'+ext)).on('close', callback);
        });
    };
    download(imageUrl,count,link_id,image_index_val,ext, function(){
        var medium = 640;
        var small = 240;
        if(ext == 'svg'){
            console.log('SVG EXT');
          im.convert(['./public/uploads/'+count+'img'+link_id+'.'+ext, '-resize', '800x600', './public/uploads/'+count+'img'+link_id+'.png'],
            function(err, stdout){
              if (err) console.log(err);
               mainFunctiion(medium, small , count, link_id, index, image_index_val);
              console.log('stdout:', stdout);
           });
        }else{
            mainFunctiion(medium, small , count, link_id, index, image_index_val);
        }
    });

    function mainFunctiion(medium , small , count , link_id , index ,image_index_val, next){
        var dateObject = new Date();
        var image_index_vall = image_index_val;
        var uniqueId =
            dateObject.getFullYear() + '' +
            dateObject.getMonth() + '' +
            dateObject.getDate() + '' +
            dateObject.getTime();
        var path      = './public/uploads/'+count+'img'+link_id+'.png';
        var imgName   = uniqueId+'.png';
        var mediumImg = uniqueId+'_medium'+'.png';
        var smallImg  = uniqueId+'_small'+'.png';
        var cl = count+'img'+link_id;
        fs.readFile(path, function(err, file_buffer){
            var params = {
                Bucket: config.AWS[0].aws_Bucket,
                Key: imgName,
                Body: file_buffer,
                ContentType:'image/jpeg',
                ACL: 'public-read',
            };
            s3.putObject(params, function (perr, pres) {
                if (perr) {
                    console.log("Error uploading data: ", perr);
                    res.json(perr);
                } else {
                    console.log(pres);
                    var result = {'image':'https://s3.amazonaws.com/today_in_history/'+imgName ,'small_image':'https://s3.amazonaws.com/today_in_history/'+smallImg, 'medium_image':'https://s3.amazonaws.com/today_in_history/'+mediumImg,'indx':count,'link_id':link_id,'indexR':index , 'image_index':image_index_vall}
                    console.log("Successfully uploaded data to myBucket/myKey", result);
                    res.json(result);
                }
            });
        });

        // Create Medium Size Image and Upload on S3
        im.resize({
            srcPath: './public/uploads/'+count+'img'+link_id+'.png',
            dstPath: './public/uploads/img_medium'+count+'img'+link_id+'.png',
            width:   medium
        }, function(err, stdout, stderr){
            if (err) {
                console.log('error');
                res.json(err);
            }
            console.log('resized-640px');
            //fs.unlink('./public/uploads/'+count+'img'+link_id+'.png');
            var mediumPath = "./public/uploads/img_medium"+count+'img'+link_id+".png";
            fs.readFile(mediumPath, function(err, file_buffer){
                var params = {
                    Bucket: config.AWS[0].aws_Bucket,
                    Key: mediumImg,
                    Body: file_buffer,
                    ContentType:'image/jpeg',
                    ACL: 'public-read',
                };
                s3.putObject(params, function (perr, pres) {
                    if (perr) {
                        console.log("Error uploading data: ", perr);
                    } else {
                        setTimeout(function(){
                            fs.unlink("./public/uploads/img_medium"+count+'img'+link_id+".png");
                        },5000);
                    }
                });

            });
        });
        im.resize({
            srcPath: './public/uploads/'+count+'img'+link_id+'.png',
            dstPath: './public/uploads/img_small'+count+'img'+link_id+'.png',
            width:   small
        }, function(err, stdout, stderr){
            if (err) {
                console.log('error');
                res.json(err);
            }
            console.log('resized-640px');
            var smallPath = "./public/uploads/img_small"+count+'img'+link_id+".png";
            fs.readFile(smallPath, function(err, file_buffer){
                var params = {
                    Bucket: config.AWS[0].aws_Bucket,
                    Key: smallImg,
                    Body: file_buffer,
                    ContentType:'image/jpeg',
                    ACL: 'public-read',
                };
                s3.putObject(params, function (perr, pres) {
                    if (perr) {
                        console.log("Error uploading data: ", perr);
                    } else {
                        setTimeout(function(){
                            fs.unlink("./public/uploads/img_small"+count+'img'+link_id+".png");
                        },6000)
                        setTimeout(function(){
                            fs.unlink('./public/uploads/'+count+'img'+link_id+'.png');
                            fs.unlink('./public/uploads/'+count+'img'+link_id+'.svg');
                        },40000)
                    }
                });
            });
        });
    }
};

