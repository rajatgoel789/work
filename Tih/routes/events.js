var connection;
// callback
 var myCallback = function(connection) {
   connection.connect(function(err) {
    if(err) {
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000);
    }
  });
   connection.on('error', function(err) {
    if(err.code === 'PROTOCOL_CONNECTION_LOST') {
      console.log('event enter');
      usingItNow(myCallback);
    } else {
      throw err;
    }
  });
 };
 var usingItNow = function(callback) {
  connection = require('../db.js').handleDisconnect();
   callback(connection);
 };
 usingItNow(myCallback);



//TO GET THE LIST OF ALL EVENTS LIST
exports.events = function(req, res){
  var d = new Date();
  if(req.body.day){
    var day = req.body.day;
  }else{
  var day = d.getDate();
  }
  //console.log(day);
  if(req.body.month){
    var month = req.body.month;
  }
  else{
    var month = d.getMonth();
    month = month + 1;
  }
  var sql = "SELECT * FROM events  WHERE day = '"+ day +"' AND month = '"+ month +"' ORDER BY year DESC";

  connection.query(sql, function(err, events) {
    if (err) throw err;
    else{
      res.json(events);
    }
  });
};

//TO GET THE EVENTS ACCORDING TO CATEGORY
exports.eventType = function(req, res){
  if(req.body.day && req.body.month)
  {
    var day = req.body.day;
    var month = req.body.month;
  }
  else{
    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth();
  }
  var order = req.body.order;

  console.log('selectedddddDatwe', req.body.day);
  console.log('selectedMonthhhhhhh', month);
  var category = req.body.category;
  var sql = "SELECT * FROM events  WHERE day = '"+ day +"' AND month = '"+ month +"' AND category_id = '"+ category +"' ORDER BY year "+order;
  connection.query(sql, function(err, events) {
    if (err) throw err;
    else{
      res.json(events);
    }
  });
};

exports.post = function(req, res){
  var id = req.params.id;
  var sql = "SELECT * FROM events WHERE id = '"+id+"' ";
  connection.query(sql, function(err, data){
    if(err) throw err;
    else{
      res.json(data);
    }
  });
};

exports.editEvn = function(req, res){
  aName = req.body.name;
  name = req.body.event;
  //name = mysql_real_escape_string(name)
  day = req.body.day;
  month = req.body.month;
  year = req.body.year;
  title = req.body.title;
  category_id = req.body.category_id;
  link_id = req.body.link_id;
  //sql = "UPDATE events SET event = ?, day = ? , month = ?, year = ?, category_id = ? WHERE id = '"+req.params.id+"' ", [name, day, month, year, category_id];
  connection.query("UPDATE events SET title = ? , name = ?,event = ?, day = ? , month = ?, year = ?, category_id = ? , link_id = ? WHERE id = '"+req.params.id+"' ", [title , aName , name, day, month, year, category_id , link_id], function(err, docs) {
    if (err) res.json(err);
    else{
      //res.render('editPost', { message: req.flash('You have updated event sucessfully') });
      res.json(docs);
    }
  });
  //res.json('data');
};

exports.linkDelete = function(req , res){
  var sql = 'DELETE FROM events_links WHERE event_id ='+req.params.id;
  connection.query(sql, function(err, result) {
    if (err) {
      console.log(err);
    }
    else{
      res.json('deleted');
    }
  });
};
exports.addLink = function(req, res){
  var link_id = req.body.link_id;
  console.log('id', req.params.id);
 var sql = 'DELETE FROM events_links WHERE event_id ='+req.params.id;
  connection.query('INSERT INTO events_links (link_id, event_id) VALUES (? , ?)',[link_id, req.params.id], function(err, result) {
  if (err) {
    console.log(err);
  }
  else{
    res.json('adding');
}
});


};
exports.search = function(req, res){
  var name = req.params.name;
  var sql = "SELECT * FROM links WHERE name LIKE '%"+name+"%'  LIMIT 25";
  connection.query(sql, function(err , data){
    if(err) throw err;
    else{
      res.json(data);
    }
  });
};
exports.eventlinks = function(req, res){
  console.log("sachin");
  var id = req.params.id;
  var sql = "SELECT events_links.link_id, links.id, links.name, links.url, links.image_url FROM events_links INNER JOIN links ON events_links.link_id = links.id AND events_links.event_id = "+id+"";
  connection.query(sql, function(err , data){
    if(err) throw err;
    else{
      res.json(data);
    }
  });
};


//to add new events to database indivisually
exports.addNewEvent = function(req , res){

  var aName = req.body.name;
  var name = req.body.event;

  var day = req.body.day;
  var month = req.body.month;
  var year = req.body.year;
  var category_id = req.body.category_id;
  var link_id = req.body.link_id;
  var title = req.body.title;
  var index = req.body.index;
  var urls = req.body.urls;
  var status = req.body.status;
  if (status == null){
    var status = "";
  }
  console.log('addNewEven events','year',year,'month',month,day,'name',name, 'aName', aName ,'title',title );
  connection.query('INSERT INTO events (title,name,event,day,month,year,category_id,link_id,status) VALUES (? , ? , ? , ? , ? , ? , ? , ? ,?)', [title,aName,name,day,month,year,category_id,link_id ,status], function(err, result) {
    if (err) {
      console.log(err);
    }
    else{
      //console.log("result==>",result);
      res.json({'result':result , 'urlsName':urls,'indexR':index});
    }
  });
}

//Add New events which are fetched from wiki
exports.addNewEvents = function(req , res){
  //var eventsArray = req.body.allEvents;
  var counter = req.body.counter;
  var month = req.body.month;
  var day = req.body.day;
  var status = req.body.status;
  var year = req.body.year;
  var category_id = req.body.category_id;
  if(category_id == 36 || category_id == 39){
    var name = '';
  }else{
    var name = req.body.name;
    //name = mysql_real_escape_string(name)
  }
  var event = req.body.event;
    //event = mysql_real_escape_string(event)
  var urls = req.body.urls;

    connection.query('INSERT INTO events (name, event, year, month, day , category_id , status) VALUES (? , ? , ? , ?, ? , ? , ?);', [name , event , year , month , day , category_id , status], function(err, results) {
      if(err)
        res.json(err);
      else
        res.json({'urlsName':urls,'result':results,'counter':counter});
    });
 
};

exports.addNewLink_new = function(req , res){
  var count = req.body.counter;
  var name = req.body.name;
  var url = req.body.url;
  var imageUrl = req.body.image;
  var status = 0;
  var eid = req.body.eventId;
  var newArray = [];
  console.log("URL----", url);
  connection.query( 'SELECT *FROM links WHERE url = ? ',[url],function(err, data) {
    if (err) {
        throw err;
    }
    else{
      if (data.length == 0) {
        connection.query('INSERT INTO links (name , url) VALUES (? , ?);', [name , url], function(err, results) {
          if(err){
            res.json(err);
          }
          else{
              var lnkId = results.insertId;
              connection.query('INSERT INTO update_links (link_id , status, img_link, url) VALUES (? , ?, ?, ?);', [lnkId , status, imageUrl, url], function(err, results1) {
                  if(err){
                      console.log('Error', err)
                  }
              });
              res.json({'res':results,'counter2':count,'event_id':eid});
          }
        });
    }else{
		console.log("---if---",url , data[0].image_url,data[0].id);
         connection.query('UPDATE links SET url = ? WHERE url = ?', [url, data[0].url],function(err, results) {});
        if(data[0].image_url == null || data[0].image_url == "" || data[0].medium_image_url == "" || data[0].small_image_url == "" ){
            console.log("---else---",url , data[0].image_url);
            //  console.log('Full null --- ',data[0].id , status, imageUrl ,name )
            //  console.log('Null called',data[0].id , status, imageUrl , name)
              connection.query('INSERT INTO update_links (link_id , status, img_link, url) VALUES (? , ?, ?, ?);', [data[0].id , status, imageUrl, url], function(err, results1) {
                    if(err){
                        console.log('Error', err)
                    }
                });
            }
        res.json({'event_id':eid,'link_id':data[0].id });
      }
    }
  });
};


exports.addNewLink = function(req , res){
  //var eventsArray = req.body.allEvents;
  console.log("request here sachin==>",req.body.url);
  var count = req.body.count;
  var name = req.body.name;
    //name = mysql_real_escape_string(name)
  var url = req.body.url;
  var image_url = req.body.image;
  var small_image = req.body.small_image;
  var medium_image = req.body.medium_image;
  var id = req.body.link_id;
  var index = req.body.index;
  var image_index_val = req.body.image_index;

  var newArray = [];

  connection.query( 'SELECT id,image_url FROM links WHERE url = ?',[url],function(err, data) {
    if (err) throw err;
    else{
      //console.log("data==>",data);
      if (data.length == 0){
        connection.query('INSERT INTO links (name , url , image_url , medium_image_url , small_image_url) VALUES (? , ? , ? , ? , ?);', [name , url , image_url , medium_image , small_image], function(err, results) {
          if(err){
            console.log('duplicate',err);
            res.json(err);
          }
          else
            console.log("results--->",results);
            res.json({'res':results,'counter2':count,'event_id':id,'indexR':index , 'image_index':image_index_val});
        });
      }else{
          connection.query('UPDATE links SET image_url = ?, medium_image_url = ?, small_image_url = ? WHERE id = ?', [image_url, medium_image, small_image , data[0].id],function(err, results) {
                        if(err)
                            console.log(err);
                          else
                            console.log('Updated',url)
           });
        res.json({'event_id':id,'link_id':data[0].id,'counter2':count,'indexR':index ,'image_index':image_index_val });
      }
    }
  });
};


exports.addEventLink = function(req , res){
  var eventId = req.body.event_id;
  var linkId = req.body.linkId;
  var index = req.body.index;
  var image_index_val = req.body.image_index;

    connection.query("INSERT INTO events_links (link_id, event_id) VALUES ('"+ linkId +"' , '"+ eventId +"');", function(err, results) {
      if(err){
          console.log("addEventLink Error",err);
          res.json(err);
      }
      else{
        res.json({'result':results,'indexR':index , 'image_index':image_index_val});
     }
     });
};

exports.updateEvent = function(req , res){
  var id = req.body.id;
  var event = req.body.event;
  var aName = req.body.name;
    //aName = mysql_real_escape_string(aName)
  var name = req.body.event;
    //name = mysql_real_escape_string(name)
  var day = req.body.day;
  var month = req.body.month;
  var year = req.body.year;
  var category_id = req.body.category_id;
  var link_id = req.body.link_id;
  var title = req.body.title;
  var index = req.body.index;
  var urls = req.body.urls;
  var status = req.body.status;
  if (status == null){
    var status = "";
  }
  connection.query('UPDATE events SET event = ? , name = ? WHERE id = ?', [event, aName, id],function(err, results) {
      if(err){
          res.json(err)
      }else{
          res.json({'result':results , 'urlsName':urls,'indexR':index,'event_id':id});
          //console.log("event_id results==>",results,event);
      }
  });

};

//noImageLink
exports.noImageLink = function(req , res){

  var url = req.body.url;
  var newArray = [];

    connection.query( 'SELECT id FROM links WHERE url = ?',[url],function(err, data) {
    if (err) throw err;
    else{
      if (data.length == 0){
        connection.query('INSERT INTO links (name , url , image_url , medium_image_url , small_image_url) VALUES (? , ? , ? , ? , ?);', ['' , url , '' , '' , ' '], function(err, results) {
          if(err){
            console.log(err);
            res.json(err);
          }
          else
            console.log("results--->",results);
            res.json({'result':results});
        });

      }else{
        res.json({'link_id':data[0].id});
      }
    }
  });
}

//Get link connected to event connectLinkEvent
exports.connectLinkEvent = function(req , res){
    var event_id = req.body.event_id;
    var link_id = req.body.linkId;

    connection.query('INSERT INTO events_links (link_id, event_id) VALUES (? , ?);', [link_id , event_id], function(err, results) {
        if(err)
          res.json(err);
        else
          res.json({"Message":"Success"});
    });

}

function mysql_real_escape_string (str) {
    return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
        switch (char) {
            case "\0":
                return "\\0";
            case "\x08":
                return "\\b";
            case "\x09":
                return "\\t";
            case "\x1a":
                return "\\z";
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\"":
            case "'":
            case "\\":
            case "%":
                return "\\"+char; // prepends a backslash to backslash, percent,
                                  // and double/single quotes
        }
    });
}


