/*
 * Users Authenticate.
 */

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var RememberMeStrategy = require('../lib').Strategy;
var crypto = require('crypto');
var utils = require('../utils');
var connection;
// callback
 var myCallback = function(connection) {
   connection.connect(function(err) {
    if(err) {
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000);
    }
  });
   connection.on('error', function(err) {
    if(err.code === 'PROTOCOL_CONNECTION_LOST') {
      console.log('user enter');
      usingItNow(myCallback);
    } else {
      throw err;
    }
  });
 };
 var usingItNow = function(callback) {
  connection = require('../db.js').handleDisconnect();
   callback(connection);
 };
 usingItNow(myCallback);

// Find user by ID that saved in cookie file
function findById(id, fn) {
  var sql="SELECT * FROM users WHERE id = '"+ id +"' limit 1";
  connection.query(sql, function(err , user) {
    if (user.length > 0) {
        fn(null, user[0]);
      } else {
        fn(new Error('User ' + id + ' does not exist'));
    }
  });
}

var tokens = {}

function consumeRememberMeToken(token, fn) {
  var uid = tokens[token];
  // invalidate the single-use token
  delete tokens[token];
  return fn(null, uid);
}

function saveRememberMeToken(token, uid, fn) {
  tokens[token] = uid;
  return fn();
}

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  findById(id, function (err, user) {
    done(err, user);
  });
});

// Local strategy
passport.use(new LocalStrategy(
  function(username, password, done) {
    process.nextTick(function () {
      // Find the user by username.  If there is no user with the given
      return check_auth_user(username,password,done);
    });
  }
));

// Remember Me cookie strategy
passport.use(new RememberMeStrategy(
  function(token, done) {
    consumeRememberMeToken(token, function(err, uid) {
      if (err) { return done(err); }
      if (!uid) { return done(null, false); }
      findById(uid, function(err, user) {
        if (err) { return done(err); }
        if (!user) { return done(null, false); }
        return done(null, user);
      });
    });
  },
  issueToken
));

// Create token
function issueToken(user, done) {
  var token = utils.randomString(64);
  saveRememberMeToken(token, user.id, function(err) {
    if (err) { return done(err); }
    return done(null, token);
  });
}


exports.login = function(req, res){
  if(typeof req.user == 'undefined'){
    res.render('login', { title: 'Login Page' , message: req.flash('error') });
  }
  else
    res.render('index', { user: req.user , message: req.flash('error') });
};


exports.logout = function(req, res){
   res.clearCookie('remember_me');
   req.logout();
   res.redirect('/');
};

exports.checkLogin = function(req, res, next){
  console.log(req.body.username);
  console.log(req.body.password);
    if (!req.body.remember_me) { return next(); }
    issueToken(req.user, function(err, token) {
      if (err) { return next(err); }
	    console.log('token',token)
      res.cookie('remember_me', token, { path: '/', httpOnly: true, maxAge: 604800000 });
      res.cookie('username', req.body.username, { path: '/', httpOnly: true, maxAge: 604800000 });
      return next();
    });
 };


// Check User Authenticated
function check_auth_user(username, password, done){
    var password = 'DYhG9980penuswvniR2G0FgaC9mi' + password;
    var encryptedPassword = crypto.createHash('sha1').update(password).digest("hex");
    //console.log(username,password);
    var sql='SELECT * FROM users WHERE BINARY username="' +username+ '" AND password ="' +encryptedPassword+ '" AND admin=1 LIMIT 1';
    //console.log("sql..",sql);
    connection.query(sql,
        function (err,results) {
            if (err) throw err;
            if(results.length > 0){
              //console.log("results",results);
                var res=results[0];
                    return done(null, res);
            }else{
                return done(null, false, { message: 'Please enter valid details!' });
         }
    });
}
