
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var request= require('request');
var user = require('./routes/user');
var cheerio = require('cheerio');
var fs = require('fs');
var AWS = require('aws-sdk');
var events = require('./routes/events');
var dailyQuotes = require('./routes/dailyQuotes');
var linksImage = require('./routes/linksImage');
var wikiRoute = require('./routes/wikiRoute');
var http = require('http');
var path = require('path');
var passport = require('passport');
var flash = require('connect-flash');

var socketio = require('socket.io');
var config = require("./lib/config");

var app = express();

// all environments
app.set('port', process.env.PORT || 4006);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.logger('dev'));
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb'}));
app.use(express.cookieParser('your secret here'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.session({ secret: 'h6hr56b67tu7t6brt6tni68b775h86758765' }));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(passport.authenticate('remember-me'));
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));




// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

var accessKeyId =  process.env.AWS_ACCESS_KEY || "xxxxxx";
var secretAccessKey = process.env.AWS_SECRET_KEY || "+xxxxxx+B+xxxxxxx";
AWS.config.update({
  accessKeyId: config.AWS[0].accessKeyId,
  secretAccessKey: config.AWS[0].secretAccessKey
});

var s3 = new AWS.S3();

app.get('/', ensureAuthenticated, routes.index);
app.get('/partials/:name', routes.partials);
app.get('/event/partials/:name', routes.partials);
app.post('/events', ensureAuthenticated,  events.events);
app.get('/quote' , dailyQuotes.quote);
app.get('/linkDelete/:id', events.linkDelete);
app.post('/event', events.eventType);
app.get('/api/eventLinks/:id', events.eventlinks);
app.put('/editEvent/:id', events.editEvn);
app.post('/event/link/:id', events.addLink);
app.post('/quotes' , dailyQuotes.quoteByDate);
app.post('/headline/event' , dailyQuotes.headlineEventFalse);
app.post('/headline/eventTrue' , dailyQuotes.headlineEventTrue);
app.post('/headline/quote' , dailyQuotes.updateQuote);
app.get('/linkData:id' , dailyQuotes.linkData);
app.post('/saveLink' , linksImage.saveLinkData);
app.post('/linkDataa' , linksImage.getLinkData);
app.post('/add/newEvent', events.addNewEvent);
app.get('/login', user.login);
app.get('/event/:id', events.post);
app.get('/search/:name', events.search);
app.post('/wikiData' , wikiRoute.wikiData);
app.post('/addAllEvents', events.addNewEvents);
app.post('/addLink', events.addNewLink);

app.post('/addLink1', events.addNewLink_new);
// Run background process
app.post('/addImagesUrls', linksImage.addImagesUrls);

app.get('/test', linksImage.test);

app.post('/eventLink', events.addEventLink);
app.post('/eventItem', wikiRoute.eventItem);
// testing ca
app.get('/test11', wikiRoute.test11);
app.get('/test22', wikiRoute.test22);

app.post('/localWikiData', wikiRoute.localWikiData);


app.post('/birthItem', wikiRoute.birthItem);
app.post('/deathItem', wikiRoute.deathItem);
app.post('/holidayItem', wikiRoute.holidayItem);
app.post('/uploadParserImage', linksImage.uploadParserImage);
app.post('/savedupEvents',wikiRoute.savedupEvents);
app.post('/deleteExistEvents',wikiRoute.deleteExistEvents);
app.post('/updateEvent',events.updateEvent);
app.post('/noImageLink',events.noImageLink);
app.post('/connectLinkEvent',events.connectLinkEvent);



app.post('/login',passport.authenticate('local', { failureRedirect: '/login', failureFlash: true }) , user.checkLogin, function(req, res) {
    // req.session.socket2 = soc;
    res.redirect('/');
});

// LogOut user
app.get('/logout', user.logout);

var server = http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

var io = socketio.listen(server);

io.on('connection', function(socket) {
  app.locals.someVar = socket;
  socket.emit('message', { message: 'Socket is connected!!' });
});


function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/login')
}

app.post('/uploadImage', linksImage.uploadImage);
app.post('/base64/image', linksImage.localUpload);

app.post('/linkImage', function(req, res){
  var counter = req.body.count;
  var image_index = req.body.image_index;
  var url = req.body.url;
  var link_id = req.body.link_id;
  var index = req.body.index;
  var src = '';
  var title = '';
  request(url, function(err,resp,body) {
      if(!err && resp.statusCode == 200) {
          var $ = cheerio.load(body);
          title = $('title').text();
      if(typeof $(".infobox .image").attr("href") != 'undefined')
        imgTag = $(".infobox .image").attr("href");
      else
        imgTag = $(".thumb .image").attr("href");

      if(typeof imgTag == 'undefined')
        imgTag = $(".vertical-navbox .image").attr("href");
      if(typeof imgTag == 'undefined')
        res.json({ curl:url,indexR:index,src: title,count:counter , link_id:link_id , image_index:image_index});
      nUrl = "http://en.wikipedia.org"+ imgTag;
      //console.log("nUrl==>", nUrl,"imgtag==>",imgTag);
      request(nUrl, function(err,resp,body) {
        if(!err && resp.statusCode == 200) {
          var $ = cheerio.load(body);
          var mainImagee = $(".fullImageLink a").attr("href");
          var mainImage1 = $(".fullImageLink a img").attr("src");
          var urls= [];
          $('body img').each(function(){
            var url = this.attr('src');
            urls.push(url);
          });
          res.json({curl:url, indexR:index,src: title, sUrl: mainImagee, mnUrl: mainImage1 , count:counter , link_id:link_id , image_index:image_index});
        }
      });
    }
  });
});
