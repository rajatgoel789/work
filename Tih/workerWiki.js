// Worker
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');


// DataBase Connection
var connection;
// callback
 var myCallback = function(connection) {
   connection.connect(function(err) {
    if(err) {
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000);
    }
  });
   connection.on('error', function(err) {
    if(err.code === 'PROTOCOL_CONNECTION_LOST') {
      console.log('links enter');
      usingItNow(myCallback);
    } else {
      throw err;
    }
  });
 };
 var usingItNow = function(callback) {
  connection = require('./db.js').handleDisconnect();
   callback(connection);
 };
 usingItNow(myCallback);

// Child Process
process.on('message', function(m) {
   var url = m.url;
   request(url, function(err,resp,body) {
    console.log('process on - - - >', url)
    if(!err && resp.statusCode == 200) {
      var $ = cheerio.load(body);
      title = $('title').text();
      var eventsW = [];
      var births = [];
      var deaths = [];
      var holidays = [];
      var last ="temp";
      var holiday = $('#Holidays_and_observances').parent().next().text();
      var eventsHtml = $('#Events').parent().next().find('ul li').length;
      var eventsHtml1 = $('.thumb , .tright').next().find('ul li').length;
      var birthHtml = $('#Births').parent().next().find('ul li').length;
      var deathHtml = $('#Deaths').parent().next().find('ul li').length;
      var holidayHtml = $('#Holidays_and_observances').parent().next().find('ul li').length;
      var holidayData = holiday.split('\n');

      // events wiki item
      for(var i=0; i<eventsHtml; i++){
        var linksName = [];
        var eventsText = $('#Events').parent().next().find("li").eq(i).text();
        var eventData = eventsText.split('–');
        var splitYear = eventData[0].split(' ');
        if(splitYear[1])
          var yearC = '-'+splitYear[0];
        else
          var yearC = splitYear[0];
        var events = $('#Events').parent().next().find("li").eq(i).children('a').length;
        for(var a=1; a < events; a++){
          var urls = $('#Events').parent().next().find("li").eq(i).children('a').eq(a).attr('href');
          urls = 'http://en.wikipedia.org'+urls;
          var name = $('#Events').parent().next().find("li").eq(i).children('a').eq(a).text();
          var ext = urls.split('.').pop().split(/\#|\?/)[0];
          if(ext != 'htm' && ext != 'html' && ext != 'php' ) {
            if(urls.indexOf('&') == -1 && urls.indexOf('=') == -1){
               linksName.push({'url':urls,'name':name});
            }
          }
        }
        //console.log("links--->",linksName);

        if(eventData[2]){
          var concatData = eventData[1]+'-'+eventData[2];
            eventsW.push({'event':concatData, 'year':yearC,'eventUrl':linksName});
        }else{
            if(typeof eventData[1] != 'undefined'){
               eventsW.push({'event':eventData[1], 'year':yearC,'eventUrl':linksName});
            }
        }
      }

      //for div inside wiki Events item
      if(eventsHtml1 > 0){
        console.log('find a div inside events wiki item ');
        for(var i=0; i<eventsHtml1; i++){
          var linksName = [];
          var eventsText = $('.thumb , .tright').next().find("li").eq(i).text();
          if(eventsText.search("–") > -1){
            var eventData = eventsText.split('–');
          }
          var splitYear = eventData[0].split(' ');
          if(splitYear[1])
            var yearC = '-'+splitYear[0];
          else
            var yearC = splitYear[0];
          var events = $('.thumb , .tright').next().find("li").eq(i).children('a').length;
          for(var a=0; a<events; a++){
            var urls = $('.thumb , .tright').next().find("li").eq(i).children('a').eq(a).attr('href');
            urls = 'http://en.wikipedia.org'+urls;
            var name = $('.thumb , .tright').next().find("li").eq(i).children('a').eq(a).text();
            var ext = urls.split('.').pop().split(/\#|\?/)[0];
            if(ext != 'htm' && ext != 'html' && ext != 'php' ) {
              if(urls.indexOf('&') == -1 && urls.indexOf('=') == -1){
                 linksName.push({'url':urls,'name':name});
              }
            }
          }
          if(eventData[2]){
            var concatData = eventData[1]+'-'+eventData[2];
            eventsW.push({'event':concatData, 'year':yearC,'eventUrl':linksName});
          }else{
            eventsW.push({'event':eventData[1], 'year':yearC,'eventUrl':linksName});
          }
        }
      }
    // births wiki item
    for(var i=0; i<birthHtml; i++){
      var linksName = [];
      var eventsText = $('#Births').parent().next().find("li").eq(i).text();
      if(eventsText.search("–") > -1){
        var eventData = eventsText.split('–');
      }
      var splitYear = eventData[0].split(' ');
      if(splitYear[1])
        var yearC = '-'+splitYear[0];
      else
        var yearC = splitYear[0];
      var nameVal = eventData[1].split(',');
      var events = $('#Births').parent().next().find("li").eq(i).children('a').length;

      for(var a=0; a<events; a++){
        var urls = $('#Births').parent().next().find("li").eq(i).children('a').eq(a).attr('href');
        urls = 'http://en.wikipedia.org'+urls;

        var name = $('#Births').parent().next().find("li").eq(i).children('a').eq(a).text();
        var ext = urls.split('.').pop().split(/\#|\?/)[0];
          if(ext != 'htm' && ext != 'html' && ext != 'php' ) {
            if(urls.indexOf('&') == -1 && urls.indexOf('=') == -1){
              if( isNaN(name) ){
                linksName.push({'url':urls,'name':name});
              }
          }
        }

      }
       //console.log("links--->",linksName);
      births.push({'name':nameVal[0],'event':eventData[1], 'year':yearC,'birthUrl':linksName});
    }
    // death wiki items
    for(var i=0; i< deathHtml; i++){
      var linksName = [];
      var eventsText = $('#Deaths').parent().next().find("li").eq(i).text();
      if(eventsText.search("–") > -1){
        var eventData = eventsText.split('–');
      }
      var splitYear = eventData[0].split(' ');
      if(splitYear[1])
        var yearC = '-'+splitYear[0];
      else
        var yearC = splitYear[0];
      var nameVal = eventData[1].split(',');
      if(nameVal.indexOf(last) == -1) {
          var events = $('#Deaths').parent().next().find("li").eq(i).children('a').length;

          for(var a=0; a<events; a++){
            var urls = $('#Deaths').parent().next().find("li").eq(i).children('a').eq(a).attr('href');
            urls = 'http://en.wikipedia.org'+urls;
            var name = $('#Deaths').parent().next().find("li").eq(i).children('a').eq(a).text();
            var ext = urls.split('.').pop().split(/\#|\?/)[0];
              if(ext != 'htm' && ext != 'html' && ext != 'php' ) {
                if(urls.indexOf('&') == -1 && urls.indexOf('=') == -1){
                  if( isNaN(name) ){
                    linksName.push({'url':urls,'name':name});
                  }
              }
            }
          }
       //console.log("links--->",linksName);
         deaths.push({'name':nameVal[0],'event':eventData[1], 'year':yearC,'birthUrl':linksName});
      }
      if(nameVal.length > 1){
         last = nameVal[1];
      }
    }

    // holidays wiki item
    for(var i=0; i<holidayHtml; i++){
      var linksName = [];
      var eventsText = $('#Holidays_and_observances').parent().next().find("li").eq(i).text();
      var events = $('#Holidays_and_observances').parent().next().find("li").eq(i).children('a').length;
      for(var a=0; a<events; a++){
        var urls = $('#Holidays_and_observances').parent().next().find("li").eq(i).children('a').eq(a).attr('href');
        urls = 'http://en.wikipedia.org'+urls;
        var name = $('#Holidays_and_observances').parent().next().find("li").eq(i).children('a').eq(a).text();
        linksName.push({'url':urls,'name':name});
      }
      //console.log("links--->",linksName);
      holidays.push({'event':eventsText,'birthUrl':linksName});
    }

    // cache set
    var myData = { wikiEvents:eventsW , wikiDeaths: deaths , wikiHolidays:holidays , wikiBirths:births };

    var outputFilename = './public/wikidata/'+m.name+'.json';

    fs.writeFile(outputFilename, JSON.stringify(myData, null, 4), function(err) {
        if(err) {
          console.log(err);
          process.send(m);
        } else {
          console.log("JSON saved to " + outputFilename);
          process.send(process.pid);
        }
    });

    }
  });

});
