var config = require("./lib/config");
var db_config = {
    host: config.DATABASE[0].host,
    user: config.DATABASE[0].user,
    password: config.DATABASE[0].password,
    database: config.DATABASE[0].database
};


// var db_config = {
//  host: 'localhost',
//  user: 'root',
//  password: 'root',
//  database: 'tihnew'
// };

var mysql = function handleDisconnect() {
  return require('mysql').createConnection(db_config); // Recreate the connection, since
}
module.exports.handleDisconnect = mysql;
